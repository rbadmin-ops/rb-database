DROP procedure IF EXISTS `admin_getRelationshipNeeds`;

DELIMITER $$
CREATE DEFINER=`DoubleAngel`@`%` PROCEDURE `admin_getRelationshipNeeds`(rid int)
BEGIN
-- rid = relationships.id

select needs.name as need, 
		trinities.abbreviation as trinity, 
		un.no_longer_needed, 
        ns.users_need_id,
		ns.slider_value, 
		ns.turned_stormy, 
		relationships.name,
		un.modified as UserNeedModifiedDate,
		un.created as UserNeedCreatedDate
  from q0_relationships relationships 
		join q0_users users on users.id = relationships.user_id
		join q0_users_needs un on un.user_id = users.id
		join q0_needs needs on needs.id = un.need_id
		join q0_needsliders ns on un.id = ns.users_need_id
		join q0_trinities trinities on trinities.id = needs.trinity_id
where relationships.id = rid
;
END$$

DELIMITER ;

