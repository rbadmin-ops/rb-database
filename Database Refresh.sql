

set FOREIGN_KEY_CHECKS = 0;

truncate table q0_administrative_audits;
truncate table q0_advice_flags;
truncate table q0_advice_categories;
truncate table q0_advice_reads;
truncate table q0_advice_items;
truncate table q0_checkins;
truncate table q0_contents;  -- this will be filled 
truncate table q0_enumerations; -- this will be filled
truncate table q0_faqs; -- this will be filled
truncate table q0_follows;
truncate table q0_forecasts;
truncate table q0_forecasts_needsliders;
truncate table q0_iacs; -- this will be filled
truncate table q0_licensemanage; -- this will be filled
truncate table q0_login_tokens; -- filled for default users
truncate table q0_needs;  -- filled for default users
truncate table q0_needsliders; -- filled for default users
truncate table q0_needsliders_histories;
truncate table q0_relationships; -- filled for default users
truncate table q0_relationships_histories; 
truncate table q0_relationships_satisfactions;
truncate table q0_statuses; -- filled for default users
truncate table q0_suggestions; 
truncate table q0_systemfeedbacks;
truncate table q0_tmp_emails;
truncate table q0_trinities; -- filled in
truncate table q0_user_activities; -- filled for default users
truncate table q0_user_contacts; -- filled for default users
truncate table q0_user_details; -- filled for default users
truncate table q0_user_email_recipients; -- filled for default users
truncate table q0_user_email_signatures; -- filled for default users
truncate table q0_user_email_templates;  -- filled for default users
truncate table q0_user_emails; -- filled for default users
truncate table q0_user_group_permissions; -- filled for default users
truncate table q0_user_groups;  -- filled for default users
truncate table q0_user_profiles; -- filled for default users
truncate table q0_user_settings; -- filled for default users
truncate table q0_users; -- filled for default users
truncate table q0_users_needs; -- filled for default users
truncate table q0_users_needs_histories;
truncate table q0_weatherwheel_segments;  -- filled in



-- copy in some of the base data from the users auth plugin
# ************************************************************
# Sequel Pro SQL dump
# Version 4499
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: brbqadevstg01.czsiaxnynump.us-east-1.rds.amazonaws.com (MySQL 5.6.21-log)
# Database: brb_dev
# Generation Time: 2015-10-13 13:27:10 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table q0_contents
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_contents`;

CREATE TABLE `q0_contents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_name` text,
  `url_name` text,
  `page_content` text,
  `page_title` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table q0_enumerations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_enumerations`;

CREATE TABLE `q0_enumerations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_name` varchar(45) NOT NULL COMMENT 'Name used in the program for the enumerations, which should match the field in the database name.  Names should all be very easy to understand what table and field they are enumerating. Another option is two fields, table name and field name.',
  `word` varchar(45) NOT NULL COMMENT 'word associated with the value, such as married, single etc.',
  `value` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified_by_user_id` int(11) DEFAULT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `enumerations_createdby` (`user_id`),
  KEY `enumerations_modifiedby_idx` (`modified_by_user_id`),
  CONSTRAINT `enumerations_createdby` FOREIGN KEY (`user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `enumerations_modifiedby` FOREIGN KEY (`modified_by_user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='This table is useful for working in the database only.  The enumerations I have outlined will be defined in the program but look like magic numbers here unless there is a "decoder ring".  This is the decoder ring';

LOCK TABLES `q0_enumerations` WRITE;
/*!40000 ALTER TABLE `q0_enumerations` DISABLE KEYS */;

INSERT INTO `q0_enumerations` (`id`, `field_name`, `word`, `value`, `user_id`, `created`, `modified_by_user_id`, `modified`)
VALUES
	(2,'needsliders_histories.checkin_move','no',0,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
	(3,'needsliders_histories.checkin_move','yes',1,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
	(4,'needsliders_histories.checkin_move','moved by delete',2,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
	(5,'trinities.abbreviation','GNH',1,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
	(6,'trinities.abbreviation','GH',2,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
	(7,'trinities.abbreviation','LTH',3,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
	(8,'needsliders_histories.checkin_move','moved by need edit trinity change',4,1,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00'),
	(9,'needsliders_histories.checkin_move','moved by checkin edit',3,1,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00'),
	(10,'checkins.checkin_type','Regular',0,1,'2015-04-01 00:56:11',1,'2015-04-01 00:56:11'),
	(11,'checkins.checkin_type','Restarted',1,1,'2015-04-01 00:56:11',1,'2015-04-01 00:56:11'),
	(12,'checkins.checkin_type','Ended',2,1,'2015-04-01 00:56:11',1,'2015-04-01 00:56:11'),
	(14,'neesliders_histories.checkin_move','moved by monthly checkup user',5,1,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00');

/*!40000 ALTER TABLE `q0_enumerations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table q0_faqs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_faqs`;

CREATE TABLE `q0_faqs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(150) NOT NULL,
  `functional_area` varchar(45) DEFAULT NULL,
  `answer` text,
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified_by_user_id` int(11) DEFAULT NULL,
  `modified` datetime NOT NULL,
  `key` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `faqs_createdby` (`user_id`),
  KEY `faqs_modifiedby_idx` (`modified_by_user_id`),
  CONSTRAINT `faqs_createdby` FOREIGN KEY (`user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `faqs_modifiedby` FOREIGN KEY (`modified_by_user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='I have not added search indexing pending knowing the tool we are going to use	';

LOCK TABLES `q0_faqs` WRITE;
/*!40000 ALTER TABLE `q0_faqs` DISABLE KEYS */;

INSERT INTO `q0_faqs` (`id`, `subject`, `functional_area`, `answer`, `user_id`, `created`, `modified_by_user_id`, `modified`, `key`)
VALUES
	(2,'Is my information private?','Global','<a href=\"#\" onclick=\"window.open(\'http://www.cnn.com\',\'_system\',\'location=yes\')\">TEST LINK</a>',1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00','faq_2'),
	(3,'What is a Check-In?','Check-in','A Check-in provides you with the ability to create a history of significant events that happen in your relationship.  This, over time, allows you to see patterns in your relationship in the Monitor Check-in History related to how many sunny vs. stormy interactions are occurring.  If you do this consistently, you\'ll be able to see how well your relationship is actually meeting what you\'re looking for, and will ultimately impact your probability of long term relationship health and fulfillment.',1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00','faq_2'),
	(4,'What are Gotta Have\'s?','My Needs','A \"Gotta Have\" Need is something in your relationship you can\'t live without.  Gotta Have\'s can provide a strong foundation to your relationship if being met, or a feeling of being deprived that gets worse over time if not.',1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00','faq_3'),
	(5,'How can I change my username?','Registration','You can\'t change your username at this time.  If this is important to you, let us know through Settings/Feedback.',1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00','faq_4'),
	(6,'Why can\'t people under 18 register?','Registration','There may be Needs entered by adults that would be inappropriate for minors to be exposed to.   We\'re a conservative group when it comes to protecting children.',1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00','faq_5'),
	(7,'How can I delete a relationship profile?','Global','You can end a relationship in Settings, which will stop the monthly check-up process, but you can\'t delete a past relationship.  As painful as it might have been, it\'s crucial to have that information to reflect on things, learn from them, and continue to build upon that learning.',1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00','faq_6'),
	(8,'Why does my relationship have a Monthly Check Up?','Monitor','We want to try and ensure that you get a chance to reflect on how your relationship is progressing, and whether it\'s getting better or wore over time, to allow you to be informed and make the best decision you can.  A monthly process just provides a structure to do that.  You should, however, not wait until the end of the month to see how things are going, adjust your Needs, and Check-in moments you experience that impact how you feel!',1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00','faq_7'),
	(9,'Why are my demographics needed in registration?','Registration','We are going to be continuously improving our forecasting calculation, which could be impacted by your demographics as well as by the community of user information that we aggregate to improve forecasting.  We also plan on introducing enhancements that will utilize your demographics, so better to get them in now!',1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00','faq_8'),
	(10,'How many Needs should I enter?','My Needs','You need at least three Needs to get a Forecast.  How many you enter is entirely up to you, and should reflect what really matters to you.',1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00','faq_9'),
	(11,'How do I know what my Needs should be?','My Needs','Reflect on your past relationship experiences.  Aspects you really loved can help determine your Gotta Have\'s; things that you really disliked deeply are your Gotta Not Have\'s; and things you often enjoyed doing are your Like to Have\'s.  You can also become aware of new Needs as you create Check-ins.',1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00','faq_10'),
	(12,'Why don\'t my unsatisfied \"Like to Have\" Needs show up as Needing Improvement?','My Needs','Like to Have\'s are not fundamental Needs.  You could have a lot of Like to Have\'s, but none of them individually are crucial to your relationship satisfaction, so listing all of those as needing improvement isn\'t really helpful to you.  While they do affect your Forecast, they have a much smaller impact than your Gotta Have and Gotta Not Have Needs.',1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00','faq_11'),
	(13,'Why do I have the same needs for all of my relationships?','My Needs','Your Needs reflect what matters most to you in your relationship.  They should not vary by person, although how each person meets your Needs certainly can vary.',1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00','faq_12'),
	(14,'Why do my changes to my Needs continue to be reflected in my ended  relationships?','My Needs','Showing a Forecast based on Needs that no longer matter just didn\'t seem appropriate.  You don\'t have to go in and assess Needs for those ended relationships, as the slider will be positioned on partly sunny, but it could be interesting to do that if you\'d like to see how the person would have satisfied your current Needs.',1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00','faq_13'),
	(15,'Why do I go through a Monthly  Check-up if I just started a relationship before the end of the month?','Global','The Monthly Check-up review occurs as a consistent reminder for you develop self-awareness about how each relationship is meeting your needs.  By showing you where your Needs stand, asking you how you feel about the relationship, and showing you a monthly summary, you begin to get used to the process from the start of a relationship, rather than just going along without guidance.',1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00','faq_14'),
	(16,'What\'s the difference between the Monthly Check-up/Forecast History summary and the Monitor Forecast Summary?','Global','The Monthly Summary shows how your relationship progressed over the month, noting what\'s sunnier or stormier, while your Forecast Summary is a recap supporting the reasons why your overall Forecast is pointed where it is.  \r\n\r\nYou might have had a stormy month, but your overall relationship could still be sunny based on everything that you\'ve experienced overall!',1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00','faq_15'),
	(17,'How do the weather symbols for a Check-in affect my relationship forecast?','Check-in','If you assign the Check-in to a Need, it will affect how well that Need is being satisfied depending on how much sunnier or stormier that Check-in is relative to  the Need weather icon before the Check-in.  In this manner, your Check-ins automatically adjust your Need satisfaction based on what you experience, and therefore impact your forecast!',1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00','faq_16'),
	(18,'What are Gotta Not Have\'s?','My Needs','A \"Gotta Not Have\" is something that you absolutely do not want in your relationship. Experiencing them causes you to feel imposed upon, leading to anger and resentment, and are more likely to contribute to an eventual break-up than your other Needs not being met.',1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00','faq_17'),
	(19,'What are Like to Have\'s?','My Needs','A \"Like to Have\" Need is one that gives you fun and enjoyment, but not something that you must have.   You could live with or without any one, but should have some met to add fun and enjoyment to your relationship.  These are most likely to be your hobbies, entertainment and recreational activities.',1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00','faq_18'),
	(20,'Why should my username be anonymous?','Global','We plan on adding features that will let users communicate with each other, and the anonymity will protect your identity considering you may be dealing with sensitive topics related to your or their relationship.',1,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00','faq_19'),
	(21,'Testing Subject Line','Sign-in','<a href=\"http://www.cnn.com\">TEST LINK</a>\r\nNew type of couples therapy features a sex-forward approach ranging from discussions about what a couple\'s own rules for monogamy are, to discussing the influence pornography has on the relationship. #dating #sex #relationships <a href=\"http://www.yahoo.com\" >First Comes Sex Talk</a>',1,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00','faq_20'),
	(22,'How is my Forecast created?','Monitor','Your Forecast provides the probability, based on how well your Needs are being supported or not, of where your relationship future appears to be headed.  It\'s based on your My Needs weather slider values, which are impacted by your Check-ins.  A\" Gotta Not Have\" Need affects your Forecast the most.  \"Gotta Have\" Needs have the next most important impact, and \"Like to Have\'s\" the least impact.   YourForecast is based on how many of these Needs you have entered, and how well they are being met.  The stormier your Needs, the worse your Forecast.',1,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00','faq_21'),
	(23,'Why should I relate Check-ins to My Needs?','Check-in','Check-ins are a reflection of the experiences you\'re having in your relationship, from great (sunny) to terrible (tornado).  By relating your experiences to your Needs, the system will continuously auto-adjust your Need fulfillment and provide you with the reality of how well your Needs are being supported.',1,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00','faq_22'),
	(24,'How do I change the Need a Check-in is related to?','Check-in','You can Edit by left swiping the entry in \"Check-ins\", and then can Unlink the Need (touching the unlink icon) and then touch the button to relate it to another Need.  This will potentially affect the Need fulfillment of any affected Needs.',1,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00','faq_23'),
	(25,'Can I Change or Delete a Check-in?','Check-in','You can delete a Check-in by left swiping and touching the Delete icon.  You can edit the Check-in date, weather symbol, description or Need it is related to by touching the Edit icon.',1,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00','faq_24'),
	(26,'Can I Edit or Delete a Need?','My Needs','You can edit or delete a Need by left swiping on that entry in \"My Needs\".  \r\n\r\nYou can delete it by touching the Delete icon, which will remove that Need from any past or existing relationship you have entered.  \r\n\r\nYou can Edit it (for the Relationship listed in the top header bar) by touching the Edit icon.  You can change the Need type (Gotta Have, Like to Have, Gotta Not Have), description or rating for that relationship.  Changing the Need type or description affects all relationships.',1,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00','faq_25'),
	(27,'Need description lookahead feature','My Needs','When you\'re entering a Need description, the lookahead feature will show you some matches that you may give you ideas.  You can touch the entry to select it, and can also modify it to convey what you\'d like it to say.   \r\n\r\nIf you see a description that you believe to be very inappropriate, please let us know through \"Settings/Contact Support\" so that we can remove it from being displayed.',1,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00','faq_26'),
	(28,'Why does the app automatically sign me out?','Sign-in','If you don\'t have any activity for an hour, we automatically sign you out for your own protection in case you\'re not using screen lock with a passcode.',1,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00','faq_27'),
	(29,'Where does \"How do I feel about my relationship?\" affect anything?','Monitor','You\'re asking to move the weather slider to convey \"How do I feel about my relationship?\" at four different places:\r\n\r\n1. When you first start a relationship;\r\n2. When you go through the Monthly Check-up for a relationship;\r\n3. If you End a relationship; and\r\n4. If you Restart an ended relationship.\r\n\r\nThis is used in Monitor to compare your \"perception\" about how your relationship with your Needs fulfillment and relationship experiences.  In this manner, we hope to give you clearer insight and perspective based on your reality.',1,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00','faq_28');

/*!40000 ALTER TABLE `q0_faqs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table q0_iacs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_iacs`;

CREATE TABLE `q0_iacs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(50) DEFAULT NULL,
  `value` text,
  `screenshot_url` varchar(255) DEFAULT NULL,
  `description` text,
  `faq` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `q0_iacs` WRITE;
/*!40000 ALTER TABLE `q0_iacs` DISABLE KEYS */;

INSERT INTO `q0_iacs` (`id`, `key`, `value`, `screenshot_url`, `description`, `faq`)
VALUES
	(5,'sign_1','Uh oh! The password is incorrect for this email address. Please re-enter the email address and password.  Touch Forgot Password if you forgot it!','','The email address isn\'t formatted properly',0),
	(9,'sign_2','There\'s no user registered for the email address you entered.  Please try again.','','This email address being used by another username',0),
	(10,'sign_3','A link to reset your password has been sent to your email address.','','The email address and username do not match',0),
	(12,'reg_1','This email address is already being used. Please register with a different email address.','','Your Password must be at least 4 characters',0),
	(13,'reg_2','Please enter a valid email address, such as you@gmail.com.','','Your passwords don\'t match, please re-enter them again',0),
	(14,'reg_4','Uh oh! Your passwords don\'t match. Please try again.','','Your Username can\'t be blank',0),
	(15,'reg_6','Your Username must be between 3-20 characters without any special characters (* ! % @).  And remember to make it anonymous.','','Relationship name cannot be blank',0),
	(16,'reg_7','Sorry... someone picked that great Username before you!  Please try another name.','','	 You\'re already using this relationship name',0),
	(17,'reg_8','THIS IS HARD CODED. Are you sure you want to cancel registration?','','Your Relationship Start date cannot be blank',0),
	(18,'set_2','Please enter at least one character for this relationship name','','Your Relationshp End date can not be before your Start date',0),
	(19,'reg_10','Please enter a valid zip code, or skip entering this item.','','Your Relationship End date cannot be in the future',0),
	(20,'reg_11','Explain that <me> can be used to maintain their Needs until they start dating someone; at that time they should add a new relationship by touching <me>(show the arrow pointing to the top <me> name and Add a relationship.','','A Relationship has been created with your Username that yu can use to manage your Needs or Check-in non-relationship focused things',0),
	(21,'reg_12','Explain that <me> can be used to maintain their Needs; they can use multiple relationships to Check-in their experiences by adding them  by touching <me>(show the arrow pointing to the top <me> name and Add a relationship.','','We know you\'re just starting this relationship, but how satisfied are you with it at this time?',0),
	(22,'reg_13','Oops, you didn\'t select any Gotta Have needs from the get started pick list! \r\n\r\nIf any descriptions were close, you use the back arrow to select them, and can edit the description in \"My Needs\" at any time.\r\n\r\nOtherwise you can add individual Needs at another time.','','Congratulations for completing registration and using the Relationship Barometer to help make your relationship the best it can be!',0),
	(23,'set_1','Oops! Did you forget your password? Please try again.','','Old Password isn\'t correct',0),
	(24,'need_7','unused need_7','','The Need description cannot be blank',0),
	(25,'need_4','Alert! Changing the Need description affects any Check-ins assigned to this Need. If this is really a different Need, add a new one using the (+). Do you just want to change the description?','','Needs apply to all relationships, so remember to go into your other Relationships and assess how well this need is being satisfied for them.',0),
	(26,'need_5','Sorry.  We couldn\'t add that Need as it already exists.','','Changing a Need type affects all Relationships, as well as any Check-ins assigned to that Need.',0),
	(27,'need_1','This Need will be removed from ALL relationship profiles and Check-ins, which may impact your Forecast. Are you sure it\'s no longer important to you?','','Deleting a Need deletes it for all Relationships, and causes any Check-ins assigned to that Need to no longer be assigned to any Need.',0),
	(28,'mon_1','Alert! Your history is only available after your first Check-up, which occurs at the end of each month.','','Display a message that 3 Needs are required for a forecast',0),
	(29,'mon_2','You don\'t have any Check-ins yet. You can view history after you create a couple. This will allow you to see your relationship trends.','','Display a message that only 1 Need is entered, and 3 are required for a forecast',0),
	(30,'set_3','Uh oh! You\'re already using this name for a relationship. Please choose a different username. You can even use a nickname.','','Display a message that only 2 Needs are entered, and 3 are required for a forecast',0),
	(31,'set_4','We hope this is a positive change for you!  While you can still add Check-ins and see your history, please restart this relationship if you get back together.','','The No Needs need Improvement message is displayed',0),
	(32,'set_5','Congrats! <Realtionship Name> has been restarted. We\'ll be here to help you as you continue to Love & Learn. We recommend rating My Needs now as some of them may have changed since you ended this relationship.','','Perceived Satisfaction has not been updated in over a month.',0),
	(33,'set_6','Congrats! Good luck with your new relationship.  Go to My Needs to rate them so you can get your first Forecast.','','Not enough data to see a Forecast History',0),
	(34,'set_7','It\'s good to add past relationships to reflect on what you can learn from them. If you\'re curious, go to My Needs to rate them so you can see how they rank against what matters to you now.','','Forecast is sunny (>56%) and their Satisfaction is within 1 segment <= 14%)',0),
	(35,'set_8','Success! Your Profile has been updated.','','Forecast is sunny (>56%) and their Satisfaction is within 2 segments (> 14% and <+ 28% )',0),
	(36,'mon_9','Sorry! Did you forget to add Check-ins  this week? If so, add some experiences you might have missed.','','Forecast is sunny (>56%) and their Satisfaction is off by more than 2 segments greater 28%)',0),
	(37,'checkin_1','Whoops! This is not an active relationship. Please Restart this relationship in Settings if you would like to continue getting a history recorded for it.','','Forecast is stormy (<=56%) and their Satisfaction is within 1 segment <= 14%)',0),
	(38,'need_2','Please enter a description for this Need. Type a couple of letters to see suggested Needs that others have created.','','Forecast is stormy (<=56%) and their Satisfaction is within 2 segments (> 14% and <+ 28% )',0),
	(39,'checkin_2','Please enter a description for this Check-in.','','Forecast is stormy (<=56%) and their Satisfaction is off by more than 2 segments greater 28%)',0),
	(40,'checkin_3','Are you sure you want to cancel this entire Check-in?  If not, touch Cancel and either assign it to a Need or Skip.','','No Check-ins exist message',0),
	(41,'checkin_5','Great job staying aware of which needs matter to you! Remember to rate this Need for any of your other relationship profiles.','','Needs Message - Oldest Needs improvement <= 30 days and  1 - 3 Needs need Improvement',0),
	(42,'checkin_6','Unassigning this this will not delete the Need but it could affect your Forecast. Sure you want to unassign this Need from the Check-in?','','Needs Message - Oldest Needs improvement <= 30 days and  4 or more Needs need Improvement',0),
	(43,'checkin_7','This Check-in is related to a Need, so deletion could affect that Needs weather slider rating.   Save to proceed, or Cancel to not delete the Check-in.','','Needs Message - Oldest Needs improvement > 30 days and <= 90 days',0),
	(44,'need_3','Success! Changing the Need category updates it across all relationship profiles, as well as any Check-ins that are assigned to this Need.','','Needs Message - Oldest Needs improvement > 90 days',0),
	(45,'','','','Check-ins Assigned to a Need Message >= 80% over last 60 days',0),
	(46,'need_6','unused need_6','','Check-ins Assigned to a Need Message < 80% over last 60 days',0),
	(47,'checkup_1','Alert! This change has not been saved. Are you sure you want to cancel this change?','','Imbalance in Check-in type Message >= Check-ins sunny',0),
	(48,'checkup_2','That\'s wonderful! Would you tell us how it helped you?','','Imbalance in Check-in type Message >= Check-ins stormy',0),
	(49,'checkup_3','Sorry that we didn\'t meet your expectations. Please tell us what we could have done better.','','No forecast history exists (hasn\'t passed an end of month)',0),
	(50,'global_2','Oops!  This error isn\'t defined.  We\'re going to look into this immediately.  Sorry for any problem this is causing you.','','No Check-ins exist',0),
	(51,'checkin_4','Select one of your existing Needs for this Check-in, add a new one \"+\", or skip if this doesn\'t matter for your needs.','','Needs Message - No Needs Need Improvement',0),
	(52,'global_1','Your device doesn\'t have a good Internet connection so the App won\'t work well. Please  try again when you the connection is faster.','','Your device has lost it\'s connection to the Internet. Please reconnect and try again.',0),
	(53,'faq_1','Is my information private?',NULL,'Yes your information is private.',1),
	(56,'faq_2','how do I Check-In?',NULL,'Check-in by spinning the wheel, then entering text',1),
	(57,'faq_3','What are Gotta Haves?',NULL,'Gotta Haves are things that you can\'t live without.',1),
	(58,'sign_4','Uh-oh! Your password reset request has expired. Please touch Forgot Password to request it again.',NULL,NULL,0),
	(59,'reg_3','The email addresses doesn\'t match.  Please check them and try again.',NULL,NULL,0),
	(60,'reg_5','Your password needs to be between 4 and 10 characters, and is case sensitive.',NULL,NULL,0),
	(61,'mon_3','There isn\'t any Forecast history yet.  Records are created each month end as part of the monthly relationship Check-up process.',NULL,NULL,0),
	(62,'checkup_4','No Check-ins were created this month.  If you\'re no longer involved with this person, please consider ending your relationship in Settings/Relationships.',NULL,NULL,0),
	(63,'checkup_5','Less than 80% of your Check-ins are related to your Needs.  It\'s important to remember doing that shows whether your experiences support your Needs or not, and also allows you to become more aware of Needs you might not have realized.',NULL,NULL,0),
	(64,'checkup_6','Most of your Check-ins are related to Needs.  That\'s great!  This is really important, as it allows us to auto-adjust your Needs satisfaction, which impacts your relationship Forecast, based on what you\'re experiencing.',NULL,NULL,0),
	(65,'mon_10','Your relationship future looks about bright as it can be, as your Needs are being met exceptionally well!',NULL,NULL,0),
	(66,'mon_11','Your relationship future looks really good, as most of your Needs are being  well satisfied!',NULL,NULL,0),
	(67,'mon_12','Your relationship future looks good, as many of your Needs are being supported.',NULL,NULL,0),
	(68,'mon_13','Your relationship future outlook is borderline, as your Needs are only partially being met.',NULL,NULL,0),
	(69,'mon_14','Your relationship future is being adversely impacted by enough Needs not being met to a decent degree.',NULL,NULL,0),
	(70,'mon_15','Your relationship future donâ€™t look very promising right now, as large gaps exist between the Needs you want and what you\'re experiencing.',NULL,NULL,0),
	(71,'mon_16','Your relationship future is in severe jeopardy, as you have many significant gaps between the Needs you want and what you\'re experiencing.',NULL,NULL,0),
	(72,'mon_17','Balanced: Your feelings are in line with your experiences, and support a sunny relatonship future.  Keep Checking-in to ensure any changes in your Needs are kept current, and that your experiences continue to inform you.',NULL,NULL,0),
	(73,'mon_18','Balanced: Your feelings are in line with your  experiences, although unfortuantely your relationship isn\'t meeting your Needs.  This awareness helps you make informed decisions about your relationship.  Browse our Guidance section, and hopefully things will improve.',NULL,NULL,0),
	(74,'mon_19','You\'re feeling even better about your relationship than your sunny Forecast   indicates.  Make sure you\'re Checking-in all of your sunny experiences and relating them to your Needs.',NULL,NULL,0),
	(75,'mon_20','You\'re feeling better about your relationship than your stormy Forecast  indicates.    Make sure you\'re Checking-in all of your sunny experiences and relating them to your Needs.',NULL,NULL,0),
	(76,'mon_21','You\'re feeling worse about your relationship than your sunny Forecast indicates.  Make sure you\'re Checking-in your stormy experiences and relating them to your Needs.',NULL,NULL,0),
	(77,'mon_22','You\'re feeling even worse about your relationship than your stormy Forecast indicates.  Make sure you\'re Checking-in your stormy experiences and relating them to your Needs.',NULL,NULL,0),
	(78,'mon_23','All of your Needs are being met. That\'s terrific!  Keep it going by Checking-in to stay aware of how well your Needs are being supported, or discover new Needs, and catch any issues before they become an issue.',NULL,NULL,0),
	(79,'mon_24','Most of your Needs are being met.  You can focus on trying to make things even better by addressing any stormy Needs in the screens that follow.  Look for advice in the Relationship Guidance area in Settings/Get Help.',NULL,NULL,0),
	(80,'mon_25','You have 3 or less Needs that can be improved on the screens to follow. Try to focus on and improve them to get your Forecast into a sunny outlook.  Look for   advice in the Relationship Guidance area in Settings/Get Help.',NULL,NULL,0),
	(81,'mon_26','You have more than 3 Needs that can be improved.  In the screens that follow, try to focus on and improve only a few  at a time.  Look for advice in the Relationship Guidance area in Settings/Get Help, and pay attention to whether things are improving over time.',NULL,NULL,0),
	(82,'mon_27','You have 3 or less Needs that are very stormy and not being  met in the screens that follow.  See if you and your partner are willing to work together to improve the likelihood of a sunny future. Look for advice in the Relationship Guidance area in Settings/Get Help.',NULL,NULL,0),
	(83,'mon_28','Unless you\'re not entering any positive experiences, you have some thinking to do about this relationship.  In the screens that follow, focus on the most significant Needs that are not being met, as this isn\'t a healthy situation. Look for advice in the Relationship Guidance area in Settings/Get Help.',NULL,NULL,0),
	(84,'mon_29','You haven\'t created any Check-ins yet.  This is really important in order to build awareness about your relationship experiences to see how well they support your Needs, or whether you have discovered any you didn\'t realize you had.',NULL,NULL,0),
	(85,'mon_30','A large percentage of Check-ins are not related to your Needs.  That prevents us from auto-adjusting your Need fulfilment, which impacts your Forecast.  Please review your Check-ins and relate them to your Needs if they were meaningful experiences.',NULL,NULL,0),
	(86,'mon_31','Most of your Check-ins are being related to Needs, which is terrific!  This builds your awareness and allows us to auto-adjust your Need fulfilment, which impacts your Forecast.',NULL,NULL,0),
	(87,'global_3','The system is undergoing maintenance, and we expect it to be available at 3PM EST/2PM CST/12PM PST).  Sorry for any inconvenience!',NULL,NULL,0),
	(88,'global_4','We have an improved version of the app that you must download from the app store.  You can Sign-in after you do that.  Thanks!',NULL,NULL,0),
	(89,'reg_15','Oops, you didn\'t select any Gotta Not Have needs from the get started pick list! \r\n\r\nIf any descriptions were close ,you use the back arrow to select them, and can edit the description in \"My Needs\" at any time.\r\n\r\nOtherwise you can add individual Needs at another time.',NULL,NULL,0),
	(90,'reg_16','NOTE  - only needed if we decide to display Next before they touch all Needs they picked! \r\n\r\nSorry!  You missed evaluating some of your Needs.  That\'s necessary to start off with good awareness.\r\n\r\nPlease touch any Need without a weather icon, and move the slider to reflect your current perspective.',NULL,NULL,0),
	(91,'global_10','unused global_10',NULL,NULL,0),
	(92,'reg_14','Oops, you didn\'t select any Like to Have needs from the get started pick list! \r\n\r\nIf any were close you use the back arrow to select them, and can edit the description in \"My Needs\" at any time.\r\n\r\nOtherwise you can add individual Needs at another time.',NULL,NULL,0),
	(93,'global_9','unused global_9',NULL,NULL,0),
	(94,'reg_17','Since you\'re not dating anyone right now, there\'s no sense in assessing your Needs so we\'ll skip that step.  \r\n\r\nYou\'ll be asked to evaluate your Needs once you start dating and add a new relationship.',NULL,NULL,0),
	(95,'reg_18','Since you\'re not dating anyone seriously, there\'s no sense in assessing your Needs so we\'ll skip that step.  \r\n\r\nWhen you want to evaluate someone more seriously, you should add a new relationship and will be prompted to review your Needs at that time.',NULL,NULL,0),
	(96,'reg_19','Oops, you didn\'t select any Needs from the get started pick lists! \r\n\r\nIf any descriptions were close, you use the back arrow to select them, and can edit the description in \"My Needs\" at any time.\r\n\r\nOtherwise you can add individual Needs at another time.',NULL,NULL,0),
	(97,'global_8','unused global_8',NULL,NULL,0),
	(98,'global_7','unused global_7',NULL,NULL,0),
	(99,'global_6','unused global_6',NULL,NULL,0),
	(100,'global_5','unused global_5',NULL,NULL,0),
	(101,'checkup_7','You don\'t have a prior month forecast for this relationship.  Once you do, you\'ll see how your forecast changes each month.',NULL,NULL,0),
	(102,'checkup_8','Congratulations! Your forecast has improved since last month.  Focus on improving any Needs that are not totally sunny to make things even better.',NULL,NULL,0),
	(103,'checkup_9','It looks like your experiences last month didn\'t support your Needs that well.  Review your stormy Check-ins to see what happened, and see if you can turn things around next month.',NULL,NULL,0),
	(104,'checkup_10','Your forecast appears to be steady and hasn\'t changed much.  That means that your overall Need fulfilment is about the same as last month.',NULL,NULL,0);

/*!40000 ALTER TABLE `q0_iacs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table q0_trinities
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_trinities`;

CREATE TABLE `q0_trinities` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `abbreviation` char(3) NOT NULL,
  `name` varchar(15) NOT NULL,
  `max_value` int(11) DEFAULT NULL COMMENT '500 for GNH, 200 GH, 50 like to have (I think) ',
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified_by_user_id` int(11) DEFAULT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `trinities_id_unique` (`id`),
  UNIQUE KEY `trinities_name__unique` (`name`),
  UNIQUE KEY `trinities_abbreviation_unique` (`abbreviation`),
  KEY `trinities_createdby_idx` (`user_id`),
  KEY `trinities_modifiedby_idx` (`modified_by_user_id`),
  CONSTRAINT `trinities_createdby` FOREIGN KEY (`user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `trinities_modifiedby` FOREIGN KEY (`modified_by_user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='There are three Trinity types.  Each has a set of mathmatical values and these are set in this table.  It may be neccessary to have a history of these in which case a candidate key will include the trinity name and date of the change';

LOCK TABLES `q0_trinities` WRITE;
/*!40000 ALTER TABLE `q0_trinities` DISABLE KEYS */;

INSERT INTO `q0_trinities` (`id`, `abbreviation`, `name`, `max_value`, `user_id`, `created`, `modified_by_user_id`, `modified`)
VALUES
	(1,'GNH','gotta not have',500,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
	(2,'GH','gotta have',200,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
	(3,'LTH','like to have',50,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00');

/*!40000 ALTER TABLE `q0_trinities` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table q0_user_group_permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_user_group_permissions`;

CREATE TABLE `q0_user_group_permissions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_group_id` int(11) NOT NULL,
  `controller` varchar(50) NOT NULL,
  `action` varchar(100) NOT NULL,
  `allowed` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `user_group_permissions_groupKEY` (`user_group_id`),
  CONSTRAINT `user_group_permissions_groupFK` FOREIGN KEY (`user_group_id`) REFERENCES `q0_user_groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `q0_user_group_permissions` WRITE;
/*!40000 ALTER TABLE `q0_user_group_permissions` DISABLE KEYS */;

INSERT INTO `q0_user_group_permissions` (`id`, `user_group_id`, `controller`, `action`, `allowed`)
VALUES
	(1,1,'Pages','display',1),
	(2,2,'Pages','display',1),
	(3,3,'Pages','display',1),
	(4,1,'UserGroupPermissions','index',1),
	(5,2,'UserGroupPermissions','index',0),
	(6,3,'UserGroupPermissions','index',0),
	(7,1,'UserGroups','index',1),
	(8,2,'UserGroups','index',0),
	(9,3,'UserGroups','index',0),
	(10,1,'UserGroups','addGroup',1),
	(11,2,'UserGroups','addGroup',0),
	(12,3,'UserGroups','addGroup',0),
	(13,1,'UserGroups','editGroup',1),
	(14,2,'UserGroups','editGroup',0),
	(15,3,'UserGroups','editGroup',0),
	(16,1,'UserGroups','deleteGroup',1),
	(17,2,'UserGroups','deleteGroup',0),
	(18,3,'UserGroups','deleteGroup',0),
	(19,1,'UserSettings','index',1),
	(20,2,'UserSettings','index',0),
	(21,3,'UserSettings','index',0),
	(22,1,'UserSettings','editSetting',1),
	(23,2,'UserSettings','editSetting',0),
	(24,3,'UserSettings','editSetting',0),
	(25,1,'Users','index',1),
	(26,2,'Users','index',0),
	(27,3,'Users','index',0),
	(28,1,'Users','online',1),
	(29,2,'Users','online',0),
	(30,3,'Users','online',0),
	(31,1,'Users','viewUser',1),
	(32,2,'Users','viewUser',0),
	(33,3,'Users','viewUser',0),
	(34,1,'Users','myprofile',0),
	(35,2,'Users','myprofile',1),
	(36,3,'Users','myprofile',0),
	(37,1,'Users','editProfile',1),
	(38,2,'Users','editProfile',1),
	(39,3,'Users','editProfile',0),
	(40,1,'Users','login',1),
	(41,2,'Users','login',1),
	(42,3,'Users','login',1),
	(43,1,'Users','logout',1),
	(44,2,'Users','logout',1),
	(45,3,'Users','logout',1),
	(46,1,'Users','register',1),
	(47,2,'Users','register',1),
	(48,3,'Users','register',1),
	(49,1,'Users','changePassword',1),
	(50,2,'Users','changePassword',1),
	(51,3,'Users','changePassword',0),
	(52,1,'Users','changeUserPassword',1),
	(53,2,'Users','changeUserPassword',0),
	(54,3,'Users','changeUserPassword',0),
	(55,1,'Users','addUser',1),
	(56,2,'Users','addUser',0),
	(57,3,'Users','addUser',0),
	(58,1,'Users','editUser',1),
	(59,2,'Users','editUser',0),
	(60,3,'Users','editUser',0),
	(61,1,'Users','deleteUser',1),
	(62,2,'Users','deleteUser',0),
	(63,3,'Users','deleteUser',0),
	(64,1,'Users','deleteAccount',0),
	(65,2,'Users','deleteAccount',1),
	(66,3,'Users','deleteAccount',0),
	(67,1,'Users','logoutUser',1),
	(68,2,'Users','logoutUser',0),
	(69,3,'Users','logoutUser',0),
	(70,1,'Users','makeInactive',1),
	(71,2,'Users','makeInactive',0),
	(72,3,'Users','makeInactive',0),
	(73,1,'Users','dashboard',1),
	(74,2,'Users','dashboard',1),
	(75,3,'Users','dashboard',1),
	(76,1,'Users','makeActiveInactive',1),
	(77,2,'Users','makeActiveInactive',0),
	(78,3,'Users','makeActiveInactive',0),
	(79,1,'Users','verifyEmail',1),
	(80,2,'Users','verifyEmail',0),
	(81,3,'Users','verifyEmail',0),
	(82,1,'Users','accessDenied',1),
	(83,2,'Users','accessDenied',1),
	(84,3,'Users','accessDenied',0),
	(85,1,'Users','userVerification',1),
	(86,2,'Users','userVerification',1),
	(87,3,'Users','userVerification',1),
	(88,1,'Users','forgotPassword',1),
	(89,2,'Users','forgotPassword',1),
	(90,3,'Users','forgotPassword',1),
	(91,1,'Users','emailVerification',1),
	(92,2,'Users','emailVerification',1),
	(93,3,'Users','emailVerification',1),
	(94,1,'Users','activatePassword',1),
	(95,2,'Users','activatePassword',1),
	(96,3,'Users','activatePassword',1),
	(97,1,'UserGroupPermissions','update',1),
	(98,2,'UserGroupPermissions','update',0),
	(99,3,'UserGroupPermissions','update',0),
	(100,1,'Users','deleteCache',1),
	(101,2,'Users','deleteCache',0),
	(102,3,'Users','deleteCache',0),
	(103,1,'Autocomplete','fetch',1),
	(104,2,'Autocomplete','fetch',1),
	(105,3,'Autocomplete','fetch',1),
	(106,1,'Users','viewUserPermissions',1),
	(107,2,'Users','viewUserPermissions',0),
	(108,3,'Users','viewUserPermissions',0),
	(109,1,'Contents','index',1),
	(110,2,'Contents','index',0),
	(111,3,'Contents','index',0),
	(112,1,'Contents','addPage',1),
	(113,2,'Contents','addPage',0),
	(114,3,'Contents','addPage',0),
	(115,1,'Contents','editPage',1),
	(116,2,'Contents','editPage',0),
	(117,3,'Contents','editPage',0),
	(118,1,'Contents','viewPage',1),
	(119,2,'Contents','viewPage',0),
	(120,3,'Contents','viewPage',0),
	(121,1,'Contents','deletePage',1),
	(122,2,'Contents','deletePage',0),
	(123,3,'Contents','deletePage',0),
	(124,1,'Contents','content',1),
	(125,2,'Contents','content',1),
	(126,3,'Contents','content',1),
	(127,1,'UserContacts','index',1),
	(128,2,'UserContacts','index',0),
	(129,3,'UserContacts','index',0),
	(130,1,'UserContacts','contactUs',1),
	(131,2,'UserContacts','contactUs',1),
	(132,3,'UserContacts','contactUs',1),
	(133,1,'Users','ajaxLoginRedirect',1),
	(134,2,'Users','ajaxLoginRedirect',1),
	(135,3,'Users','ajaxLoginRedirect',1),
	(136,1,'Users','viewProfile',1),
	(137,2,'Users','viewProfile',1),
	(138,3,'Users','viewProfile',1),
	(139,1,'Users','sendMails',1),
	(140,2,'Users','sendMails',0),
	(141,3,'Users','sendMails',0),
	(142,1,'Users','searchEmails',1),
	(143,2,'Users','searchEmails',0),
	(144,3,'Users','searchEmails',0),
	(145,1,'UserEmails','index',1),
	(146,1,'UserEmails','send',1),
	(147,1,'UserEmails','sendToUser',1),
	(148,1,'UserEmails','sendReply',1),
	(149,1,'UserEmails','view',1),
	(150,1,'UserGroupPermissions','subPermissions',1),
	(151,1,'UserGroupPermissions','getPermissions',1),
	(152,1,'UserGroupPermissions','permissionGroupMatrix',1),
	(153,1,'UserGroupPermissions','permissionSubGroupMatrix',1),
	(154,1,'UserGroupPermissions','changePermission',1),
	(155,1,'Users','indexSearch',1),
	(156,1,'UserEmailSignatures','index',1),
	(157,1,'UserEmailSignatures','add',1),
	(158,1,'UserEmailSignatures','edit',1),
	(159,1,'UserEmailSignatures','delete',1),
	(160,1,'UserEmailTemplates','index',1),
	(161,1,'UserEmailTemplates','add',1),
	(162,1,'UserEmailTemplates','edit',1),
	(163,1,'UserEmailTemplates','delete',1),
	(164,1,'UserSettings','cakelog',1),
	(165,1,'UserSettings','cakelogbackup',1),
	(166,1,'UserSettings','cakelogdelete',1),
	(167,1,'UserSettings','cakelogempty',1),
	(168,1,'Users','addMultipleUsers',1),
	(169,1,'Users','uploadCsv',1);

/*!40000 ALTER TABLE `q0_user_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table q0_user_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_user_groups`;

CREATE TABLE `q0_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `alias_name` varchar(100) DEFAULT NULL,
  `description` text,
  `allowRegistration` int(1) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_group_selfjoinKEY` (`parent_id`),
  CONSTRAINT `user_group_selfjoinFK` FOREIGN KEY (`parent_id`) REFERENCES `q0_user_groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `q0_user_groups` WRITE;
/*!40000 ALTER TABLE `q0_user_groups` DISABLE KEYS */;

INSERT INTO `q0_user_groups` (`id`, `parent_id`, `name`, `alias_name`, `description`, `allowRegistration`, `created`, `modified`)
VALUES
	(1,NULL,'Admin','Admin',NULL,0,'2014-11-18 08:24:38','2014-11-18 08:24:38'),
	(2,NULL,'User','User',NULL,1,'2014-11-18 08:24:38','2014-11-18 08:24:38'),
	(3,NULL,'Guest','Guest',NULL,0,'2014-11-18 08:24:38','2014-11-18 08:24:38');

/*!40000 ALTER TABLE `q0_user_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table q0_user_settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_user_settings`;

CREATE TABLE `q0_user_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `name_public` text,
  `value` varchar(256) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `category` varchar(20) DEFAULT 'OTHER',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `q0_user_settings` WRITE;
/*!40000 ALTER TABLE `q0_user_settings` DISABLE KEYS */;

INSERT INTO `q0_user_settings` (`id`, `name`, `name_public`, `value`, `type`, `category`)
VALUES
	(1,'defaultTimeZone','Enter default time zone identifier','America/New_York','input','OTHER'),
	(2,'siteName','Enter Your Site Name','User Management Plugin','input','OTHER'),
	(3,'siteRegistration','New Registration is allowed or not','1','checkbox','USER'),
	(4,'allowDeleteAccount','Allow users to delete account','0','checkbox','USER'),
	(5,'sendRegistrationMail','Send Registration Mail After User Registered','0','checkbox','EMAIL'),
	(6,'sendPasswordChangeMail','Send Password Change Mail After User changed password','0','checkbox','EMAIL'),
	(7,'emailVerification','Want to verify user\'s email address?','0','checkbox','EMAIL'),
	(8,'emailFromAddress','Enter email by which emails will be send.','example@example.com','input','EMAIL'),
	(9,'emailFromName','Enter Email From Name','BRB','input','EMAIL'),
	(10,'allowChangeUsername','Do you want to allow users to change their username?','1','checkbox','USER'),
	(11,'bannedUsernames','Set banned usernames comma separated(no space, no quotes)','Administrator, SuperAdmin','input','USER'),
	(12,'useRecaptcha','Do you want to add captcha support on registration form, contact us form, login form in case bad logins, forgot password page, email verification page? Please note we have separate settings for all pages to Add or Remove captcha.','0','checkbox','RECAPTCHA'),
	(13,'privateKeyFromRecaptcha','Enter private key for Recaptcha from google','','input','RECAPTCHA'),
	(14,'publicKeyFromRecaptcha','Enter public key for recaptcha from google','','input','RECAPTCHA'),
	(15,'loginRedirectUrl','Enter URL where user will be redirected after login ','/dashboard','input','OTHER'),
	(16,'logoutRedirectUrl','Enter URL where user will be redirected after logout','/login','input','OTHER'),
	(17,'permissions','Do you Want to enable permissions for users?','0','checkbox','PERMISSION'),
	(18,'adminPermissions','Do you want to check permissions for Admin?','0','checkbox','PERMISSION'),
	(19,'defaultGroupId','Enter default group id for user registration','2','input','GROUP'),
	(20,'adminGroupId','Enter Admin Group Id','1','input','GROUP'),
	(21,'guestGroupId','Enter Guest Group Id','3','input','GROUP'),
	(22,'useFacebookLogin','Want to use Facebook Connect on your site?','0','checkbox','FACEBOOK'),
	(23,'facebookAppId','Facebook Application Id','','input','FACEBOOK'),
	(24,'facebookSecret','Facebook Application Secret Code','','input','FACEBOOK'),
	(25,'facebookScope','Facebook Permissions','user_status, publish_stream, email','input','FACEBOOK'),
	(26,'useTwitterLogin','Want to use Twitter Connect on your site?','0','checkbox','TWITTER'),
	(27,'twitterConsumerKey','Twitter Consumer Key','','input','TWITTER'),
	(28,'twitterConsumerSecret','Twitter Consumer Secret','','input','TWITTER'),
	(29,'useGmailLogin','Want to use Gmail Connect on your site?','1','checkbox','GOOGLE'),
	(30,'useYahooLogin','Want to use Yahoo Connect on your site?','1','checkbox','YAHOO'),
	(31,'useLinkedinLogin','Want to use Linkedin Connect on your site?','0','checkbox','LINKEDIN'),
	(32,'linkedinApiKey','Linkedin Api Key','','input','LINKEDIN'),
	(33,'linkedinSecretKey','Linkedin Secret Key','','input','LINKEDIN'),
	(34,'useFoursquareLogin','Want to use Foursquare Connect on your site?','0','checkbox','FOURSQUARE'),
	(35,'foursquareClientId','Foursquare Client Id','','input','FOURSQUARE'),
	(36,'foursquareClientSecret','Foursquare Client Secret','','input','FOURSQUARE'),
	(37,'viewOnlineUserTime','You can view online users and guest from last few minutes, set time in minutes ','30','input','USER'),
	(38,'useHttps','Do you want to HTTPS for whole site?','0','checkbox','OTHER'),
	(39,'httpsUrls','You can set selected urls for HTTPS (e.g. users/login, users/register)',NULL,'input','OTHER'),
	(40,'imgDir','Enter Image directory name where users profile photos will be uploaded. This directory should be in webroot/img directory','umphotos','input','OTHER'),
	(41,'QRDN','Increase this number by 1 every time if you made any changes in CSS or JS file','12345678','input','OTHER'),
	(42,'cookieName','Please enter cookie name for your site which is used to login user automatically for remember me functionality','UMPremiumCookie','input','OTHER'),
	(43,'adminEmailAddress','Admin Email address for emails','','input','EMAIL'),
	(44,'useRecaptchaOnLogin','Do you want to add captcha support on login form in case bad logins? For this feature you must have Captcha setting ON with valid private and public keys.','1','checkbox','RECAPTCHA'),
	(45,'badLoginAllowCount','Set number of allowed bad logins. for e.g. 5 or 10. For this feature you must have Captcha setting ON with valid private and public keys.','5','input','RECAPTCHA'),
	(46,'useRecaptchaOnRegistration','Do you want to add captcha support on registration form? For this feature you must have Captcha setting ON with valid private and public keys.','1','checkbox','RECAPTCHA'),
	(47,'useRecaptchaOnForgotPassword','Do you want to add captcha support on forgot password page? For this feature you must have Captcha setting ON with valid private and public keys.','1','checkbox','RECAPTCHA'),
	(48,'useRecaptchaOnEmailVerification','Do you want to add captcha support on email verification page? For this feature you must have Captcha setting ON with valid private and public keys.','1','checkbox','RECAPTCHA'),
	(49,'useRememberMe','Set true/false if you want to add/remove remember me feature on login page','1','checkbox','USER'),
	(50,'allowUserMultipleLogin','Do you want to allow multiple logins with same user account for users(not admin)?','1','checkbox','USER'),
	(51,'allowAdminMultipleLogin','Do you want to allow multiple logins with same user account for admin(not users)?','1','checkbox','USER'),
	(52,'loginIdleTime','Set max idle time in minutes for user. This idle time will be used when multiple logins are not allowed for same user account. If max idle time reached since user last activity on site then anyone can login with same account in other browser and idle user will be logged out.','10','input','USER');

/*!40000 ALTER TABLE `q0_user_settings` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;




-- create a user called BRB_System that is the createdby and modifiedby for the initialized data
--    it will have the same password as the male_default user
insert into q0_users 
			(user_group_id, 
			username, 
			password, 
			salt, 
			email, 
			active, 
			email_verified, 
			created, 
			modified)
	values( 2,
			"BRB_System", 
			"2818ae22524cc65160848a0c6cf14aafd700910d3982a44a62f5d8fd90a59a0a", 
			"Ag3Zjd7AmAtAo9mZXAhvkvhKffnG23PpIggTqS1O4y9cvL06fFVGvPgfVlQoxucg", 
			"system@brb.com",
			1,
			1,
			now(),
			now());


select @SystemUserId := id from q0_users where username = "BRB_System";

-- User needs a status so this is the initial status 
Insert into q0_statuses 
			(relationship_status, 
			user_id, 
			created, 
			modified_by_user_id, 
			modified)
		select 1,
			id,
			now(),
			id,
			now() 
	from q0_users 
		where username = "BRB_System";

-- add Initialize's profile
insert into q0_user_profiles 
		(status_id,birth_day,gender,interested_in,divorced,tandc_accepted_date, city, state, country, user_id, created, modified_by_user_id, modified)
		select 1,"19900101",1,2,0,now(),"New York","New York","United States",id, now(),id,now() from q0_users where username = "BRB_System";


-- add Initilize default relationship
insert into q0_relationships (name, start_date, is_default,user_id, created, modified_by_user_id, modified)
		select "< me >",curdate(),1,id, now(),id,now() from q0_users where username = "BRB_System";;


set FOREIGN_KEY_CHECKS = 1;
 
