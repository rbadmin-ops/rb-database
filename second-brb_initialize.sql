# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: brb-dev.czsiaxnynump.us-east-1.rds.amazonaws.com (MySQL 5.6.21-log)
# Database: brb_initialize
# Generation Time: 2015-05-28 05:06:06 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE USER 'DoubleAngel'@'localhost' IDENTIFIED BY 'Craw1Fish';
GRANT ALL PRIVILEGES ON *.* TO 'DoubleAngle'@'localhost' WITH GRANT OPTION;
CREATE USER 'DoubleAngel'@'%' IDENTIFIED BY 'Craw1Fish';
GRANT ALL PRIVILEGES ON *.* TO 'DoubleAngle'@'%'  WITH GRANT OPTION;


# Dump of table checkins_data
# ------------------------------------------------------------

DROP VIEW IF EXISTS `checkins_data`;

CREATE TABLE `checkins_data` (
   `checkins_id` INT(11) NOT NULL DEFAULT '0',
   `checkins_users_need_id` INT(11) NULL DEFAULT NULL,
   `checkins_relationship_id` INT(11) NOT NULL,
   `checkins_wheel_setting` TINYINT(4) NOT NULL,
   `checkin_date` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
   `checkins_comment` VARCHAR(2048) NOT NULL,
   `checkins_deleted_checkin` TINYINT(1) NOT NULL DEFAULT '0',
   `checkins_user_id` INT(11) NOT NULL,
   `checkins_created` DATETIME NOT NULL,
   `checkins_modified_by` INT(11) NULL DEFAULT NULL,
   `checkins_modified` DATETIME NOT NULL,
   `relationships_id` INT(11) NOT NULL DEFAULT '0',
   `relationships_name` VARCHAR(100) NULL DEFAULT NULL,
   `relationships_start_date` DATE NOT NULL,
   `relationships_end_date` DATE NULL DEFAULT NULL,
   `relationships_is_default` TINYINT(1) NOT NULL DEFAULT '0',
   `relationships_user_id` INT(11) NOT NULL,
   `relationships_created` DATETIME NOT NULL,
   `relationships_modified_by` INT(11) NULL DEFAULT NULL,
   `relationships_modified` DATETIME NOT NULL,
   `users_needs_id` INT(11) NULL DEFAULT '0',
   `users_needs_need_id` INT(11) NULL DEFAULT NULL,
   `users_needs_picked_need` INT(11) NULL DEFAULT NULL,
   `users_needs_not_longer_needed` TINYINT(1) NULL DEFAULT '0',
   `users_needs_user_id` INT(11) NULL DEFAULT NULL,
   `users_needs_created` DATETIME NULL DEFAULT NULL,
   `users_needs_modified_by` INT(11) NULL DEFAULT NULL,
   `users_needs_modified` DATETIME NULL DEFAULT NULL,
   `needs_id` INT(11) NULL DEFAULT '0',
   `needs_name` VARCHAR(50) NULL DEFAULT '',
   `needs_trinity_id` TINYINT(4) NULL DEFAULT NULL,
   `needs_show_as_public` TINYINT(1) NULL DEFAULT '0',
   `needs_user_id` INT(11) NULL DEFAULT NULL,
   `needs_created` DATETIME NULL DEFAULT NULL,
   `needs_modified_by` INT(11) NULL DEFAULT NULL,
   `needs_modified` DATETIME NULL DEFAULT NULL
) ENGINE=MyISAM;



# Dump of table diaguserneedsallinfo
# ------------------------------------------------------------

DROP VIEW IF EXISTS `diaguserneedsallinfo`;

CREATE TABLE `diaguserneedsallinfo` (
   `users_id` INT(11) NOT NULL DEFAULT '0',
   `users_username` VARCHAR(100) NULL DEFAULT NULL,
   `users_password` VARCHAR(255) NULL DEFAULT NULL,
   `users_salt` VARCHAR(100) NULL DEFAULT NULL,
   `users_email` VARCHAR(100) NULL DEFAULT NULL,
   `users_active` VARCHAR(3) NULL DEFAULT '0',
   `users_last_login` DATETIME NULL DEFAULT NULL,
   `users_ip_address` VARCHAR(50) NULL DEFAULT NULL,
   `users_created` DATETIME NULL DEFAULT NULL,
   `users_modified` DATETIME NULL DEFAULT NULL,
   `usersneeds_id` INT(11) NULL DEFAULT '0',
   `userneeds_needid` INT(11) NULL DEFAULT NULL,
   `usersneeds_no_longer_needed` TINYINT(1) NULL DEFAULT '0',
   `usersneeds_no_longer_needed_date` DATETIME NULL DEFAULT NULL,
   `userneeds_Userid` INT(11) NULL DEFAULT NULL,
   `userneeds_created` DATETIME NULL DEFAULT NULL,
   `userneeds_modifiedby` INT(11) NULL DEFAULT NULL,
   `userneeds_modified` DATETIME NULL DEFAULT NULL,
   `needs_Id` INT(11) NULL DEFAULT '0',
   `needs_name` VARCHAR(50) NULL DEFAULT '',
   `needs_trinity_id` TINYINT(4) NULL DEFAULT NULL,
   `needs_show_as_public` TINYINT(1) NULL DEFAULT '0',
   `needs_user_id` INT(11) NULL DEFAULT NULL,
   `needs_created` DATETIME NULL DEFAULT NULL,
   `needs_modifiedby` INT(11) NULL DEFAULT NULL,
   `needs_modified` DATETIME NULL DEFAULT NULL,
   `needslider_id` INT(11) NULL DEFAULT '0',
   `needslider_relationshipid` INT(11) NULL DEFAULT NULL,
   `needslider_user_need_id` INT(11) NULL DEFAULT NULL,
   `needslider_slider_value` FLOAT NULL DEFAULT NULL,
   `needslider_turned_stormy` DATETIME NULL DEFAULT NULL,
   `nneedslider_UserId` INT(11) NULL DEFAULT NULL,
   `needslider_created` DATETIME NULL DEFAULT NULL,
   `needslider_modifiedby` INT(11) NULL DEFAULT NULL,
   `needslider_modified` DATETIME NULL DEFAULT NULL,
   `relationship_id` INT(11) NULL DEFAULT '0',
   `relationship_name` VARCHAR(100) NULL DEFAULT NULL,
   `relationship_startdate` DATE NULL DEFAULT NULL,
   `relationship_enddate` DATE NULL DEFAULT NULL,
   `relationship_isdefault` TINYINT(1) NULL DEFAULT '0',
   `relationship_userid` INT(11) NULL DEFAULT NULL,
   `relationship_created` DATETIME NULL DEFAULT NULL,
   `relationship_modifiedby` INT(11) NULL DEFAULT NULL,
   `relationship_modified` DATETIME NULL DEFAULT NULL
) ENGINE=MyISAM;



# Dump of table DiagUserNeedsAllInfo
# ------------------------------------------------------------

DROP VIEW IF EXISTS `DiagUserNeedsAllInfo`;

CREATE TABLE `DiagUserNeedsAllInfo` (
   `Users_ID` INT(11) NOT NULL DEFAULT '0',
   `Users_username` VARCHAR(100) NULL DEFAULT NULL,
   `Users_password` VARCHAR(255) NULL DEFAULT NULL,
   `Users_salt` VARCHAR(100) NULL DEFAULT NULL,
   `Users_email` VARCHAR(100) NULL DEFAULT NULL,
   `Users_active` VARCHAR(3) NULL DEFAULT '0',
   `Users_last_login` DATETIME NULL DEFAULT NULL,
   `Users_ip_address` VARCHAR(50) NULL DEFAULT NULL,
   `Users_Created` DATETIME NULL DEFAULT NULL,
   `Users_Modified` DATETIME NULL DEFAULT NULL,
   `UsersNeeds_ID` INT(11) NULL DEFAULT '0',
   `UserNeeds_NeedID` INT(11) NULL DEFAULT NULL,
   `UsersNeeds_no_longer_needed` TINYINT(1) NULL DEFAULT '0',
   `UsersNeeds_no_longer_needed_date` DATETIME NULL DEFAULT NULL,
   `UserNeeds_UserID` INT(11) NULL DEFAULT NULL,
   `UserNeeds_Created` DATETIME NULL DEFAULT NULL,
   `UserNeeds_ModifiedBy` INT(11) NULL DEFAULT NULL,
   `UserNeeds_Modified` DATETIME NULL DEFAULT NULL,
   `Needs_Id` INT(11) NULL DEFAULT '0',
   `Needs_name` VARCHAR(50) NULL DEFAULT '',
   `Needs_trinity_id` TINYINT(4) NULL DEFAULT NULL,
   `Needs_show_as_public` TINYINT(1) NULL DEFAULT '0',
   `Needs_user_id` INT(11) NULL DEFAULT NULL,
   `Needs_created` DATETIME NULL DEFAULT NULL,
   `Needs_ModifiedBy` INT(11) NULL DEFAULT NULL,
   `Needs_Modified` DATETIME NULL DEFAULT NULL,
   `Needslider_id` INT(11) NULL DEFAULT '0',
   `Needslider_relationshipid` INT(11) NULL DEFAULT NULL,
   `Needslider_user_need_id` INT(11) NULL DEFAULT NULL,
   `Needslider_slider_value` FLOAT NULL DEFAULT NULL,
   `Needslider_turned_stormy` DATETIME NULL DEFAULT NULL,
   `nNeedslider_UserId` INT(11) NULL DEFAULT NULL,
   `Needslider_Created` DATETIME NULL DEFAULT NULL,
   `Needslider_ModifiedBy` INT(11) NULL DEFAULT NULL,
   `Needslider_Modified` DATETIME NULL DEFAULT NULL,
   `Relationship_id` INT(11) NULL DEFAULT '0',
   `Relationship_name` VARCHAR(100) NULL DEFAULT NULL,
   `Relationship_startdate` DATE NULL DEFAULT NULL,
   `Relationship_enddate` DATE NULL DEFAULT NULL,
   `Relationship_isdefault` TINYINT(1) NULL DEFAULT '0',
   `Relationship_userid` INT(11) NULL DEFAULT NULL,
   `Relationship_created` DATETIME NULL DEFAULT NULL,
   `Relationship_modifiedby` INT(11) NULL DEFAULT NULL,
   `Relationship_modified` DATETIME NULL DEFAULT NULL
) ENGINE=MyISAM;



# Dump of table ForecastAndRelationshipDates
# ------------------------------------------------------------

DROP VIEW IF EXISTS `ForecastAndRelationshipDates`;

CREATE TABLE `ForecastAndRelationshipDates` (
   `User_Id` INT(11) NOT NULL DEFAULT '0',
   `Relationship_Id` INT(11) NOT NULL DEFAULT '0',
   `LastForecastDate` DATETIME NULL DEFAULT NULL,
   `EndedDate` DATETIME NULL DEFAULT NULL
) ENGINE=MyISAM;



# Dump of table q0_administrative_audits
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_administrative_audits`;

CREATE TABLE `q0_administrative_audits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `table_name` varchar(100) NOT NULL,
  `column_name` varchar(100) NOT NULL,
  `record_id` int(11) NOT NULL,
  `from` text,
  `to` text,
  `user_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified_by_user_id` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `administrative_audits_createdby_idx` (`user_id`),
  KEY `administrative_audits_modifiedby_idx` (`modified_by_user_id`),
  CONSTRAINT `administrative_audits_createdby` FOREIGN KEY (`user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `administrative_audits_modifiedby` FOREIGN KEY (`modified_by_user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='All column data will be changed to character data for this audit and entered in the from and to fields.  We are not trying to make a rollback facility here.  We just want to know who changed what and when';



# Dump of table q0_advice_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_advice_categories`;

CREATE TABLE `q0_advice_categories` (
  `id` int(11) NOT NULL,
  `category` char(25) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified_by_user_id` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `category_UNIQUE` (`category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='As we learn more about how people use advice the ability to easily add categories without a code build seems to make sense, hence a table to hold the category names rather than enumeration	';



# Dump of table q0_advice_flags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_advice_flags`;

CREATE TABLE `q0_advice_flags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `flag_type` binary(1) NOT NULL COMMENT 'enumeration of flag  1 = like   2= Flag as problem',
  `advice_item_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified_by_user_id` int(11) DEFAULT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_unique` (`id`),
  UNIQUE KEY `user_item_type_unq` (`user_id`,`advice_item_id`,`flag_type`),
  KEY `advice_items_flags_idx` (`advice_item_id`),
  KEY `advice_flags_createdby_idx` (`user_id`),
  KEY `advice_flags_modifiedby_idx` (`modified_by_user_id`),
  CONSTRAINT `advice_flags_createdby` FOREIGN KEY (`user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `advice_flags_modifiedby` FOREIGN KEY (`modified_by_user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `advice_item_flagged` FOREIGN KEY (`advice_item_id`) REFERENCES `q0_advice_items` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Keeps track of what advice items users like and/or are problems.';



# Dump of table q0_advice_items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_advice_items`;

CREATE TABLE `q0_advice_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `advice_type` tinyint(4) NOT NULL COMMENT 'An Enum 1=essay 2=poll, 3 =Current poll of the week, 4=past or future poll of the week',
  `item_text` text NOT NULL COMMENT 'Question text goes here.  For responses to type Essay questions the text goes here.  for responses to poll type questions this field will be blank',
  `user_age` int(11) NOT NULL COMMENT 'age of the user as of the date they created this item, calculated from birthday',
  `user_gender` char(1) NOT NULL COMMENT 'gender of the user creating item',
  `user_interestedin` char(1) NOT NULL COMMENT 'interestedin setting of user creating this item at time they created it',
  `user_relationship_status` char(1) NOT NULL COMMENT 'users relationship status at the time they created this item',
  `admin_problem_flag` char(1) DEFAULT NULL COMMENT 'Items can be flagged by a user in which case they are not shown to that user anymore.  This flag is  one flagged by administration will not be shown to anyone.  could be a setting for don''t show this single item or don''t show this item and all children (future)',
  `admin_problem_set_date` datetime DEFAULT NULL,
  `poll_answer` bit(1) DEFAULT NULL COMMENT '0 = No, 1 = Yes   For essay questions this will be null.',
  `poll_of_week_started_on` date DEFAULT NULL,
  `poll_of_week_ended_on` date DEFAULT NULL,
  `q0_advice_item_id` int(11) DEFAULT NULL COMMENT 'If null then this is an original question, if not then this is a response to the question pointed to by this id',
  `needslider_id` int(11) DEFAULT NULL COMMENT 'Optional field to be filled in when the user posted this question from the needs screen. It is possible for the need that this points to to be deleted ',
  `needslider_value` int(11) DEFAULT NULL COMMENT 'optional as well, slider value at the tie of tying this advice question to this need',
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified_by_user_id` int(11) DEFAULT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `advice_itemsResponse_idx` (`q0_advice_item_id`),
  KEY `advice_itemscreatedby_idx` (`user_id`),
  KEY `advice_itemsNeed_idx` (`needslider_id`),
  KEY `advice_itemsmodifiedby_idx` (`modified_by_user_id`),
  CONSTRAINT `advice_itemsNeed` FOREIGN KEY (`needslider_id`) REFERENCES `q0_needsliders` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `advice_itemsResponse` FOREIGN KEY (`q0_advice_item_id`) REFERENCES `q0_advice_items` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `advice_items_createdby` FOREIGN KEY (`user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `advice_itemsmodifiedby` FOREIGN KEY (`modified_by_user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Questions and followup messages for the Advice feature,  This table is a self-join from the answers up to the question or to the answer to an answer.';



# Dump of table q0_advice_reads
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_advice_reads`;

CREATE TABLE `q0_advice_reads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `q0_advice_readscol` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='User/AdviceItems association for read items.  This get''s knarly.  Count of not read replys for people you are following is all the replys to questions that you are following that have been created after you started follwing that do not have a record here for having been read.   New is the same way but with another attribute ';



# Dump of table q0_checkins
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_checkins`;

CREATE TABLE `q0_checkins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_need_id` int(11) DEFAULT NULL,
  `relationship_id` int(11) NOT NULL,
  `wheel_setting` tinyint(4) NOT NULL COMMENT 'This is the rating for this check in either Good or Bad from 1 to 7',
  `checkin_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `checkin_type` int(11) NOT NULL DEFAULT '0' COMMENT '0 = regular checkin; 1 = restart relationship; 2 = end relationship',
  `comment` varchar(2048) NOT NULL,
  `deleted_checkin` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified_by_user_id` int(11) DEFAULT NULL,
  `modified` datetime NOT NULL,
  `day` smallint(4) NOT NULL,
  `week` smallint(4) NOT NULL,
  `year` smallint(4) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_unique` (`id`),
  KEY `checkins_relationship_idx` (`relationship_id`),
  KEY `checkins_users_needs` (`users_need_id`),
  KEY `checkins_createdby` (`user_id`),
  KEY `checkins_modifiedby` (`modified_by_user_id`),
  CONSTRAINT `checkins_createdby` FOREIGN KEY (`user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `checkins_modifiedby` FOREIGN KEY (`modified_by_user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `checkins_relationship` FOREIGN KEY (`relationship_id`) REFERENCES `q0_relationships` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `checkins_users_needs` FOREIGN KEY (`users_need_id`) REFERENCES `q0_users_needs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Checkin is the act of telling the system what the state of the relationship is right now.  A check in may be tied to a specific need or just in general. It is always about a specific relationship.  It may or may not result in a change of state in that relationship.	';



# Dump of table q0_contents
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_contents`;

CREATE TABLE `q0_contents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_name` text,
  `url_name` text,
  `page_content` text,
  `page_title` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table q0_enumerations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_enumerations`;

CREATE TABLE `q0_enumerations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_name` varchar(45) NOT NULL COMMENT 'Name used in the program for the enumerations, which should match the field in the database name.  Names should all be very easy to understand what table and field they are enumerating. Another option is two fields, table name and field name.',
  `word` varchar(45) NOT NULL COMMENT 'word associated with the value, such as married, single etc.',
  `value` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified_by_user_id` int(11) DEFAULT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `enumerations_createdby` (`user_id`),
  KEY `enumerations_modifiedby_idx` (`modified_by_user_id`),
  CONSTRAINT `enumerations_createdby` FOREIGN KEY (`user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `enumerations_modifiedby` FOREIGN KEY (`modified_by_user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='This table is useful for working in the database only.  The enumerations I have outlined will be defined in the program but look like magic numbers here unless there is a "decoder ring".  This is the decoder ring';

LOCK TABLES `q0_enumerations` WRITE;
/*!40000 ALTER TABLE `q0_enumerations` DISABLE KEYS */;

INSERT INTO `q0_enumerations` (`id`, `field_name`, `word`, `value`, `user_id`, `created`, `modified_by_user_id`, `modified`)
VALUES
	(2,'needsliders_histories.checkin_move','no',0,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
	(3,'needsliders_histories.checkin_move','yes',1,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
	(4,'needsliders_histories.checkin_move','moved by delete',2,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
	(5,'trinities.abbreviation','GNH',1,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
	(6,'trinities.abbreviation','GH',2,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
	(7,'trinities.abbreviation','LTH',3,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
	(8,'needsliders_histories.checkin_move','moved by need edit trinity change',4,1,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00'),
	(9,'needsliders_histories.checkin_move','moved by checkin edit',3,1,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00'),
	(10,'checkins.checkin_type','Regular',0,1,'2015-04-01 00:56:11',1,'2015-04-01 00:56:11'),
	(11,'checkins.checkin_type','Restarted',1,1,'2015-04-01 00:56:11',1,'2015-04-01 00:56:11'),
	(12,'checkins.checkin_type','Ended',2,1,'2015-04-01 00:56:11',1,'2015-04-01 00:56:11'),
	(14,'neesliders_histories.checkin_move','moved by monthly checkup user',5,1,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00');

/*!40000 ALTER TABLE `q0_enumerations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table q0_faqs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_faqs`;

CREATE TABLE `q0_faqs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(150) NOT NULL,
  `functional_area` varchar(45) DEFAULT NULL,
  `answer` text,
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified_by_user_id` int(11) DEFAULT NULL,
  `modified` datetime NOT NULL,
  `key` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `faqs_createdby` (`user_id`),
  KEY `faqs_modifiedby_idx` (`modified_by_user_id`),
  CONSTRAINT `faqs_createdby` FOREIGN KEY (`user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `faqs_modifiedby` FOREIGN KEY (`modified_by_user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='I have not added search indexing pending knowing the tool we are going to use	';

LOCK TABLES `q0_faqs` WRITE;
/*!40000 ALTER TABLE `q0_faqs` DISABLE KEYS */;

INSERT INTO `q0_faqs` (`id`, `subject`, `functional_area`, `answer`, `user_id`, `created`, `modified_by_user_id`, `modified`, `key`)
VALUES
	(2,'Is my information private?','Global','Your information will not be provided to any third party, and will only be used for our internal purposes ... take from the legal document...',1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00','faq_1'),
	(3,'What is a Check-In?','Check-in','A Check-in provides you with the ability to create a history of significant events that happen in your relationship.  This, over time, allows you to see patterns in your relationship in the Monitor Check-in History related to how many sunny vs. stormy interactions are occurring.  If you do this consistently, you\'ll be able to see how well your relationship is actually meeting what you\'re looking for, and will ultimately impact your probability of long term relationship health and fulfillment.',1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00','faq_2'),
	(4,'What are Gotta Have\'s?','My Needs','take from our in-app messaging and help',1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00','faq_3'),
	(5,'How can I change my username?','Registration','You can\'t change your username at this time.  If this is important to you, let us know through Settings/Feedback.',1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00','faq_4'),
	(6,'Why can\'t people under 18 register?','Registration','There may be Needs entered by adults that would be inappropriate for minors to be exposed to.   We\'re a conservative group when it comes to protecting children.',1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00','faq_5'),
	(7,'How can I delete a relationship profile?','Global','You can end a relationship in Settings, which will stop the monthly check-up process, but you can\'t delete a past relationship.  As painful as it might have been, it\'s crucial to have that information to reflect on things, learn from them, and continue to build upon that learning.',1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00','faq_6'),
	(8,'Why does my relationship have a Monthly Check Up?','Monitor','We want to try and ensure that you get a chance to reflect on how your relationship is progressing, and whether it\'s getting better or wore over time, to allow you to be informed and make the best decision you can.  A monthly process just provides a structure to do that.  You should, however, not wait until the end of the month to see how things are going, adjust your Needs, and Check-in moments you experience that impact how you feel!',1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00','faq_7'),
	(9,'Why are my demographics needed in registration?','Registration','We are going to be continuously improving our forecasting calculation, which could be impacted by your demographics as well as by the community of user information that we aggregate to improve forecasting.  We also plan on introducing enhancements that will utilize your demographics, so better to get them in now!',1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00','faq_8'),
	(10,'How many Needs should I enter?','My Needs','You need at least three Needs to get a Forecast.  How many you enter is entirely up to you, and should reflect what really matters to you.',1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00','faq_9'),
	(11,'How do I know what my Needs should be?','My Needs','Reflect on your past relationship experiences.  Aspects you really loved can help determine your Gotta Have\'s; things that you really disliked deeply are your Gotta Not Have\'s; and things you often enjoyed doing are your Like to Have\'s.  You can also become aware of new Needs as you create Check-ins.',1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00','faq_10'),
	(12,'Why don\'t my unsatisfied \"Like to Have\" Needs show up as Needing Improvement?','My Needs','Like to Have\'s are not fundamental Needs.  You could have a lot of Like to Have\'s, but none of them individually are crucial to your relationship satisfaction, so listing all of those as needing improvement isn\'t really helpful to you.  While they do affect your Forecast, they have a much smaller impact than your Gotta Have and Gotta Not Have Needs.',1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00','faq_11'),
	(13,'Why do I have the same needs for all of my relationships?','My Needs','Your Needs reflect what matters most to you in your relationship.  They should not vary by person, although how each person meets your Needs certainly can vary.',1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00','faq_12'),
	(14,'Why do my changes to my Needs continue to be reflected in my ended  relationships?','My Needs','Showing a Forecast based on Needs that no longer matter just didn\'t seem appropriate.  You don\'t have to go in and assess Needs for those ended relationships, as the slider will be positioned on partly sunny, but it could be interesting to do that if you\'d like to see how the person would have satisfied your current Needs.',1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00','faq_13'),
	(15,'Why do I go through a Monthly  Check-up if I just started a relationship before the end of the month?','Global','The Monthly Check-up review occurs as a consistent reminder for you develop self-awareness about how each relationship is meeting your needs.  By showing you where your Needs stand, asking you how you feel about the relationship, and showing you a monthly summary, you begin to get used to the process from the start of a relationship, rather than just going along without guidance.',1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00','faq_14'),
	(16,'What\'s the difference between the Monthly Check-up/Forecast History summary and the Monitor Forecast Summary?','Global','The Monthly Summary shows how your relationship progressed over the month, noting what\'s sunnier or stormier, while your Forecast Summary is a recap supporting the reasons why your overall Forecast is pointed where it is.  \r\n\r\nYou might have had a stormy month, but your overall relationship could still be sunny based on everything that you\'ve experienced overall!',1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00','faq_15'),
	(17,'How do the weather symbols for a Check-in affect my relationship forecast?','Check-in','If you assign the Check-in to a Need, it will affect how well that Need is being satisfied depending on how much sunnier or stormier that Check-in is relative to  the Need weather icon before the Check-in.  In this manner, your Check-ins automatically adjust your Need satisfaction based on what you experience, and therefore impact your forecast!',1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00','faq_16'),
	(18,'What are Gotta Not Have\'s?','My Needs','take from Help',1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00','faq_17'),
	(19,'What are Like to Have\'s?','My Needs','take from Help',1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00','faq_18'),
	(20,'Why should my username be anonymous?','Global','We plan on adding features that will let users communicate with each other, and the anonymity will protect your identity considering you may be dealing with sensitive topics related to your or their relationship.',1,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00','faq_19');

/*!40000 ALTER TABLE `q0_faqs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table q0_follows
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_follows`;

CREATE TABLE `q0_follows` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `following_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified_by_user_id` int(11) DEFAULT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_unique` (`id`),
  UNIQUE KEY `i_follow_you_unq` (`user_id`,`following_id`),
  KEY `user_is_following_idx` (`following_id`),
  KEY `follows_createdby` (`user_id`),
  KEY `follows_modifiedby_idx` (`modified_by_user_id`),
  CONSTRAINT `follows_createdby` FOREIGN KEY (`user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `follows_modifiedby` FOREIGN KEY (`modified_by_user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_is_following` FOREIGN KEY (`following_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Keeps track of who is following who';



# Dump of table q0_forecasts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_forecasts`;

CREATE TABLE `q0_forecasts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relationship_id` int(11) NOT NULL COMMENT 'Relationship this health check applies to',
  `forecastsValue` decimal(10,2) NOT NULL COMMENT 'This is the calculated RelationshipForecast value for the period from the needslider values.',
  `forecast_date` datetime NOT NULL COMMENT 'The date the forecast was done OR the last day of previous month if the forecast is done by the service.',
  `has_this_helped` tinyint(4) DEFAULT NULL,
  `has_this_helped_comment` varchar(1024) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified_by_user_id` int(11) DEFAULT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_unique` (`id`),
  KEY `forecasts_relationships_idx` (`relationship_id`),
  KEY `forecasts_createdby_idx` (`user_id`),
  KEY `forecasts_modifiedby_idx` (`modified_by_user_id`),
  CONSTRAINT `forecasts_createdby` FOREIGN KEY (`user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `forecasts_modifiedby` FOREIGN KEY (`modified_by_user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `forecastsrelationship` FOREIGN KEY (`relationship_id`) REFERENCES `q0_relationships` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Holds the monthly check value for each relationship. See the FR documents for details on timing and math';

LOCK TABLES `q0_forecasts` WRITE;
/*!40000 ALTER TABLE `q0_forecasts` DISABLE KEYS */;

INSERT INTO `q0_forecasts` (`id`, `relationship_id`, `forecastsValue`, `forecast_date`, `has_this_helped`, `has_this_helped_comment`, `user_id`, `created`, `modified_by_user_id`, `modified`)
VALUES
	(1,3,60.40,'2015-04-29 16:43:13',NULL,NULL,2,'2015-04-29 16:43:13',2,'2015-05-23 16:43:13'),
	(2,2,63.90,'2015-04-29 16:43:13',NULL,NULL,3,'2015-04-29 16:43:13',3,'2015-05-23 16:43:13'),
	(3,4,63.40,'2015-04-29 16:43:14',NULL,NULL,5,'2015-04-29 16:43:14',5,'2015-05-23 16:43:14'),
	(4,5,50.00,'2015-04-29 16:43:15',NULL,NULL,6,'2015-04-29 16:43:15',6,'2015-05-23 16:43:15'),
	(5,37,50.00,'2015-04-29 16:43:17',NULL,NULL,6,'2015-04-29 16:43:17',6,'2015-05-23 16:43:17'),
	(6,38,50.00,'2015-04-29 16:43:20',NULL,NULL,6,'2015-04-29 16:43:20',6,'2015-05-23 16:43:20'),
	(7,39,50.00,'2015-04-29 16:43:22',NULL,NULL,6,'2015-04-29 16:43:22',6,'2015-05-23 16:43:22'),
	(8,40,56.60,'2015-04-29 16:43:24',NULL,NULL,6,'2015-04-29 16:43:24',6,'2015-05-23 16:43:24'),
	(9,41,50.00,'2015-04-29 16:43:27',NULL,NULL,6,'2015-04-29 16:43:27',6,'2015-05-23 16:43:27'),
	(10,42,50.00,'2015-04-29 16:43:29',NULL,NULL,6,'2015-04-29 16:43:29',6,'2015-05-23 16:43:29'),
	(11,43,50.00,'2015-04-29 16:43:31',NULL,NULL,6,'2015-04-29 16:43:31',6,'2015-05-23 16:43:31'),
	(12,60,50.00,'2015-04-29 16:43:34',NULL,NULL,6,'2015-04-29 16:43:34',6,'2015-05-23 16:43:34'),
	(13,85,53.90,'2015-04-29 16:43:36',NULL,NULL,6,'2015-04-29 16:43:36',6,'2015-05-23 16:43:36'),
	(14,6,40.20,'2015-04-29 16:43:39',NULL,NULL,7,'2015-04-29 16:43:39',7,'2015-05-23 16:43:39'),
	(15,17,51.60,'2015-04-29 16:43:40',NULL,NULL,7,'2015-04-29 16:43:40',7,'2015-05-23 16:43:40'),
	(16,18,51.60,'2015-04-29 16:43:41',NULL,NULL,7,'2015-04-29 16:43:41',7,'2015-05-23 16:43:41'),
	(17,19,51.60,'2015-04-29 16:43:43',NULL,NULL,7,'2015-04-29 16:43:43',7,'2015-05-23 16:43:43'),
	(18,20,54.80,'2015-04-29 16:43:44',NULL,NULL,7,'2015-04-29 16:43:44',7,'2015-05-23 16:43:44'),
	(19,27,51.60,'2015-04-29 16:43:46',NULL,NULL,7,'2015-04-29 16:43:46',7,'2015-05-23 16:43:46'),
	(20,28,46.50,'2015-04-29 16:43:47',NULL,NULL,7,'2015-04-29 16:43:47',7,'2015-05-23 16:43:47'),
	(21,36,51.60,'2015-04-29 16:43:49',NULL,NULL,7,'2015-04-29 16:43:49',7,'2015-05-23 16:43:49'),
	(22,62,84.20,'2015-04-29 16:43:50',NULL,NULL,7,'2015-04-29 16:43:50',7,'2015-05-23 16:43:50'),
	(23,64,54.80,'2015-04-29 16:43:52',NULL,NULL,7,'2015-04-29 16:43:52',7,'2015-05-23 16:43:52'),
	(24,65,54.90,'2015-04-29 16:43:53',NULL,NULL,7,'2015-04-29 16:43:53',7,'2015-05-23 16:43:53'),
	(25,74,15.20,'2015-04-29 16:43:55',NULL,NULL,7,'2015-04-29 16:43:55',7,'2015-05-23 16:43:55'),
	(26,87,56.40,'2015-04-29 16:43:56',NULL,NULL,7,'2015-04-29 16:43:56',7,'2015-05-23 16:43:56'),
	(27,10,63.90,'2015-04-29 16:43:58',NULL,NULL,10,'2015-04-29 16:43:58',10,'2015-05-23 16:43:58'),
	(28,12,59.30,'2015-04-29 16:43:59',NULL,NULL,11,'2015-04-29 16:43:59',11,'2015-05-23 16:43:59'),
	(29,11,60.40,'2015-04-29 16:43:59',NULL,NULL,12,'2015-04-29 16:43:59',12,'2015-05-23 16:43:59'),
	(30,13,64.90,'2015-04-29 16:44:00',NULL,NULL,13,'2015-04-29 16:44:00',13,'2015-05-23 16:44:00'),
	(31,14,63.90,'2015-04-29 16:44:01',NULL,NULL,14,'2015-04-29 16:44:01',14,'2015-05-23 16:44:01'),
	(32,15,63.90,'2015-04-29 16:44:01',NULL,NULL,16,'2015-04-29 16:44:01',16,'2015-05-23 16:44:01'),
	(33,44,63.90,'2015-04-29 16:44:02',NULL,NULL,17,'2015-04-29 16:44:02',17,'2015-05-23 16:44:02'),
	(34,16,63.90,'2015-04-29 16:44:02',NULL,NULL,18,'2015-04-29 16:44:02',18,'2015-05-23 16:44:02'),
	(35,21,60.40,'2015-04-29 16:44:04',NULL,NULL,29,'2015-04-29 16:44:04',29,'2015-05-23 16:44:04'),
	(36,22,57.00,'2015-04-29 16:44:04',NULL,NULL,29,'2015-04-29 16:44:04',29,'2015-05-23 16:44:04'),
	(37,23,60.40,'2015-04-29 16:44:05',NULL,NULL,30,'2015-04-29 16:44:05',30,'2015-05-23 16:44:05'),
	(38,24,57.00,'2015-04-29 16:44:05',NULL,NULL,30,'2015-04-29 16:44:05',30,'2015-05-23 16:44:05'),
	(39,25,60.40,'2015-04-29 16:44:06',NULL,NULL,31,'2015-04-29 16:44:06',31,'2015-05-23 16:44:06'),
	(40,26,57.00,'2015-04-29 16:44:07',NULL,NULL,31,'2015-04-29 16:44:07',31,'2015-05-23 16:44:07'),
	(41,29,27.60,'2015-04-29 16:44:07',NULL,NULL,32,'2015-04-29 16:44:07',32,'2015-05-23 16:44:07'),
	(42,30,41.90,'2015-04-29 16:44:08',NULL,NULL,34,'2015-04-29 16:44:08',34,'2015-05-23 16:44:08'),
	(43,31,53.40,'2015-04-29 16:44:08',NULL,NULL,34,'2015-04-29 16:44:08',34,'2015-05-23 16:44:08'),
	(44,33,53.40,'2015-04-29 16:44:09',NULL,NULL,34,'2015-04-29 16:44:09',34,'2015-05-23 16:44:09'),
	(45,34,46.90,'2015-04-29 16:44:10',NULL,NULL,36,'2015-04-29 16:44:10',36,'2015-05-23 16:44:10'),
	(46,45,58.30,'2015-04-29 16:44:11',NULL,NULL,38,'2015-04-29 16:44:11',38,'2015-05-23 16:44:11'),
	(47,46,57.20,'2015-04-29 16:44:12',NULL,NULL,41,'2015-04-29 16:44:12',41,'2015-05-23 16:44:12'),
	(48,47,63.90,'2015-04-29 16:44:12',NULL,NULL,43,'2015-04-29 16:44:12',43,'2015-05-23 16:44:12'),
	(49,48,63.90,'2015-04-29 16:44:13',NULL,NULL,45,'2015-04-29 16:44:13',45,'2015-05-23 16:44:13'),
	(50,49,60.40,'2015-04-29 16:44:13',NULL,NULL,47,'2015-04-29 16:44:13',47,'2015-05-23 16:44:13'),
	(51,50,57.40,'2015-04-29 16:44:14',NULL,NULL,48,'2015-04-29 16:44:14',48,'2015-05-23 16:44:14'),
	(52,51,60.40,'2015-04-29 16:44:15',NULL,NULL,50,'2015-04-29 16:44:15',50,'2015-05-23 16:44:15'),
	(53,52,57.00,'2015-04-29 16:44:15',NULL,NULL,50,'2015-04-29 16:44:15',50,'2015-05-23 16:44:15'),
	(54,53,60.40,'2015-04-29 16:44:16',NULL,NULL,52,'2015-04-29 16:44:16',52,'2015-05-23 16:44:16'),
	(55,54,60.40,'2015-04-29 16:44:17',NULL,NULL,53,'2015-04-29 16:44:17',53,'2015-05-23 16:44:17'),
	(56,55,55.60,'2015-04-29 16:44:17',NULL,NULL,54,'2015-04-29 16:44:17',54,'2015-05-23 16:44:17'),
	(57,57,65.20,'2015-04-29 16:44:18',NULL,NULL,56,'2015-04-29 16:44:18',56,'2015-05-23 16:44:18'),
	(58,58,63.90,'2015-04-29 16:44:18',NULL,NULL,58,'2015-04-29 16:44:18',58,'2015-05-23 16:44:18'),
	(59,59,60.40,'2015-04-29 16:44:19',NULL,NULL,64,'2015-04-29 16:44:19',64,'2015-05-23 16:44:19'),
	(60,61,60.40,'2015-04-29 16:44:20',NULL,NULL,68,'2015-04-29 16:44:20',68,'2015-05-23 16:44:20'),
	(61,63,60.40,'2015-04-29 16:44:21',NULL,NULL,70,'2015-04-29 16:44:21',70,'2015-05-23 16:44:21'),
	(62,68,60.40,'2015-04-29 16:44:21',NULL,NULL,71,'2015-04-29 16:44:21',71,'2015-05-23 16:44:21'),
	(63,69,60.40,'2015-04-29 16:44:22',NULL,NULL,72,'2015-04-29 16:44:22',72,'2015-05-23 16:44:22'),
	(64,70,63.90,'2015-04-29 16:44:23',NULL,NULL,73,'2015-04-29 16:44:23',73,'2015-05-23 16:44:23'),
	(65,71,63.90,'2015-04-29 16:44:23',NULL,NULL,74,'2015-04-29 16:44:23',74,'2015-05-23 16:44:23'),
	(66,72,0.00,'2015-04-29 16:44:24',NULL,NULL,80,'2015-04-29 16:44:24',80,'2015-05-23 16:44:24'),
	(67,73,60.40,'2015-04-29 16:44:24',NULL,NULL,82,'2015-04-29 16:44:24',82,'2015-05-23 16:44:24'),
	(68,76,50.00,'2015-04-29 16:44:25',NULL,NULL,83,'2015-04-29 16:44:25',83,'2015-05-23 16:44:25'),
	(69,80,50.80,'2015-04-29 16:44:26',NULL,NULL,83,'2015-04-29 16:44:26',83,'2015-05-23 16:44:26'),
	(70,78,37.00,'2015-04-29 16:44:27',NULL,NULL,85,'2015-04-29 16:44:27',85,'2015-05-23 16:44:27'),
	(71,79,50.00,'2015-04-29 16:44:27',NULL,NULL,85,'2015-04-29 16:44:27',85,'2015-05-23 16:44:27'),
	(72,81,54.50,'2015-04-29 16:44:28',NULL,NULL,87,'2015-04-29 16:44:28',87,'2015-05-23 16:44:28'),
	(73,82,48.10,'2015-04-29 16:44:29',NULL,NULL,96,'2015-04-29 16:44:29',96,'2015-05-23 16:44:29'),
	(74,83,60.40,'2015-04-29 16:44:29',NULL,NULL,97,'2015-04-29 16:44:29',97,'2015-05-23 16:44:29'),
	(75,84,60.40,'2015-04-29 16:44:30',NULL,NULL,101,'2015-04-29 16:44:30',101,'2015-05-23 16:44:30'),
	(76,88,60.40,'2015-04-29 16:44:31',NULL,NULL,103,'2015-04-29 16:44:31',103,'2015-05-23 16:44:31'),
	(77,89,60.40,'2015-04-29 16:44:32',NULL,NULL,104,'2015-04-29 16:44:32',104,'2015-05-23 16:44:32'),
	(78,90,60.40,'2015-04-29 16:44:32',NULL,NULL,105,'2015-04-29 16:44:32',105,'2015-05-23 16:44:32'),
	(79,91,60.40,'2015-04-29 16:44:33',NULL,NULL,106,'2015-04-29 16:44:33',106,'2015-05-23 16:44:33'),
	(80,92,60.40,'2015-04-29 16:44:33',NULL,NULL,107,'2015-04-29 16:44:33',107,'2015-05-23 16:44:33'),
	(81,93,60.40,'2015-04-29 16:44:34',NULL,NULL,110,'2015-04-29 16:44:34',110,'2015-05-23 16:44:34'),
	(82,94,63.90,'2015-04-29 16:44:34',NULL,NULL,111,'2015-04-29 16:44:34',111,'2015-05-23 16:44:34'),
	(83,95,50.00,'2015-04-29 16:44:35',NULL,NULL,112,'2015-04-29 16:44:35',112,'2015-05-23 16:44:35'),
	(84,97,49.90,'2015-04-29 16:44:36',NULL,NULL,113,'2015-04-29 16:44:36',113,'2015-05-23 16:44:36'),
	(85,96,60.40,'2015-04-29 16:44:36',NULL,NULL,114,'2015-04-29 16:44:36',114,'2015-05-23 16:44:36'),
	(86,99,57.00,'2015-04-29 16:44:37',NULL,NULL,115,'2015-04-29 16:44:37',115,'2015-05-23 16:44:37'),
	(87,100,60.40,'2015-04-29 16:44:38',NULL,NULL,116,'2015-04-29 16:44:38',116,'2015-05-23 16:44:38'),
	(88,101,50.00,'2015-04-29 16:44:38',NULL,NULL,117,'2015-04-29 16:44:38',117,'2015-05-23 16:44:38'),
	(89,102,60.40,'2015-04-29 16:44:40',NULL,NULL,118,'2015-04-29 16:44:40',118,'2015-05-23 16:44:40'),
	(90,3,60.40,'2015-05-23 19:49:00',NULL,NULL,2,'2015-05-23 19:49:00',2,'2015-05-23 19:49:00'),
	(91,2,63.90,'2015-05-23 19:49:01',NULL,NULL,3,'2015-05-23 19:49:01',3,'2015-05-23 19:49:01'),
	(92,4,63.40,'2015-05-23 19:49:02',NULL,NULL,5,'2015-05-23 19:49:02',5,'2015-05-23 19:49:02'),
	(93,5,50.00,'2015-05-23 19:49:02',NULL,NULL,6,'2015-05-23 19:49:02',6,'2015-05-23 19:49:02'),
	(94,37,50.00,'2015-05-23 19:49:05',NULL,NULL,6,'2015-05-23 19:49:05',6,'2015-05-23 19:49:05'),
	(95,42,50.00,'2015-05-23 19:49:07',NULL,NULL,6,'2015-05-23 19:49:07',6,'2015-05-23 19:49:07'),
	(96,6,40.20,'2015-05-23 19:49:09',NULL,NULL,7,'2015-05-23 19:49:09',7,'2015-05-23 19:49:09'),
	(97,18,51.60,'2015-05-23 19:49:11',NULL,NULL,7,'2015-05-23 19:49:11',7,'2015-05-23 19:49:11'),
	(98,19,51.60,'2015-05-23 19:49:12',NULL,NULL,7,'2015-05-23 19:49:12',7,'2015-05-23 19:49:12'),
	(99,27,51.60,'2015-05-23 19:49:13',NULL,NULL,7,'2015-05-23 19:49:13',7,'2015-05-23 19:49:13'),
	(100,62,84.20,'2015-05-23 19:49:15',NULL,NULL,7,'2015-05-23 19:49:15',7,'2015-05-23 19:49:15'),
	(101,65,54.90,'2015-05-23 19:49:16',NULL,NULL,7,'2015-05-23 19:49:16',7,'2015-05-23 19:49:16'),
	(102,74,15.20,'2015-05-23 19:49:18',NULL,NULL,7,'2015-05-23 19:49:18',7,'2015-05-23 19:49:18'),
	(103,87,56.40,'2015-05-23 19:49:19',NULL,NULL,7,'2015-05-23 19:49:19',7,'2015-05-23 19:49:19'),
	(104,12,59.30,'2015-05-23 19:49:21',NULL,NULL,11,'2015-05-23 19:49:21',11,'2015-05-23 19:49:21'),
	(105,11,60.40,'2015-05-23 19:49:21',NULL,NULL,12,'2015-05-23 19:49:21',12,'2015-05-23 19:49:21'),
	(106,13,64.90,'2015-05-23 19:49:22',NULL,NULL,13,'2015-05-23 19:49:22',13,'2015-05-23 19:49:22'),
	(107,14,63.90,'2015-05-23 19:49:23',NULL,NULL,14,'2015-05-23 19:49:23',14,'2015-05-23 19:49:23'),
	(108,15,63.90,'2015-05-23 19:49:23',NULL,NULL,16,'2015-05-23 19:49:23',16,'2015-05-23 19:49:23'),
	(109,44,63.90,'2015-05-23 19:49:24',NULL,NULL,17,'2015-05-23 19:49:24',17,'2015-05-23 19:49:24'),
	(110,16,63.90,'2015-05-23 19:49:24',NULL,NULL,18,'2015-05-23 19:49:24',18,'2015-05-23 19:49:24'),
	(111,21,60.40,'2015-05-23 19:49:25',NULL,NULL,29,'2015-05-23 19:49:25',29,'2015-05-23 19:49:25'),
	(112,22,57.00,'2015-05-23 19:49:26',NULL,NULL,29,'2015-05-23 19:49:26',29,'2015-05-23 19:49:26'),
	(113,23,60.40,'2015-05-23 19:49:27',NULL,NULL,30,'2015-05-23 19:49:27',30,'2015-05-23 19:49:27'),
	(114,24,57.00,'2015-05-23 19:49:27',NULL,NULL,30,'2015-05-23 19:49:27',30,'2015-05-23 19:49:27'),
	(115,25,60.40,'2015-05-23 19:49:28',NULL,NULL,31,'2015-05-23 19:49:28',31,'2015-05-23 19:49:28'),
	(116,26,57.00,'2015-05-23 19:49:28',NULL,NULL,31,'2015-05-23 19:49:28',31,'2015-05-23 19:49:28'),
	(117,29,27.60,'2015-05-23 19:49:29',NULL,NULL,32,'2015-05-23 19:49:29',32,'2015-05-23 19:49:29'),
	(118,30,41.90,'2015-05-23 19:49:29',NULL,NULL,34,'2015-05-23 19:49:29',34,'2015-05-23 19:49:29'),
	(119,31,53.40,'2015-05-23 19:49:30',NULL,NULL,34,'2015-05-23 19:49:30',34,'2015-05-23 19:49:30'),
	(120,45,58.30,'2015-05-23 19:49:31',NULL,NULL,38,'2015-05-23 19:49:31',38,'2015-05-23 19:49:31'),
	(121,46,57.20,'2015-05-23 19:49:32',NULL,NULL,41,'2015-05-23 19:49:32',41,'2015-05-23 19:49:32'),
	(122,47,63.90,'2015-05-23 19:49:32',NULL,NULL,43,'2015-05-23 19:49:32',43,'2015-05-23 19:49:32'),
	(123,48,63.90,'2015-05-23 19:49:33',NULL,NULL,45,'2015-05-23 19:49:33',45,'2015-05-23 19:49:33'),
	(124,49,60.40,'2015-05-23 19:49:33',NULL,NULL,47,'2015-05-23 19:49:33',47,'2015-05-23 19:49:33'),
	(125,50,57.40,'2015-05-23 19:49:34',NULL,NULL,48,'2015-05-23 19:49:34',48,'2015-05-23 19:49:34'),
	(126,51,60.40,'2015-05-23 19:49:35',NULL,NULL,50,'2015-05-23 19:49:35',50,'2015-05-23 19:49:35'),
	(127,52,57.00,'2015-05-23 19:49:35',NULL,NULL,50,'2015-05-23 19:49:35',50,'2015-05-23 19:49:35'),
	(128,53,60.40,'2015-05-23 19:49:36',NULL,NULL,52,'2015-05-23 19:49:36',52,'2015-05-23 19:49:36'),
	(129,54,60.40,'2015-05-23 19:49:36',NULL,NULL,53,'2015-05-23 19:49:36',53,'2015-05-23 19:49:36'),
	(130,55,55.60,'2015-05-23 19:49:37',NULL,NULL,54,'2015-05-23 19:49:37',54,'2015-05-23 19:49:37'),
	(131,57,65.20,'2015-05-23 19:49:38',NULL,NULL,56,'2015-05-23 19:49:38',56,'2015-05-23 19:49:38'),
	(132,58,63.90,'2015-05-23 19:49:38',NULL,NULL,58,'2015-05-23 19:49:38',58,'2015-05-23 19:49:38'),
	(133,59,60.40,'2015-05-23 19:49:39',NULL,NULL,64,'2015-05-23 19:49:39',64,'2015-05-23 19:49:39'),
	(134,61,60.40,'2015-05-23 19:49:40',NULL,NULL,68,'2015-05-23 19:49:40',68,'2015-05-23 19:49:40'),
	(135,63,60.40,'2015-05-23 19:49:40',NULL,NULL,70,'2015-05-23 19:49:40',70,'2015-05-23 19:49:40'),
	(136,68,60.40,'2015-05-23 19:49:41',NULL,NULL,71,'2015-05-23 19:49:41',71,'2015-05-23 19:49:41'),
	(137,69,60.40,'2015-05-23 19:49:41',NULL,NULL,72,'2015-05-23 19:49:41',72,'2015-05-23 19:49:41'),
	(138,70,63.90,'2015-05-23 19:49:42',NULL,NULL,73,'2015-05-23 19:49:42',73,'2015-05-23 19:49:42'),
	(139,71,63.90,'2015-05-23 19:49:42',NULL,NULL,74,'2015-05-23 19:49:42',74,'2015-05-23 19:49:42'),
	(140,72,0.00,'2015-05-23 19:49:43',NULL,NULL,80,'2015-05-23 19:49:43',80,'2015-05-23 19:49:43'),
	(141,73,60.40,'2015-05-23 19:49:43',NULL,NULL,82,'2015-05-23 19:49:43',82,'2015-05-23 19:49:43'),
	(142,76,50.00,'2015-05-23 19:49:44',NULL,NULL,83,'2015-05-23 19:49:44',83,'2015-05-23 19:49:44'),
	(143,80,50.80,'2015-05-23 19:49:45',NULL,NULL,83,'2015-05-23 19:49:45',83,'2015-05-23 19:49:45'),
	(144,78,37.00,'2015-05-23 19:49:45',NULL,NULL,85,'2015-05-23 19:49:45',85,'2015-05-23 19:49:45'),
	(145,79,50.00,'2015-05-23 19:49:46',NULL,NULL,85,'2015-05-23 19:49:46',85,'2015-05-23 19:49:46'),
	(146,81,54.50,'2015-05-23 19:49:47',NULL,NULL,87,'2015-05-23 19:49:47',87,'2015-05-23 19:49:47'),
	(147,82,48.10,'2015-05-23 19:49:47',NULL,NULL,96,'2015-05-23 19:49:47',96,'2015-05-23 19:49:47'),
	(148,83,60.40,'2015-05-23 19:49:48',NULL,NULL,97,'2015-05-23 19:49:48',97,'2015-05-23 19:49:48'),
	(149,84,60.40,'2015-05-23 19:49:49',NULL,NULL,101,'2015-05-23 19:49:49',101,'2015-05-23 19:49:49'),
	(150,88,60.40,'2015-05-23 19:49:50',NULL,NULL,103,'2015-05-23 19:49:50',103,'2015-05-23 19:49:50'),
	(151,89,60.40,'2015-05-23 19:49:50',NULL,NULL,104,'2015-05-23 19:49:50',104,'2015-05-23 19:49:50'),
	(152,90,60.40,'2015-05-23 19:49:51',NULL,NULL,105,'2015-05-23 19:49:51',105,'2015-05-23 19:49:51'),
	(153,91,60.40,'2015-05-23 19:49:51',NULL,NULL,106,'2015-05-23 19:49:51',106,'2015-05-23 19:49:51'),
	(154,92,60.40,'2015-05-23 19:49:52',NULL,NULL,107,'2015-05-23 19:49:52',107,'2015-05-23 19:49:52'),
	(155,93,60.40,'2015-05-23 19:49:53',NULL,NULL,110,'2015-05-23 19:49:53',110,'2015-05-23 19:49:53'),
	(156,94,63.90,'2015-05-23 19:49:53',NULL,NULL,111,'2015-05-23 19:49:53',111,'2015-05-23 19:49:53'),
	(157,95,50.00,'2015-05-23 19:49:54',NULL,NULL,112,'2015-05-23 19:49:54',112,'2015-05-23 19:49:54'),
	(158,97,49.90,'2015-05-23 19:49:54',NULL,NULL,113,'2015-05-23 19:49:54',113,'2015-05-23 19:49:54'),
	(159,96,60.40,'2015-05-23 19:49:55',NULL,NULL,114,'2015-05-23 19:49:55',114,'2015-05-23 19:49:55'),
	(160,99,57.00,'2015-05-23 19:49:56',NULL,NULL,115,'2015-05-23 19:49:56',115,'2015-05-23 19:49:56'),
	(161,100,60.40,'2015-05-23 19:49:56',NULL,NULL,116,'2015-05-23 19:49:56',116,'2015-05-23 19:49:56'),
	(162,101,38.00,'2015-05-23 19:49:57',NULL,NULL,117,'2015-05-23 19:49:57',117,'2015-05-23 19:49:57'),
	(163,102,60.40,'2015-05-23 19:49:58',NULL,NULL,118,'2015-05-23 19:49:58',118,'2015-05-23 19:49:58');

/*!40000 ALTER TABLE `q0_forecasts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table q0_forecasts_needsliders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_forecasts_needsliders`;

CREATE TABLE `q0_forecasts_needsliders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `needslider_id` int(11) NOT NULL,
  `forecast_id` int(11) NOT NULL,
  `Slider` float DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified_by_user_id` int(11) DEFAULT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `forecasts_needsliders_needslider_id_idx` (`needslider_id`),
  KEY `forecasts_needsliders_createdby` (`user_id`),
  KEY `forecasts_needsliders_modifiedby_idx` (`modified_by_user_id`),
  CONSTRAINT `forecasts_needsliders_createdby` FOREIGN KEY (`user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `forecasts_needsliders_modifiedby` FOREIGN KEY (`modified_by_user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `forecasts_needsliders_needslider_id` FOREIGN KEY (`needslider_id`) REFERENCES `q0_needsliders` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='This will hold the position of the need sliders for each need active at the time a snapshot is taken.  ';



# Dump of table q0_iacs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_iacs`;

CREATE TABLE `q0_iacs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(50) DEFAULT NULL,
  `value` text,
  `screenshot_url` varchar(255) DEFAULT NULL,
  `description` text,
  `faq` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `q0_iacs` WRITE;
/*!40000 ALTER TABLE `q0_iacs` DISABLE KEYS */;

INSERT INTO `q0_iacs` (`id`, `key`, `value`, `screenshot_url`, `description`, `faq`)
VALUES
	(5,'sign_1','Uh oh! The password is incorrect for this email address. Please re-enter the email address and password.  Touch Forgot Password if you forgot it!','','The email address isn\'t formatted properly',0),
	(9,'sign_2','There\'s no user registered for the email address you entered.  Please try again.','','This email address being used by another username',0),
	(10,'sign_3','A link to reset your password has been sent to your email address.','','The email address and username do not match',0),
	(12,'reg_1','This email address is already being used. Please register with a different email address.','','Your Password must be at least 4 characters',0),
	(13,'reg_2','Please enter a valid email address, such as you@gmail.com.','','Your passwords don\'t match, please re-enter them again',0),
	(14,'reg_4','Uh oh! Your passwords don\'t match. Please try again.','','Your Username can\'t be blank',0),
	(15,'reg_6','Your Username must be between 3-20 characters without any special characters (* ! % @).  And remember to make it anonymous.','','Relationship name cannot be blank',0),
	(16,'reg_7','Sorry... someone picked that great Username before you!  Please try another name.','','	 You\'re already using this relationship name',0),
	(17,'reg_8','THIS IS HARD CODED. Are you sure you want to cancel registration?','','Your Relationship Start date cannot be blank',0),
	(18,'set_2','Please enter at least one character for this relationship name','','Your Relationshp End date can not be before your Start date',0),
	(19,'reg_10','Please enter a valid zip code, or skip entering this item.','','Your Relationship End date cannot be in the future',0),
	(20,'reg_11','unused','','A Relationship has been created with your Username that yu can use to manage your Needs or Check-in non-relationship focused things',0),
	(21,'reg_12','unused','','We know you\'re just starting this relationship, but how satisfied are you with it at this time?',0),
	(22,'reg_13','unused','','Congratulations for completing registration and using the Relationship Barometer to help make your relationship the best it can be!',0),
	(23,'set_1','Oops! Did you forget your password? Please try again.','','Old Password isn\'t correct',0),
	(24,'need_7','unused','','The Need description cannot be blank',0),
	(25,'need_4','Alert! Changing the Need description affects any Check-ins assigned to this Need. If this is really a different Need, add a new one using the (+). Do you just want to change the description?','','Needs apply to all relationships, so remember to go into your other Relationships and assess how well this need is being satisfied for them.',0),
	(26,'need_5','unused','','Changing a Need type affects all Relationships, as well as any Check-ins assigned to that Need.',0),
	(27,'need_1','This Need will be removed from ALL relationship profiles and Check-ins, which may impact your Forecast. Are you sure it\'s no longer important to you?','','Deleting a Need deletes it for all Relationships, and causes any Check-ins assigned to that Need to no longer be assigned to any Need.',0),
	(28,'mon_1','Alert! Your history is only available after your first Check-up, which occurs at the end of each month.','','Display a message that 3 Needs are required for a forecast',0),
	(29,'mon_2','Alert! You don\'t have any Check-ins. You can view history after you create a couple. This will allow you to see your relationship trends.','','Display a message that only 1 Need is entered, and 3 are required for a forecast',0),
	(30,'set_3','Uh oh! You\'re already using this name for a relationship. Please choose a different username. You can even use a nickname.','','Display a message that only 2 Needs are entered, and 3 are required for a forecast',0),
	(31,'set_4','We hope this is a positive change for you!  While you can still add Check-ins and see your history, please restart this relationship if you get back together.','','The No Needs need Improvement message is displayed',0),
	(32,'set_5','Congrats! <Realtionship Name> has been restarted. We\'ll be here to help you as you continue to Love & Learn. We recommend rating My Needs now as some of them may have changed since you ended this relationship.','','Perceived Satisfaction has not been updated in over a month.',0),
	(33,'set_6','Congrats! Good luck with your new relationship.  Go to My Needs to rate them so you can get your first Forecast.','','Not enough data to see a Forecast History',0),
	(34,'set_7','It\'s good to add past relationships to reflect on what you can learn from them. If you\'re curious, go to My Needs to rate them so you can see how they rank against what matters to you now.','','Forecast is sunny (>56%) and their Satisfaction is within 1 segment <= 14%)',0),
	(35,'set_8','Success! Your Profile has been updated.','','Forecast is sunny (>56%) and their Satisfaction is within 2 segments (> 14% and <+ 28% )',0),
	(36,'mon_9','No Check-ins this week!  Were you bored?','','Forecast is sunny (>56%) and their Satisfaction is off by more than 2 segments greater 28%)',0),
	(37,'checkin_1','Whoops! This is not an active relationship. Please Restart this relationship in Settings if you would like to continue getting a history recorded for it.','','Forecast is stormy (<=56%) and their Satisfaction is within 1 segment <= 14%)',0),
	(38,'need_2','Please enter a description for this Need. Type a couple of letters to see suggested Needs that others have created.','','Forecast is stormy (<=56%) and their Satisfaction is within 2 segments (> 14% and <+ 28% )',0),
	(39,'checkin_2','Please enter a description for this Check-in.','','Forecast is stormy (<=56%) and their Satisfaction is off by more than 2 segments greater 28%)',0),
	(40,'checkin_3','Are you sure you want to cancel this entire Check-in?  If not, touch Cancel and either assign it to a Need or Skip.','','No Check-ins exist message',0),
	(41,'checkin_5','Great job staying aware of which needs matter to you! Remember to rate this Need for any of your other relationship profiles.','','Needs Message - Oldest Needs improvement <= 30 days and  1 - 3 Needs need Improvement',0),
	(42,'checkin_6','Unassigning this this will not delete the Need but it could affect your Forecast. Sure you want to unassign this Need from the Check-in?','','Needs Message - Oldest Needs improvement <= 30 days and  4 or more Needs need Improvement',0),
	(43,'checkin_7','Are you sure you want to delete this Check-in?  If it\'s assigned to a Need, it could also affect your Forecast.','','Needs Message - Oldest Needs improvement > 30 days and <= 90 days',0),
	(44,'need_3','Success! Changing the Need category updates it across all relationship profiles, as well as any Check-ins that are assigned to this Need.','','Needs Message - Oldest Needs improvement > 90 days',0),
	(45,'','','','Check-ins Assigned to a Need Message >= 80% over last 60 days',0),
	(46,'need_6','unused','','Check-ins Assigned to a Need Message < 80% over last 60 days',0),
	(47,'checkup_1','Alert! This change has not been saved. Are you sure you want to cancel this change?','','Imbalance in Check-in type Message >= Check-ins sunny',0),
	(48,'checkup_2','That\'s wonderful! Would you tell us how it helped you?','','Imbalance in Check-in type Message >= Check-ins stormy',0),
	(49,'checkup_3','Sorry that we didn\'t meet your expectations. Please tell us what we could have done better.','','No forecast history exists (hasn\'t passed an end of month)',0),
	(50,'global_2','Oops!  This error isn\'t defined.  We\'re going to look into this immediately.  Sorry for any problem this is causing you.','','No Check-ins exist',0),
	(51,'checkin_4','Select one of your existing Needs for this Check-in, add a new one \"+\", or skip if this doesn\'t matter for your needs.','','Needs Message - No Needs Need Improvement',0),
	(52,'global_1','Your device doesn\'t have a good Internet connection so the App won\'t work well. Please  try again when you the connection is faster.','','Your device has lost it\'s connection to the Internet. Please reconnect and try again.',0),
	(53,'faq_1','Is my information private?',NULL,'Yes your information is private.',1),
	(56,'faq_2','how do I Check-In?',NULL,'Check-in by spinning the wheel, then entering text',1),
	(57,'faq_3','What are Gotta Haves?',NULL,'Gotta Haves are things that you can\'t live without.',1),
	(58,'sign_4','Uh-oh! Your password reset request has expired. Please touch Forgot Password to request it again.',NULL,NULL,0),
	(59,'reg_3','The email addresses doesn\'t match.  Please check them and try again.',NULL,NULL,0),
	(60,'reg_5','Your password needs to be between 4 and 10 characters, and is case sensitive.',NULL,NULL,0);

/*!40000 ALTER TABLE `q0_iacs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table q0_licensemanage
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_licensemanage`;

CREATE TABLE `q0_licensemanage` (
  `id` int(11) NOT NULL,
  `product` varchar(45) NOT NULL,
  `type` int(11) NOT NULL COMMENT 'enum for the type of license.  GNU, PrivateCompany, Apache, etc.',
  `acquisition_date` date NOT NULL,
  `renewal_days` int(11) DEFAULT NULL,
  `publish_url` varchar(100) DEFAULT NULL,
  `license_text` text,
  `must_publish` bit(1) NOT NULL,
  `license_cost` varchar(45) NOT NULL COMMENT 'text description of initial cost and renewal or usage costs',
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_UNIQUE` (`product`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Record license information for all included products including which need to be published as part of the open or other license and which do not	';



# Dump of table q0_login_tokens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_login_tokens`;

CREATE TABLE `q0_login_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `token` char(32) NOT NULL,
  `duration` varchar(32) NOT NULL,
  `used` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `expires` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `login_tokens_userKEY` (`user_id`),
  CONSTRAINT `login_tokens_userFK` FOREIGN KEY (`user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table q0_monitor_usages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_monitor_usages`;

CREATE TABLE `q0_monitor_usages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relationship_id` int(11) NOT NULL,
  `monitor_function` int(11) NOT NULL COMMENT 'Use enums for the functions.',
  `gender` char(1) NOT NULL,
  `age` tinyint(4) NOT NULL,
  `relationship_status` tinyint(4) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified_by_user_id` int(11) NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `monitor_usages_relationshipid_idx` (`relationship_id`),
  KEY `monitor_usages_user_id_idx` (`user_id`),
  CONSTRAINT `monitor_usages_relationshipid` FOREIGN KEY (`relationship_id`) REFERENCES `q0_relationships` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `monitor_usages_user_id` FOREIGN KEY (`user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='log record of monitor function usage	';



# Dump of table q0_needs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_needs`;

CREATE TABLE `q0_needs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT 'This is the short name that fits on the phone screen.  25 characters too much??????',
  `trinity_id` tinyint(4) NOT NULL COMMENT 'I still have concerns about categorizing needs here.  I understand the reasoning, I wonder what happens when a user enters a new need as a GTH and then later changes it to a LTH, this would necessarily require us to add the record again in the need table, now then change it back to GTH and since that was there original pick we can avoid adding it again but if they change it back to LTH we won''t know it was there before and will need to put a new record in.  We will have to avoid this by careful backend programming to check for duplicates ',
  `show_as_public` tinyint(1) NOT NULL DEFAULT '0' COMMENT ' Defaults to showing as public.  A user will see all needs that ShowAsPublic is true and any needs they have created themselves as well',
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified_by_user_id` int(11) DEFAULT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_unique` (`id`),
  UNIQUE KEY `name_type_unq` (`name`,`trinity_id`),
  KEY `needs_trinities_idx` (`trinity_id`),
  KEY `needs_createdby` (`user_id`),
  KEY `needs_modifiedby_idx` (`modified_by_user_id`),
  CONSTRAINT `needs_createdby` FOREIGN KEY (`user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `needs_modifiedby` FOREIGN KEY (`modified_by_user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `needs_trinities` FOREIGN KEY (`trinity_id`) REFERENCES `q0_trinities` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Needs are the user defined descriptions of what a person wants in a relationship.   users have needs.  They are their needs regardless of the relationship.  This is part of the secret sauce of Michaels program, to get people to focus on what they need and can''t stand in a relationship and then find the person that meets those needs.  Needs are added to the list when a user either enters a new need without picking one from the list or when they pick one from the list but then change the wording.';

LOCK TABLES `q0_needs` WRITE;
/*!40000 ALTER TABLE `q0_needs` DISABLE KEYS */;

INSERT INTO `q0_needs` (`id`, `name`, `trinity_id`, `show_as_public`, `user_id`, `created`, `modified_by_user_id`, `modified`)
VALUES
	(1,'Cheats on me',1,1,1,'2015-05-28 02:06:18',1,'2015-05-28 02:06:18'),
	(2,'Enjoys dancing',3,1,1,'2015-05-28 02:06:18',1,'2015-05-28 02:06:18'),
	(3,'Good Communicator',2,1,1,'2015-05-28 02:06:18',1,'2015-05-28 02:06:18'),
	(4,'Great physical chemistry',2,1,1,'2015-05-28 02:06:18',1,'2015-05-28 02:06:18'),
	(5,'Enjoys football',3,1,1,'2015-05-28 02:06:18',1,'2015-05-28 02:06:18'),
	(6,'Always complains',1,1,1,'2015-05-28 02:06:18',1,'2015-05-28 02:06:18');

/*!40000 ALTER TABLE `q0_needs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table q0_needsliders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_needsliders`;

CREATE TABLE `q0_needsliders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relationship_id` int(11) NOT NULL,
  `users_need_id` int(11) NOT NULL,
  `slider_value` float NOT NULL,
  `turned_stormy` datetime DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified_by_user_id` int(11) DEFAULT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_unique` (`id`),
  UNIQUE KEY `userneed_relationship_unq` (`relationship_id`,`users_need_id`),
  KEY `needsliders_relationship_idx` (`relationship_id`),
  KEY `needsliders_users_needs_idx` (`users_need_id`),
  KEY `needsliders_createdby` (`user_id`),
  KEY `needsliders_modifiedby_idx` (`modified_by_user_id`),
  CONSTRAINT `needsliders_createdby` FOREIGN KEY (`user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `needsliders_modifiedby` FOREIGN KEY (`modified_by_user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `needsliders_relationship` FOREIGN KEY (`relationship_id`) REFERENCES `q0_relationships` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `needsliders_users_needs` FOREIGN KEY (`users_need_id`) REFERENCES `q0_users_needs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Holds the attributes of this need that are being met or not by this relationship. This value is recalculated on:';

LOCK TABLES `q0_needsliders` WRITE;
/*!40000 ALTER TABLE `q0_needsliders` DISABLE KEYS */;

INSERT INTO `q0_needsliders` (`id`, `relationship_id`, `users_need_id`, `slider_value`, `turned_stormy`, `user_id`, `created`, `modified_by_user_id`, `modified`)
VALUES
	(1,2,1,134,NULL,3,'2015-05-28 02:06:20',3,'2015-05-28 02:06:20'),
	(2,2,2,30.5,NULL,3,'2015-05-28 02:06:20',3,'2015-05-28 02:06:20'),
	(3,2,3,315,NULL,3,'2015-05-28 02:06:20',3,'2015-05-28 02:06:20'),
	(4,3,4,290,NULL,2,'2015-05-28 02:06:20',2,'2015-05-28 02:06:20'),
	(5,3,5,29,NULL,2,'2015-05-28 02:06:20',2,'2015-05-28 02:06:20'),
	(6,3,6,134,NULL,2,'2015-05-28 02:06:20',2,'2015-05-28 02:06:20');

/*!40000 ALTER TABLE `q0_needsliders` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table q0_needsliders_histories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_needsliders_histories`;

CREATE TABLE `q0_needsliders_histories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `needslider_id` int(11) NOT NULL,
  `moved_from_value` float NOT NULL COMMENT 'The calculated value that is displayed when the user displays the screen for this need for this relationship.  Tihs is only stored if the user manually changes the position.',
  `moved_to_value` float NOT NULL COMMENT 'The value that the user enteres to override the calculated value.',
  `checkins_move` smallint(2) NOT NULL COMMENT 'If the slider movement was caused by a checkin then this flag is set true, if it was a human moving the slider this flag is set false.  I changed this from default 0 to no default so that the code inserting this record HAS to take this into account',
  `turned_stormy` datetime DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified_by_user_id` int(11) DEFAULT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_unique` (`id`),
  KEY `needsliders_histories_needsliders_idx` (`needslider_id`),
  KEY `needsliders_histories_createdby` (`user_id`),
  KEY `needsliders_histories_modifiedby_idx` (`modified_by_user_id`),
  CONSTRAINT `needsliders_histories_createdby` FOREIGN KEY (`user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `needsliders_histories_modifiedby` FOREIGN KEY (`modified_by_user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `needsliders_histories_needsliders` FOREIGN KEY (`needslider_id`) REFERENCES `q0_needsliders` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Holds the attributes of this need that are being met or not by this relationship. The actual display value of the needs slider is a calcualated value.  Here we store only the manual changes a user makes to the needs slider. ';



# Dump of table q0_relationships
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_relationships`;

CREATE TABLE `q0_relationships` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relationship_satisfaction_id` int(11) DEFAULT NULL COMMENT 'Id of the most current satisfaction value',
  `name` varchar(100) DEFAULT NULL COMMENT 'Must be unique for user id',
  `start_date` date NOT NULL,
  `end_date` date DEFAULT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified_by_user_id` int(11) DEFAULT NULL,
  `modified` datetime NOT NULL,
  `howumet` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_unique` (`id`),
  UNIQUE KEY `user_relationship_unq` (`name`,`user_id`),
  KEY `relationships_createdby` (`user_id`),
  KEY `relationships_modifiedby_idx` (`modified_by_user_id`),
  KEY `relationship_satisfaction_current_idx` (`relationship_satisfaction_id`),
  CONSTRAINT `relationship_satisfaction_current` FOREIGN KEY (`relationship_satisfaction_id`) REFERENCES `q0_relationships_satisfactions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `relationships_createdby` FOREIGN KEY (`user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `relationships_modifiedby` FOREIGN KEY (`modified_by_user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='These are the relationships a user is tracking.  Relationships have start and end dates but a relationship can restart.  In that case we push a record onto the Relationship history table with the original start and end dates and chagne the information here to the newly restarted relationship';

LOCK TABLES `q0_relationships` WRITE;
/*!40000 ALTER TABLE `q0_relationships` DISABLE KEYS */;

INSERT INTO `q0_relationships` (`id`, `relationship_satisfaction_id`, `name`, `start_date`, `end_date`, `is_default`, `user_id`, `created`, `modified_by_user_id`, `modified`, `howumet`)
VALUES
	(1,NULL,'< me >','2015-05-28',NULL,1,1,'2015-05-28 02:06:16',1,'2015-05-28 02:06:16',NULL),
	(2,NULL,'Male','2006-03-18',NULL,1,3,'2015-05-28 02:06:19',3,'2015-05-28 02:06:19',NULL),
	(3,NULL,'Female','1932-01-20',NULL,1,2,'2015-05-28 02:06:19',2,'2015-05-28 02:06:19',NULL);

/*!40000 ALTER TABLE `q0_relationships` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table q0_relationships_histories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_relationships_histories`;

CREATE TABLE `q0_relationships_histories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relationship_id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date DEFAULT NULL,
  `reason_for_ending` text COMMENT 'Not null if End Date is filled in',
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified_by_user_id` int(11) DEFAULT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `relationships_histories_relationships_idx` (`relationship_id`),
  KEY `relationships_histories_createdby` (`user_id`),
  KEY `relationships_histories_modifiedby_idx` (`modified_by_user_id`),
  CONSTRAINT `relationships_histories_Relationships` FOREIGN KEY (`relationship_id`) REFERENCES `q0_relationships` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `relationships_histories_createdby` FOREIGN KEY (`user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `relationships_histories_modifiedby` FOREIGN KEY (`modified_by_user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Because a relationship can start and stop several times we are going to keep a record of this in a separate one to many table along with the reason a relationship stops if it does.  The norm will be one record with a start date, possibly and end date and a reason.  If the person goes back to that relationship then a second relationshipdates record is created. 	';



# Dump of table q0_relationships_satisfactions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_relationships_satisfactions`;

CREATE TABLE `q0_relationships_satisfactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relationship_id` int(11) NOT NULL,
  `satisfaction_value` tinyint(4) NOT NULL COMMENT 'The value they set the slider at',
  `comment` text,
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified_by_user_id` int(11) DEFAULT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `relationships_satisfactions_relationship_idx` (`relationship_id`),
  KEY `relationships_satisfactions_createdby` (`user_id`),
  KEY `relationships_satisfactions_modifiedby_idx` (`modified_by_user_id`),
  CONSTRAINT `relationships_satisfactions_createdby` FOREIGN KEY (`user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `relationships_satisfactions_modifiedby` FOREIGN KEY (`modified_by_user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `relationships_satisfactions_relationship` FOREIGN KEY (`relationship_id`) REFERENCES `q0_relationships` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='This is the record of all the times the user changed the slider for there satisfaction in a relationship in settings.';

LOCK TABLES `q0_relationships_satisfactions` WRITE;
/*!40000 ALTER TABLE `q0_relationships_satisfactions` DISABLE KEYS */;

INSERT INTO `q0_relationships_satisfactions` (`id`, `relationship_id`, `satisfaction_value`, `comment`, `user_id`, `created`, `modified_by_user_id`, `modified`)
VALUES
	(1,3,49,'',2,'2015-05-28 02:06:20',2,'2015-05-28 02:06:20'),
	(2,3,81,'',2,'2015-05-28 02:06:20',2,'2015-05-28 02:06:20'),
	(4,2,47,'',3,'2015-05-28 02:06:20',3,'2015-05-28 02:06:20');

/*!40000 ALTER TABLE `q0_relationships_satisfactions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table q0_statuses
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_statuses`;

CREATE TABLE `q0_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relationship_status` tinyint(3) NOT NULL COMMENT 'enumerations of the status''s like "single", "Causal Dating" "engaged'' etc. ',
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified_by_user_id` int(11) DEFAULT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_unique` (`id`),
  KEY `statuses_createdby` (`user_id`),
  KEY `statuses_modifiedby_idx` (`modified_by_user_id`),
  CONSTRAINT `statuses_createdby` FOREIGN KEY (`user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `statuses_modifiedby` FOREIGN KEY (`modified_by_user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Holds the current and past status,  A user has a current status and that''s held in a status foriegn key in user table so we don''t have to look for (max date) or the like here.';

LOCK TABLES `q0_statuses` WRITE;
/*!40000 ALTER TABLE `q0_statuses` DISABLE KEYS */;

INSERT INTO `q0_statuses` (`id`, `relationship_status`, `user_id`, `created`, `modified_by_user_id`, `modified`)
VALUES
	(1,1,1,'2015-05-28 02:06:16',1,'2015-05-28 02:06:16'),
	(2,2,3,'2015-05-28 02:06:17',3,'2015-05-28 02:06:17'),
	(3,2,2,'2015-05-28 02:06:17',2,'2015-05-28 02:06:17');

/*!40000 ALTER TABLE `q0_statuses` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table q0_suggestions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_suggestions`;

CREATE TABLE `q0_suggestions` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'this is an int becase I can''t imagine so many messages we need more than that.',
  `identifier` varchar(20) DEFAULT NULL,
  `category` varchar(45) DEFAULT NULL COMMENT 'the first category is WeatherWheelNeedSetting.  These are the 7 we are initially suggesting based on the position of the weather wheel for a need.  We might add 7 more for WeatherWheelForcastSetting for instance',
  `text` varchar(2048) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified_by_user_id` int(11) DEFAULT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `suggestions_createdby` (`user_id`),
  KEY `suggestions_modifiedby_idx` (`modified_by_user_id`),
  CONSTRAINT `suggestions_createdby` FOREIGN KEY (`user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `suggestions_modifiedby` FOREIGN KEY (`modified_by_user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='This table is a list of the suggestion messages that we may use when certain factors or series of factors present themselves. As of now these are just one per weather wheel segment but having it in a separate table with message numbers and a name allows us to add to the messages and change them without coding.  new criteria for when the messages come up may require coding but these could also be put in a stored procedure and have the procedure called when a helpful suggestion is called for and it is the procedures job to decide which message to use.  This is very much a brainstorm table for me rather than something I''m committed too.  ';



# Dump of table q0_systemfeedbacks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_systemfeedbacks`;

CREATE TABLE `q0_systemfeedbacks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` char(10) NOT NULL COMMENT '"Bug", "Ask a Question", "Submit an Improvement","Complaint", "System" possibly "Error"',
  `app_version` char(10) DEFAULT NULL,
  `device_type` varchar(20) DEFAULT NULL,
  `device_os` varchar(20) DEFAULT NULL,
  `function_in_use` char(10) DEFAULT NULL,
  `comments` varchar(500) DEFAULT NULL,
  `to_be_handled_by` int(11) DEFAULT NULL COMMENT 'This can be an enum for the various teams these can be handled by.  This can be used as a filter to see "my team items" and/or a way to send this on to something like Asembla for the added features of Issue Resolution that has.  Samples for this enum are CustomerService=1, Development=2, Compliance=3 etc.  this starts as null, undefined till someone triages it.',
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified_by_user_id` int(11) DEFAULT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `systemfeedbacks_createdby` (`user_id`),
  KEY `systemfeedbacks_modifiedby_idx` (`modified_by_user_id`),
  CONSTRAINT `systemfeedbacks_createdby` FOREIGN KEY (`user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `systemfeedbacks_modifiedby` FOREIGN KEY (`modified_by_user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='This table is for Bugs, System Messages we may create, user feedback.  Anything that we need to know about what''s happening out there in the wild.	';



# Dump of table q0_tmp_emails
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_tmp_emails`;

CREATE TABLE `q0_tmp_emails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(256) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table q0_trinities
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_trinities`;

CREATE TABLE `q0_trinities` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `abbreviation` char(3) NOT NULL,
  `name` varchar(15) NOT NULL,
  `max_value` int(11) DEFAULT NULL COMMENT '500 for GNH, 200 GH, 50 like to have (I think) ',
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified_by_user_id` int(11) DEFAULT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `trinities_id_unique` (`id`),
  UNIQUE KEY `trinities_name__unique` (`name`),
  UNIQUE KEY `trinities_abbreviation_unique` (`abbreviation`),
  KEY `trinities_createdby_idx` (`user_id`),
  KEY `trinities_modifiedby_idx` (`modified_by_user_id`),
  CONSTRAINT `trinities_createdby` FOREIGN KEY (`user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `trinities_modifiedby` FOREIGN KEY (`modified_by_user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='There are three Trinity types.  Each has a set of mathmatical values and these are set in this table.  It may be neccessary to have a history of these in which case a candidate key will include the trinity name and date of the change';

LOCK TABLES `q0_trinities` WRITE;
/*!40000 ALTER TABLE `q0_trinities` DISABLE KEYS */;

INSERT INTO `q0_trinities` (`id`, `abbreviation`, `name`, `max_value`, `user_id`, `created`, `modified_by_user_id`, `modified`)
VALUES
	(1,'GNH','gotta not have',500,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
	(2,'GH','gotta have',200,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
	(3,'LTH','like to have',50,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00');

/*!40000 ALTER TABLE `q0_trinities` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table q0_user_activities
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_user_activities`;

CREATE TABLE `q0_user_activities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `useragent` varchar(256) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `last_action` int(10) DEFAULT NULL,
  `last_url` text,
  `logout_time` int(10) DEFAULT NULL,
  `user_browser` text,
  `ip_address` varchar(50) DEFAULT NULL,
  `logout` int(11) NOT NULL DEFAULT '0',
  `deleted` int(1) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_activity_userKEY` (`user_id`),
  CONSTRAINT `user_activities_usersFK` FOREIGN KEY (`user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table q0_user_contacts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_user_contacts`;

CREATE TABLE `q0_user_contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `requirement` text,
  `reply_message` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_contacts_userKEY` (`user_id`),
  CONSTRAINT `user_contacts_userFK` FOREIGN KEY (`user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table q0_user_details
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_user_details`;

CREATE TABLE `q0_user_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `photo` text,
  `bday` date DEFAULT NULL,
  `location` varchar(256) DEFAULT NULL,
  `marital_status` varchar(20) DEFAULT NULL,
  `cellphone` varchar(15) DEFAULT NULL,
  `web_page` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_details_userKEY` (`user_id`),
  CONSTRAINT `user_details_usersFK` FOREIGN KEY (`user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table q0_user_email_recipients
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_user_email_recipients`;

CREATE TABLE `q0_user_email_recipients` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `user_email_id` int(10) NOT NULL,
  `user_id` int(10) DEFAULT NULL,
  `email_address` varchar(100) NOT NULL,
  `is_email_sent` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_email_recipients_userKEY` (`user_id`),
  CONSTRAINT `user_email_recipients_userFK` FOREIGN KEY (`user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table q0_user_email_signatures
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_user_email_signatures`;

CREATE TABLE `q0_user_email_signatures` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `signature_name` varchar(100) NOT NULL,
  `signature` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table q0_user_email_templates
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_user_email_templates`;

CREATE TABLE `q0_user_email_templates` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `template_name` varchar(100) NOT NULL,
  `header` text,
  `footer` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table q0_user_emails
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_user_emails`;

CREATE TABLE `q0_user_emails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) DEFAULT NULL,
  `user_group_id` int(11) DEFAULT NULL,
  `cc_to` text,
  `from_name` varchar(200) DEFAULT NULL,
  `from_email` varchar(200) DEFAULT NULL,
  `subject` varchar(500) NOT NULL,
  `message` text NOT NULL,
  `sent_by` int(11) DEFAULT NULL,
  `is_email_sent` int(1) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_emails_usergroupKEY` (`user_group_id`),
  KEY `user_emails_sentbyKEY` (`sent_by`),
  CONSTRAINT `user_emails_sentbyFK` FOREIGN KEY (`sent_by`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_emails_usergroupFK` FOREIGN KEY (`user_group_id`) REFERENCES `q0_user_groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table q0_user_group_permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_user_group_permissions`;

CREATE TABLE `q0_user_group_permissions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_group_id` int(11) NOT NULL,
  `controller` varchar(50) NOT NULL,
  `action` varchar(100) NOT NULL,
  `allowed` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `user_group_permissions_groupKEY` (`user_group_id`),
  CONSTRAINT `user_group_permissions_groupFK` FOREIGN KEY (`user_group_id`) REFERENCES `q0_user_groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `q0_user_group_permissions` WRITE;
/*!40000 ALTER TABLE `q0_user_group_permissions` DISABLE KEYS */;

INSERT INTO `q0_user_group_permissions` (`id`, `user_group_id`, `controller`, `action`, `allowed`)
VALUES
	(1,1,'Pages','display',1),
	(2,2,'Pages','display',1),
	(3,3,'Pages','display',1),
	(4,1,'UserGroupPermissions','index',1),
	(5,2,'UserGroupPermissions','index',0),
	(6,3,'UserGroupPermissions','index',0),
	(7,1,'UserGroups','index',1),
	(8,2,'UserGroups','index',0),
	(9,3,'UserGroups','index',0),
	(10,1,'UserGroups','addGroup',1),
	(11,2,'UserGroups','addGroup',0),
	(12,3,'UserGroups','addGroup',0),
	(13,1,'UserGroups','editGroup',1),
	(14,2,'UserGroups','editGroup',0),
	(15,3,'UserGroups','editGroup',0),
	(16,1,'UserGroups','deleteGroup',1),
	(17,2,'UserGroups','deleteGroup',0),
	(18,3,'UserGroups','deleteGroup',0),
	(19,1,'UserSettings','index',1),
	(20,2,'UserSettings','index',0),
	(21,3,'UserSettings','index',0),
	(22,1,'UserSettings','editSetting',1),
	(23,2,'UserSettings','editSetting',0),
	(24,3,'UserSettings','editSetting',0),
	(25,1,'Users','index',1),
	(26,2,'Users','index',0),
	(27,3,'Users','index',0),
	(28,1,'Users','online',1),
	(29,2,'Users','online',0),
	(30,3,'Users','online',0),
	(31,1,'Users','viewUser',1),
	(32,2,'Users','viewUser',0),
	(33,3,'Users','viewUser',0),
	(34,1,'Users','myprofile',0),
	(35,2,'Users','myprofile',1),
	(36,3,'Users','myprofile',0),
	(37,1,'Users','editProfile',1),
	(38,2,'Users','editProfile',1),
	(39,3,'Users','editProfile',0),
	(40,1,'Users','login',1),
	(41,2,'Users','login',1),
	(42,3,'Users','login',1),
	(43,1,'Users','logout',1),
	(44,2,'Users','logout',1),
	(45,3,'Users','logout',1),
	(46,1,'Users','register',1),
	(47,2,'Users','register',1),
	(48,3,'Users','register',1),
	(49,1,'Users','changePassword',1),
	(50,2,'Users','changePassword',1),
	(51,3,'Users','changePassword',0),
	(52,1,'Users','changeUserPassword',1),
	(53,2,'Users','changeUserPassword',0),
	(54,3,'Users','changeUserPassword',0),
	(55,1,'Users','addUser',1),
	(56,2,'Users','addUser',0),
	(57,3,'Users','addUser',0),
	(58,1,'Users','editUser',1),
	(59,2,'Users','editUser',0),
	(60,3,'Users','editUser',0),
	(61,1,'Users','deleteUser',1),
	(62,2,'Users','deleteUser',0),
	(63,3,'Users','deleteUser',0),
	(64,1,'Users','deleteAccount',0),
	(65,2,'Users','deleteAccount',1),
	(66,3,'Users','deleteAccount',0),
	(67,1,'Users','logoutUser',1),
	(68,2,'Users','logoutUser',0),
	(69,3,'Users','logoutUser',0),
	(70,1,'Users','makeInactive',1),
	(71,2,'Users','makeInactive',0),
	(72,3,'Users','makeInactive',0),
	(73,1,'Users','dashboard',1),
	(74,2,'Users','dashboard',1),
	(75,3,'Users','dashboard',1),
	(76,1,'Users','makeActiveInactive',1),
	(77,2,'Users','makeActiveInactive',0),
	(78,3,'Users','makeActiveInactive',0),
	(79,1,'Users','verifyEmail',1),
	(80,2,'Users','verifyEmail',0),
	(81,3,'Users','verifyEmail',0),
	(82,1,'Users','accessDenied',1),
	(83,2,'Users','accessDenied',1),
	(84,3,'Users','accessDenied',0),
	(85,1,'Users','userVerification',1),
	(86,2,'Users','userVerification',1),
	(87,3,'Users','userVerification',1),
	(88,1,'Users','forgotPassword',1),
	(89,2,'Users','forgotPassword',1),
	(90,3,'Users','forgotPassword',1),
	(91,1,'Users','emailVerification',1),
	(92,2,'Users','emailVerification',1),
	(93,3,'Users','emailVerification',1),
	(94,1,'Users','activatePassword',1),
	(95,2,'Users','activatePassword',1),
	(96,3,'Users','activatePassword',1),
	(97,1,'UserGroupPermissions','update',1),
	(98,2,'UserGroupPermissions','update',0),
	(99,3,'UserGroupPermissions','update',0),
	(100,1,'Users','deleteCache',1),
	(101,2,'Users','deleteCache',0),
	(102,3,'Users','deleteCache',0),
	(103,1,'Autocomplete','fetch',1),
	(104,2,'Autocomplete','fetch',1),
	(105,3,'Autocomplete','fetch',1),
	(106,1,'Users','viewUserPermissions',1),
	(107,2,'Users','viewUserPermissions',0),
	(108,3,'Users','viewUserPermissions',0),
	(109,1,'Contents','index',1),
	(110,2,'Contents','index',0),
	(111,3,'Contents','index',0),
	(112,1,'Contents','addPage',1),
	(113,2,'Contents','addPage',0),
	(114,3,'Contents','addPage',0),
	(115,1,'Contents','editPage',1),
	(116,2,'Contents','editPage',0),
	(117,3,'Contents','editPage',0),
	(118,1,'Contents','viewPage',1),
	(119,2,'Contents','viewPage',0),
	(120,3,'Contents','viewPage',0),
	(121,1,'Contents','deletePage',1),
	(122,2,'Contents','deletePage',0),
	(123,3,'Contents','deletePage',0),
	(124,1,'Contents','content',1),
	(125,2,'Contents','content',1),
	(126,3,'Contents','content',1),
	(127,1,'UserContacts','index',1),
	(128,2,'UserContacts','index',0),
	(129,3,'UserContacts','index',0),
	(130,1,'UserContacts','contactUs',1),
	(131,2,'UserContacts','contactUs',1),
	(132,3,'UserContacts','contactUs',1),
	(133,1,'Users','ajaxLoginRedirect',1),
	(134,2,'Users','ajaxLoginRedirect',1),
	(135,3,'Users','ajaxLoginRedirect',1),
	(136,1,'Users','viewProfile',1),
	(137,2,'Users','viewProfile',1),
	(138,3,'Users','viewProfile',1),
	(139,1,'Users','sendMails',1),
	(140,2,'Users','sendMails',0),
	(141,3,'Users','sendMails',0),
	(142,1,'Users','searchEmails',1),
	(143,2,'Users','searchEmails',0),
	(144,3,'Users','searchEmails',0),
	(145,1,'UserEmails','index',1),
	(146,1,'UserEmails','send',1),
	(147,1,'UserEmails','sendToUser',1),
	(148,1,'UserEmails','sendReply',1),
	(149,1,'UserEmails','view',1),
	(150,1,'UserGroupPermissions','subPermissions',1),
	(151,1,'UserGroupPermissions','getPermissions',1),
	(152,1,'UserGroupPermissions','permissionGroupMatrix',1),
	(153,1,'UserGroupPermissions','permissionSubGroupMatrix',1),
	(154,1,'UserGroupPermissions','changePermission',1),
	(155,1,'Users','indexSearch',1),
	(156,1,'UserEmailSignatures','index',1),
	(157,1,'UserEmailSignatures','add',1),
	(158,1,'UserEmailSignatures','edit',1),
	(159,1,'UserEmailSignatures','delete',1),
	(160,1,'UserEmailTemplates','index',1),
	(161,1,'UserEmailTemplates','add',1),
	(162,1,'UserEmailTemplates','edit',1),
	(163,1,'UserEmailTemplates','delete',1),
	(164,1,'UserSettings','cakelog',1),
	(165,1,'UserSettings','cakelogbackup',1),
	(166,1,'UserSettings','cakelogdelete',1),
	(167,1,'UserSettings','cakelogempty',1),
	(168,1,'Users','addMultipleUsers',1),
	(169,1,'Users','uploadCsv',1);

/*!40000 ALTER TABLE `q0_user_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table q0_user_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_user_groups`;

CREATE TABLE `q0_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `alias_name` varchar(100) DEFAULT NULL,
  `description` text,
  `allowRegistration` int(1) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_group_selfjoinKEY` (`parent_id`),
  CONSTRAINT `user_group_selfjoinFK` FOREIGN KEY (`parent_id`) REFERENCES `q0_user_groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `q0_user_groups` WRITE;
/*!40000 ALTER TABLE `q0_user_groups` DISABLE KEYS */;

INSERT INTO `q0_user_groups` (`id`, `parent_id`, `name`, `alias_name`, `description`, `allowRegistration`, `created`, `modified`)
VALUES
	(1,NULL,'Admin','Admin',NULL,0,'2014-11-18 08:24:38','2014-11-18 08:24:38'),
	(2,NULL,'User','User',NULL,1,'2014-11-18 08:24:38','2014-11-18 08:24:38'),
	(3,NULL,'Guest','Guest',NULL,0,'2014-11-18 08:24:38','2014-11-18 08:24:38');

/*!40000 ALTER TABLE `q0_user_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table q0_user_profiles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_user_profiles`;

CREATE TABLE `q0_user_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status_id` int(11) NOT NULL COMMENT 'This is the status that the user currently self reflects in the system.  Ongoing status will be maintained in the status  history table. This status can be changed by the user at any time.  That will add a new record in the Status table and reset the foriegn key here.',
  `birth_day` date NOT NULL,
  `gender` char(1) NOT NULL,
  `interested_in` char(1) NOT NULL COMMENT 'M/F/B Male, Female, Both',
  `divorced` int(1) NOT NULL,
  `tandc_accepted_date` datetime NOT NULL,
  `hide_intro_tutorial` int(1) NOT NULL DEFAULT '0',
  `last_signed_in` datetime DEFAULT NULL,
  `last_settings_update` datetime DEFAULT NULL,
  `password_is_temporary` datetime DEFAULT NULL COMMENT 'Use this to indicate the forgot password function was called and triggers a "change password" on the next login as long as the time is not more than 24 hours previous.',
  `city` varchar(45) NOT NULL COMMENT 'The city, state country columns may be changed, since they are validated against a google list, to a foriegn key to a list of city, state and country table records.  Likely to be faster with the join than adding all the duplication',
  `state` varchar(45) NOT NULL COMMENT 'this can not be a link to a states table unless you are going to put all the worlds "states" in there',
  `country` varchar(45) NOT NULL DEFAULT 'United States',
  `zipcode` char(5) DEFAULT NULL,
  `user_role` char(1) DEFAULT 'U' COMMENT 'Only a few user roles will be needed so a single character should do it. ',
  `special_user_flag` binary(1) DEFAULT NULL COMMENT 'Need to be able to flag users as helpers of various sorts. This column will hold the flags. The flags will be bit representatons so that they are additive.  for instance if beta testers are 1 and friendlies are 2 then a beta tester that is also a friendly is 3, 00000011: using this allows one byte to hold a one to many relation',
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified_by_user_id` int(11) DEFAULT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_unique` (`id`),
  UNIQUE KEY `user_id_unique` (`user_id`),
  KEY `status_idx` (`status_id`),
  KEY `user_profiles_createdby` (`user_id`),
  KEY `user_profiles_modifiedby_id_idx` (`modified_by_user_id`),
  CONSTRAINT `user_profiles_createdby` FOREIGN KEY (`user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_profiles_modifiedby` FOREIGN KEY (`modified_by_user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_statuses_id` FOREIGN KEY (`status_id`) REFERENCES `q0_statuses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Holds neccessary customer information for mobile app users	.  See notes on /StatusId/Password/ PasswordIsTemporary/City/SpecialUserFlag';

LOCK TABLES `q0_user_profiles` WRITE;
/*!40000 ALTER TABLE `q0_user_profiles` DISABLE KEYS */;

INSERT INTO `q0_user_profiles` (`id`, `status_id`, `birth_day`, `gender`, `interested_in`, `divorced`, `tandc_accepted_date`, `hide_intro_tutorial`, `last_signed_in`, `last_settings_update`, `password_is_temporary`, `city`, `state`, `country`, `zipcode`, `user_role`, `special_user_flag`, `user_id`, `created`, `modified_by_user_id`, `modified`)
VALUES
	(1,1,'1990-01-01','1','2',0,'2015-05-28 02:06:16',0,NULL,NULL,NULL,'New York','New York','United States',NULL,'U',NULL,1,'2015-05-28 02:06:16',1,'2015-05-28 02:06:16'),
	(2,3,'1935-01-20','2','1',1,'0000-00-00 00:00:00',1,NULL,NULL,NULL,'New Orleans, LA, USA','','United States',NULL,'U',NULL,2,'2015-05-28 02:06:17',2,'2015-05-28 02:06:17'),
	(3,2,'1953-03-19','1','2',0,'2015-03-19 17:37:48',1,NULL,NULL,NULL,'Beverly Hills, CA 90210, USA','','United States',NULL,'U',NULL,3,'2015-05-28 02:06:18',3,'2015-05-28 02:06:18');

/*!40000 ALTER TABLE `q0_user_profiles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table q0_user_settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_user_settings`;

CREATE TABLE `q0_user_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `name_public` text,
  `value` varchar(256) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `category` varchar(20) DEFAULT 'OTHER',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `q0_user_settings` WRITE;
/*!40000 ALTER TABLE `q0_user_settings` DISABLE KEYS */;

INSERT INTO `q0_user_settings` (`id`, `name`, `name_public`, `value`, `type`, `category`)
VALUES
	(1,'defaultTimeZone','Enter default time zone identifier','America/New_York','input','OTHER'),
	(2,'siteName','Enter Your Site Name','User Management Plugin','input','OTHER'),
	(3,'siteRegistration','New Registration is allowed or not','1','checkbox','USER'),
	(4,'allowDeleteAccount','Allow users to delete account','0','checkbox','USER'),
	(5,'sendRegistrationMail','Send Registration Mail After User Registered','0','checkbox','EMAIL'),
	(6,'sendPasswordChangeMail','Send Password Change Mail After User changed password','0','checkbox','EMAIL'),
	(7,'emailVerification','Want to verify user\'s email address?','0','checkbox','EMAIL'),
	(8,'emailFromAddress','Enter email by which emails will be send.','example@example.com','input','EMAIL'),
	(9,'emailFromName','Enter Email From Name','BRB','input','EMAIL'),
	(10,'allowChangeUsername','Do you want to allow users to change their username?','1','checkbox','USER'),
	(11,'bannedUsernames','Set banned usernames comma separated(no space, no quotes)','Administrator, SuperAdmin','input','USER'),
	(12,'useRecaptcha','Do you want to add captcha support on registration form, contact us form, login form in case bad logins, forgot password page, email verification page? Please note we have separate settings for all pages to Add or Remove captcha.','0','checkbox','RECAPTCHA'),
	(13,'privateKeyFromRecaptcha','Enter private key for Recaptcha from google','','input','RECAPTCHA'),
	(14,'publicKeyFromRecaptcha','Enter public key for recaptcha from google','','input','RECAPTCHA'),
	(15,'loginRedirectUrl','Enter URL where user will be redirected after login ','/dashboard','input','OTHER'),
	(16,'logoutRedirectUrl','Enter URL where user will be redirected after logout','/login','input','OTHER'),
	(17,'permissions','Do you Want to enable permissions for users?','0','checkbox','PERMISSION'),
	(18,'adminPermissions','Do you want to check permissions for Admin?','0','checkbox','PERMISSION'),
	(19,'defaultGroupId','Enter default group id for user registration','2','input','GROUP'),
	(20,'adminGroupId','Enter Admin Group Id','1','input','GROUP'),
	(21,'guestGroupId','Enter Guest Group Id','3','input','GROUP'),
	(22,'useFacebookLogin','Want to use Facebook Connect on your site?','0','checkbox','FACEBOOK'),
	(23,'facebookAppId','Facebook Application Id','','input','FACEBOOK'),
	(24,'facebookSecret','Facebook Application Secret Code','','input','FACEBOOK'),
	(25,'facebookScope','Facebook Permissions','user_status, publish_stream, email','input','FACEBOOK'),
	(26,'useTwitterLogin','Want to use Twitter Connect on your site?','0','checkbox','TWITTER'),
	(27,'twitterConsumerKey','Twitter Consumer Key','','input','TWITTER'),
	(28,'twitterConsumerSecret','Twitter Consumer Secret','','input','TWITTER'),
	(29,'useGmailLogin','Want to use Gmail Connect on your site?','1','checkbox','GOOGLE'),
	(30,'useYahooLogin','Want to use Yahoo Connect on your site?','1','checkbox','YAHOO'),
	(31,'useLinkedinLogin','Want to use Linkedin Connect on your site?','0','checkbox','LINKEDIN'),
	(32,'linkedinApiKey','Linkedin Api Key','','input','LINKEDIN'),
	(33,'linkedinSecretKey','Linkedin Secret Key','','input','LINKEDIN'),
	(34,'useFoursquareLogin','Want to use Foursquare Connect on your site?','0','checkbox','FOURSQUARE'),
	(35,'foursquareClientId','Foursquare Client Id','','input','FOURSQUARE'),
	(36,'foursquareClientSecret','Foursquare Client Secret','','input','FOURSQUARE'),
	(37,'viewOnlineUserTime','You can view online users and guest from last few minutes, set time in minutes ','30','input','USER'),
	(38,'useHttps','Do you want to HTTPS for whole site?','0','checkbox','OTHER'),
	(39,'httpsUrls','You can set selected urls for HTTPS (e.g. users/login, users/register)',NULL,'input','OTHER'),
	(40,'imgDir','Enter Image directory name where users profile photos will be uploaded. This directory should be in webroot/img directory','umphotos','input','OTHER'),
	(41,'QRDN','Increase this number by 1 every time if you made any changes in CSS or JS file','12345678','input','OTHER'),
	(42,'cookieName','Please enter cookie name for your site which is used to login user automatically for remember me functionality','UMPremiumCookie','input','OTHER'),
	(43,'adminEmailAddress','Admin Email address for emails','','input','EMAIL'),
	(44,'useRecaptchaOnLogin','Do you want to add captcha support on login form in case bad logins? For this feature you must have Captcha setting ON with valid private and public keys.','1','checkbox','RECAPTCHA'),
	(45,'badLoginAllowCount','Set number of allowed bad logins. for e.g. 5 or 10. For this feature you must have Captcha setting ON with valid private and public keys.','5','input','RECAPTCHA'),
	(46,'useRecaptchaOnRegistration','Do you want to add captcha support on registration form? For this feature you must have Captcha setting ON with valid private and public keys.','1','checkbox','RECAPTCHA'),
	(47,'useRecaptchaOnForgotPassword','Do you want to add captcha support on forgot password page? For this feature you must have Captcha setting ON with valid private and public keys.','1','checkbox','RECAPTCHA'),
	(48,'useRecaptchaOnEmailVerification','Do you want to add captcha support on email verification page? For this feature you must have Captcha setting ON with valid private and public keys.','1','checkbox','RECAPTCHA'),
	(49,'useRememberMe','Set true/false if you want to add/remove remember me feature on login page','1','checkbox','USER'),
	(50,'allowUserMultipleLogin','Do you want to allow multiple logins with same user account for users(not admin)?','1','checkbox','USER'),
	(51,'allowAdminMultipleLogin','Do you want to allow multiple logins with same user account for admin(not users)?','1','checkbox','USER'),
	(52,'loginIdleTime','Set max idle time in minutes for user. This idle time will be used when multiple logins are not allowed for same user account. If max idle time reached since user last activity on site then anyone can login with same account in other browser and idle user will be logged out.','10','input','USER');

/*!40000 ALTER TABLE `q0_user_settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table q0_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_users`;

CREATE TABLE `q0_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fb_id` bigint(100) DEFAULT NULL,
  `fb_access_token` text,
  `twt_id` bigint(100) DEFAULT NULL,
  `twt_access_token` text,
  `twt_access_secret` text,
  `ldn_id` varchar(100) DEFAULT NULL,
  `user_group_id` int(11) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `salt` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `active` varchar(3) DEFAULT '0',
  `email_verified` int(1) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `by_admin` int(1) NOT NULL DEFAULT '0',
  `ip_address` varchar(50) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `forgot_pass_key` varchar(128) DEFAULT NULL,
  `forgot_pass_expires` int(11) DEFAULT NULL,
  `forgot_pass_pending` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user` (`username`),
  UNIQUE KEY `mail` (`email`),
  KEY `user_user_groupKEY` (`user_group_id`),
  CONSTRAINT `user_user_groupFK` FOREIGN KEY (`user_group_id`) REFERENCES `q0_user_groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `q0_users` WRITE;
/*!40000 ALTER TABLE `q0_users` DISABLE KEYS */;

INSERT INTO `q0_users` (`id`, `fb_id`, `fb_access_token`, `twt_id`, `twt_access_token`, `twt_access_secret`, `ldn_id`, `user_group_id`, `username`, `password`, `salt`, `email`, `first_name`, `last_name`, `active`, `email_verified`, `last_login`, `by_admin`, `ip_address`, `created`, `modified`, `forgot_pass_key`, `forgot_pass_expires`, `forgot_pass_pending`)
VALUES
	(1,NULL,NULL,NULL,NULL,NULL,NULL,2,'BRB_System','2818ae22524cc65160848a0c6cf14aafd700910d3982a44a62f5d8fd90a59a0a','Ag3Zjd7AmAtAo9mZXAhvkvhKffnG23PpIggTqS1O4y9cvL06fFVGvPgfVlQoxucg','system@brb.com',NULL,NULL,'1',1,NULL,0,NULL,'2015-05-28 02:06:15','2015-05-28 02:06:15',NULL,NULL,0),
	(2,NULL,NULL,NULL,NULL,NULL,NULL,2,'female_default','f879a98752320fd748ffea5008858cd1f6f24d31cafbcbd1330edfa7b3a4cd7b','acP+amvmEyD/D5xSbZeVmprF4GttZdDPviCVNu7qhFiugsMaaNY6aObWulNuT+4J','female_default@relationshipbarometer.com',NULL,NULL,'1',1,NULL,0,NULL,'2015-05-28 02:06:16','2015-05-28 02:06:16',NULL,NULL,0),
	(3,NULL,NULL,NULL,NULL,NULL,NULL,2,'male_default','581513fe50a3ca2124c46255c962363431c400655ce42dbdc8c155127e7a89ee','5RwOoh/0P9to9o6suX+QeAo+KgZCVmgNsGQ+8HOoqlnEufvkrTu/FjFOwuvOU2TY','male_default@relationshipbarometer.com',NULL,NULL,'1',1,NULL,0,NULL,'2015-05-28 02:06:16','2015-05-28 02:06:16',NULL,NULL,0);

/*!40000 ALTER TABLE `q0_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table q0_users_needs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_users_needs`;

CREATE TABLE `q0_users_needs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `need_id` int(11) NOT NULL,
  `picked_need_id` int(11) DEFAULT NULL COMMENT 'PickedNeed_id will be the same as Need_id for a need that the user did not change wording.  It will be null if the user did not pick a need but entered one as original.  ',
  `no_longer_needed` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'This is the database implmentation of delete a need.  Keeps the info but program or views can filter it out.   Having a bit is probably faster than finding out if the date is null or not.',
  `no_longer_needed_date` datetime DEFAULT NULL COMMENT 'When a user "deletes" a need we will set the NoLongerNeeded bit and enter the date here.  From that point the user will never see this need again. I suggest that checkins still be linked to this need if they exist but that the program can, if you wish, simply ignore the need if the bit is set making it look to the user as if that check in was now unlinked.  We will keep valuable information and the user will see the behavior you want them too',
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified_by_user_id` int(11) DEFAULT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_unique` (`id`),
  KEY `users_needs_need_idx` (`need_id`),
  KEY `users_needs_picked_need_idx` (`picked_need_id`),
  KEY `users_needs_createdby` (`user_id`),
  KEY `users_needs_modifiedby_idx` (`modified_by_user_id`),
  CONSTRAINT `users_needs_createdby` FOREIGN KEY (`user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `users_needs_modifiedby` FOREIGN KEY (`modified_by_user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `users_needs_need` FOREIGN KEY (`need_id`) REFERENCES `q0_needs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `users_needs_picked_need` FOREIGN KEY (`picked_need_id`) REFERENCES `q0_needs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='This links the user to the need.  Need is a dynamic table that users will be adding to all the time.   Later we should be able to get some interesting analysis out of the number of users who pick certain need wordings and how many are on both Must have and Must Not Have lists.   If this is a new need then the need will be entered new into the need table and the userid of the user creating it will be added to that table.  A need is new for the need table if the user does not select an existing need or changes the wording of a selected need or changes the trinity selection for the need and the need does not exist in the needs table with that trinity id. ';

LOCK TABLES `q0_users_needs` WRITE;
/*!40000 ALTER TABLE `q0_users_needs` DISABLE KEYS */;

INSERT INTO `q0_users_needs` (`id`, `need_id`, `picked_need_id`, `no_longer_needed`, `no_longer_needed_date`, `user_id`, `created`, `modified_by_user_id`, `modified`)
VALUES
	(1,4,4,0,NULL,3,'2015-05-28 02:06:19',3,'2015-05-28 02:06:19'),
	(2,5,5,0,NULL,3,'2015-05-28 02:06:19',3,'2015-05-28 02:06:19'),
	(3,6,6,0,NULL,3,'2015-05-28 02:06:19',3,'2015-05-28 02:06:19'),
	(4,1,1,0,NULL,2,'2015-05-28 02:06:19',2,'2015-05-28 02:06:19'),
	(5,2,2,0,NULL,2,'2015-05-28 02:06:19',2,'2015-05-28 02:06:19'),
	(6,3,3,0,NULL,2,'2015-05-28 02:06:19',2,'2015-05-28 02:06:19');

/*!40000 ALTER TABLE `q0_users_needs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table q0_users_needs_histories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_users_needs_histories`;

CREATE TABLE `q0_users_needs_histories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_need_id` int(11) NOT NULL,
  `changed_on` datetime DEFAULT NULL,
  `trinity_from` tinyint(4) DEFAULT NULL,
  `trinity_to` tinyint(4) DEFAULT NULL,
  `need_id_from` int(11) DEFAULT NULL,
  `need_id_to` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified_by_user_id` int(11) DEFAULT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `users_needs_histories_users_need_idx` (`users_need_id`),
  KEY `users_needs_histories_createdby` (`user_id`),
  KEY `users_needs_histories_modifiedby_idx` (`modified_by_user_id`),
  CONSTRAINT `users_needs_histories_users_needs` FOREIGN KEY (`users_need_id`) REFERENCES `q0_users_needs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Since a need wording can be changed and a need trinity can be changed we are keeping the history of those changes in this file.  I don''t think there is anything the program uses from this, it is for later analysis and troubleshooting customer issues.  Two things can change that need tracking, a change in the needid and a change in the trinity.  either one or both may be tracked in one record.';



# Dump of table q0_weatherwheel_segments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `q0_weatherwheel_segments`;

CREATE TABLE `q0_weatherwheel_segments` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `segment_number` tinyint(4) NOT NULL COMMENT 'From 1 to 7',
  `lower_bound` decimal(10,0) NOT NULL COMMENT 'Lowest value that would set the indicator at this segment',
  `upper_bound` decimal(10,0) NOT NULL COMMENT 'Highest Value that would set the indicator at this segment',
  `midpoint` decimal(10,0) NOT NULL COMMENT 'Value used in the "normalized checkin" calculation',
  `name` varchar(15) NOT NULL COMMENT 'Sun, Partly Cloudy, Partly Sunny, Totally Cloudy, Rainy, Thunderstorm, Tornado',
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified_by_user_id` int(11) DEFAULT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_unique` (`id`),
  UNIQUE KEY `segmentnumber_unique` (`segment_number`),
  UNIQUE KEY `lowervalue_unique` (`lower_bound`),
  UNIQUE KEY `upperbound_unique` (`upper_bound`),
  UNIQUE KEY `midpoint_unique` (`midpoint`),
  KEY `weatherwheel_segments_createdby` (`user_id`),
  KEY `weatherwheel_segments_modifiedby_idx` (`modified_by_user_id`),
  CONSTRAINT `weatherwheel_segments_createdby` FOREIGN KEY (`user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `weatherwheel_segments_modifiedby` FOREIGN KEY (`modified_by_user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='The anomoly of Segment 1 not being even with all other segments suggests that we keep the values in a table somewhere, either in the code or on disk and read it in on start up.  It could also be in code but the primary concern is that this is stored in one place and not set in code whereever it''s needed, just in case it changes sometime.	';



# Dump of table Relationship_CheckinDates
# ------------------------------------------------------------

DROP VIEW IF EXISTS `Relationship_CheckinDates`;

CREATE TABLE `Relationship_CheckinDates` (
   `RelationshipEndDate` DATETIME NULL DEFAULT NULL,
   `RelationshipRestartDate` DATETIME NULL DEFAULT NULL,
   `relationship_id` INT(11) NOT NULL
) ENGINE=MyISAM;



# Dump of table Relationship_EndDates
# ------------------------------------------------------------

DROP VIEW IF EXISTS `Relationship_EndDates`;

CREATE TABLE `Relationship_EndDates` (
   `EndedDate` DATETIME NULL DEFAULT NULL,
   `relationship_id` INT(11) NOT NULL DEFAULT '0',
   `name` VARCHAR(100) NULL DEFAULT NULL
) ENGINE=MyISAM;





# Replace placeholder table for Relationship_EndDates with correct view syntax
# ------------------------------------------------------------

DROP TABLE `Relationship_EndDates`;

CREATE ALGORITHM=UNDEFINED DEFINER=`DoubleAngel`@`%` SQL SECURITY DEFINER VIEW `Relationship_EndDates`
AS SELECT
   (case when isnull(`rcd`.`RelationshipEndDate`) then `rcd`.`RelationshipEndDate` when isnull(`rcd`.`RelationshipRestartDate`) then `rcd`.`RelationshipEndDate` when (`rcd`.`RelationshipEndDate` > `rcd`.`RelationshipRestartDate`) then `rcd`.`RelationshipEndDate` else NULL end) AS `EndedDate`,
   `r`.`id` AS `relationship_id`,
   `r`.`name` AS `name`
FROM (`q0_relationships` `r` left join `Relationship_CheckinDates` `rcd` on((`r`.`id` = `rcd`.`relationship_id`))) where ((`r`.`name` <> '< me >') and (`r`.`name` <> '<me>'));


# Replace placeholder table for diaguserneedsallinfo with correct view syntax
# ------------------------------------------------------------

DROP TABLE `diaguserneedsallinfo`;

CREATE ALGORITHM=UNDEFINED DEFINER=`DoubleAngel`@`%` SQL SECURITY DEFINER VIEW `diaguserneedsallinfo`
AS SELECT
   `users`.`id` AS `users_id`,
   `users`.`username` AS `users_username`,
   `users`.`password` AS `users_password`,
   `users`.`salt` AS `users_salt`,
   `users`.`email` AS `users_email`,
   `users`.`active` AS `users_active`,
   `users`.`last_login` AS `users_last_login`,
   `users`.`ip_address` AS `users_ip_address`,
   `users`.`created` AS `users_created`,
   `users`.`modified` AS `users_modified`,
   `un`.`id` AS `usersneeds_id`,
   `un`.`need_id` AS `userneeds_needid`,
   `un`.`no_longer_needed` AS `usersneeds_no_longer_needed`,
   `un`.`no_longer_needed_date` AS `usersneeds_no_longer_needed_date`,
   `un`.`user_id` AS `userneeds_Userid`,
   `un`.`created` AS `userneeds_created`,
   `un`.`modified_by_user_id` AS `userneeds_modifiedby`,
   `un`.`modified` AS `userneeds_modified`,
   `needs`.`id` AS `needs_Id`,
   `needs`.`name` AS `needs_name`,
   `needs`.`trinity_id` AS `needs_trinity_id`,
   `needs`.`show_as_public` AS `needs_show_as_public`,
   `needs`.`user_id` AS `needs_user_id`,
   `needs`.`created` AS `needs_created`,
   `needs`.`modified_by_user_id` AS `needs_modifiedby`,
   `needs`.`modified` AS `needs_modified`,
   `ns`.`id` AS `needslider_id`,
   `ns`.`relationship_id` AS `needslider_relationshipid`,
   `ns`.`users_need_id` AS `needslider_user_need_id`,
   `ns`.`slider_value` AS `needslider_slider_value`,
   `ns`.`turned_stormy` AS `needslider_turned_stormy`,
   `ns`.`user_id` AS `nneedslider_UserId`,
   `ns`.`created` AS `needslider_created`,
   `ns`.`modified_by_user_id` AS `needslider_modifiedby`,
   `ns`.`modified` AS `needslider_modified`,
   `r`.`id` AS `relationship_id`,
   `r`.`name` AS `relationship_name`,
   `r`.`start_date` AS `relationship_startdate`,
   `r`.`end_date` AS `relationship_enddate`,
   `r`.`is_default` AS `relationship_isdefault`,
   `r`.`user_id` AS `relationship_userid`,
   `r`.`created` AS `relationship_created`,
   `r`.`modified_by_user_id` AS `relationship_modifiedby`,
   `r`.`modified` AS `relationship_modified`
FROM ((((`q0_users` `users` left join `q0_users_needs` `un` on((`users`.`id` = `un`.`user_id`))) left join `q0_needs` `needs` on((`needs`.`id` = `un`.`need_id`))) left join `q0_needsliders` `ns` on((`ns`.`users_need_id` = `un`.`id`))) left join `q0_relationships` `r` on((`r`.`user_id` = `users`.`id`)));


# Replace placeholder table for DiagUserNeedsAllInfo with correct view syntax
# ------------------------------------------------------------

DROP TABLE `DiagUserNeedsAllInfo`;

CREATE ALGORITHM=UNDEFINED DEFINER=`DoubleAngel`@`%` SQL SECURITY DEFINER VIEW `DiagUserNeedsAllInfo`
AS SELECT
   `users`.`id` AS `Users_ID`,
   `users`.`username` AS `Users_username`,
   `users`.`password` AS `Users_password`,
   `users`.`salt` AS `Users_salt`,
   `users`.`email` AS `Users_email`,
   `users`.`active` AS `Users_active`,
   `users`.`last_login` AS `Users_last_login`,
   `users`.`ip_address` AS `Users_ip_address`,
   `users`.`created` AS `Users_Created`,
   `users`.`modified` AS `Users_Modified`,
   `un`.`id` AS `UsersNeeds_ID`,
   `un`.`need_id` AS `UserNeeds_NeedID`,
   `un`.`no_longer_needed` AS `UsersNeeds_no_longer_needed`,
   `un`.`no_longer_needed_date` AS `UsersNeeds_no_longer_needed_date`,
   `un`.`user_id` AS `UserNeeds_UserID`,
   `un`.`created` AS `UserNeeds_Created`,
   `un`.`modified_by_user_id` AS `UserNeeds_ModifiedBy`,
   `un`.`modified` AS `UserNeeds_Modified`,
   `needs`.`id` AS `Needs_Id`,
   `needs`.`name` AS `Needs_name`,
   `needs`.`trinity_id` AS `Needs_trinity_id`,
   `needs`.`show_as_public` AS `Needs_show_as_public`,
   `needs`.`user_id` AS `Needs_user_id`,
   `needs`.`created` AS `Needs_created`,
   `needs`.`modified_by_user_id` AS `Needs_ModifiedBy`,
   `needs`.`modified` AS `Needs_Modified`,
   `ns`.`id` AS `Needslider_id`,
   `ns`.`relationship_id` AS `Needslider_relationshipid`,
   `ns`.`users_need_id` AS `Needslider_user_need_id`,
   `ns`.`slider_value` AS `Needslider_slider_value`,
   `ns`.`turned_stormy` AS `Needslider_turned_stormy`,
   `ns`.`user_id` AS `nNeedslider_UserId`,
   `ns`.`created` AS `Needslider_Created`,
   `ns`.`modified_by_user_id` AS `Needslider_ModifiedBy`,
   `ns`.`modified` AS `Needslider_Modified`,
   `r`.`id` AS `Relationship_id`,
   `r`.`name` AS `Relationship_name`,
   `r`.`start_date` AS `Relationship_startdate`,
   `r`.`end_date` AS `Relationship_enddate`,
   `r`.`is_default` AS `Relationship_isdefault`,
   `r`.`user_id` AS `Relationship_userid`,
   `r`.`created` AS `Relationship_created`,
   `r`.`modified_by_user_id` AS `Relationship_modifiedby`,
   `r`.`modified` AS `Relationship_modified`
FROM ((((`q0_users` `users` left join `q0_users_needs` `un` on((`users`.`id` = `un`.`user_id`))) left join `q0_needs` `needs` on((`needs`.`id` = `un`.`need_id`))) left join `q0_needsliders` `ns` on((`ns`.`users_need_id` = `un`.`id`))) left join `q0_relationships` `r` on((`r`.`user_id` = `users`.`id`)));


# Replace placeholder table for ForecastAndRelationshipDates with correct view syntax
# ------------------------------------------------------------

DROP TABLE `ForecastAndRelationshipDates`;

CREATE ALGORITHM=UNDEFINED DEFINER=`DoubleAngel`@`%` SQL SECURITY DEFINER VIEW `ForecastAndRelationshipDates`
AS SELECT
   `users`.`id` AS `User_Id`,
   `r`.`id` AS `Relationship_Id`,(select max(`q0_forecasts`.`forecast_date`)
FROM `q0_forecasts` where (`q0_forecasts`.`relationship_id` = `r`.`id`) group by `q0_forecasts`.`relationship_id`) AS `LastForecastDate`,`red`.`EndedDate` AS `EndedDate` from ((`q0_users` `users` join `q0_relationships` `r` on((`users`.`id` = `r`.`user_id`))) join `Relationship_EndDates` `red` on((`red`.`relationship_id` = `r`.`id`)));


# Replace placeholder table for Relationship_CheckinDates with correct view syntax
# ------------------------------------------------------------

DROP TABLE `Relationship_CheckinDates`;

CREATE ALGORITHM=UNDEFINED DEFINER=`DoubleAngel`@`%` SQL SECURITY DEFINER VIEW `Relationship_CheckinDates`
AS SELECT
   (select max(`eDates`.`checkin_date`)
FROM `q0_checkins` `eDates` where ((`eDates`.`relationship_id` = `checkins`.`relationship_id`) and (`eDates`.`deleted_checkin` = 0) and (`eDates`.`checkin_type` = 2)) group by `eDates`.`relationship_id`) AS `RelationshipEndDate`,(select max(`sDates`.`checkin_date`) from `q0_checkins` `sDates` where ((`sDates`.`relationship_id` = `checkins`.`relationship_id`) and (`sDates`.`deleted_checkin` = 0) and (`sDates`.`checkin_type` = 1)) group by `sDates`.`relationship_id`) AS `RelationshipRestartDate`,`checkins`.`relationship_id` AS `relationship_id` from `q0_checkins` `checkins` group by `checkins`.`relationship_id`;


# Replace placeholder table for checkins_data with correct view syntax
# ------------------------------------------------------------

DROP TABLE `checkins_data`;

CREATE ALGORITHM=UNDEFINED DEFINER=`DoubleAngel`@`%` SQL SECURITY DEFINER VIEW `checkins_data`
AS SELECT
   `checkins`.`id` AS `checkins_id`,
   `checkins`.`users_need_id` AS `checkins_users_need_id`,
   `checkins`.`relationship_id` AS `checkins_relationship_id`,
   `checkins`.`wheel_setting` AS `checkins_wheel_setting`,
   `checkins`.`checkin_date` AS `checkin_date`,
   `checkins`.`comment` AS `checkins_comment`,
   `checkins`.`deleted_checkin` AS `checkins_deleted_checkin`,
   `checkins`.`user_id` AS `checkins_user_id`,
   `checkins`.`created` AS `checkins_created`,
   `checkins`.`modified_by_user_id` AS `checkins_modified_by`,
   `checkins`.`modified` AS `checkins_modified`,
   `relationships`.`id` AS `relationships_id`,
   `relationships`.`name` AS `relationships_name`,
   `relationships`.`start_date` AS `relationships_start_date`,
   `relationships`.`end_date` AS `relationships_end_date`,
   `relationships`.`is_default` AS `relationships_is_default`,
   `relationships`.`user_id` AS `relationships_user_id`,
   `relationships`.`created` AS `relationships_created`,
   `relationships`.`modified_by_user_id` AS `relationships_modified_by`,
   `relationships`.`modified` AS `relationships_modified`,
   `un`.`id` AS `users_needs_id`,
   `un`.`need_id` AS `users_needs_need_id`,
   `un`.`picked_need_id` AS `users_needs_picked_need`,
   `un`.`no_longer_needed` AS `users_needs_not_longer_needed`,
   `un`.`user_id` AS `users_needs_user_id`,
   `un`.`created` AS `users_needs_created`,
   `un`.`modified_by_user_id` AS `users_needs_modified_by`,
   `un`.`modified` AS `users_needs_modified`,
   `needs`.`id` AS `needs_id`,
   `needs`.`name` AS `needs_name`,
   `needs`.`trinity_id` AS `needs_trinity_id`,
   `needs`.`show_as_public` AS `needs_show_as_public`,
   `needs`.`user_id` AS `needs_user_id`,
   `needs`.`created` AS `needs_created`,
   `needs`.`modified_by_user_id` AS `needs_modified_by`,
   `needs`.`modified` AS `needs_modified`
FROM ((((`q0_checkins` `checkins` join `q0_users` `users` on((`users`.`id` = `checkins`.`user_id`))) join `q0_relationships` `relationships` on((`checkins`.`relationship_id` = `relationships`.`id`))) left join `q0_users_needs` `un` on(((`checkins`.`users_need_id` = `un`.`id`) and ((`un`.`no_longer_needed` = 0) or isnull(`un`.`no_longer_needed`))))) left join `q0_needs` `needs` on((`un`.`need_id` = `needs`.`id`))) where (`checkins`.`deleted_checkin` = 0);

--
-- Dumping routines (PROCEDURE) for database 'brb_initialize'
--
DELIMITER ;;

# Dump of PROCEDURE adminCreateIndex
# ------------------------------------------------------------

/*!50003 DROP PROCEDURE IF EXISTS `adminCreateIndex` */;;
/*!50003 SET SESSION SQL_MODE="NO_ENGINE_SUBSTITUTION"*/;;
/*!50003 CREATE*/ /*!50020 DEFINER=`DoubleAngel`@`%`*/ /*!50003 PROCEDURE `adminCreateIndex`(
    given_database VARCHAR(64),
    given_table    VARCHAR(64),
    given_index    VARCHAR(64),
    given_columns  VARCHAR(64),
	indextype varchar(50),
	out result varchar(1000) 
)
BEGIN

    DECLARE IndexIsThere INTEGER;

    SELECT COUNT(*) into IndexIsThere
    FROM INFORMATION_SCHEMA.STATISTICS
    WHERE table_schema = given_database
    AND   table_name   = given_table
    AND   index_name   = given_index;
	
	set result = "";
    IF IndexIsThere <> 0 THEN
		SET @sqlstmt = CONCAT('ALTER TABLE ',given_table,' DROP INDEX ',given_index);
		set result = 'Drop index, ';
		prepare st from @sqlstmt;
		execute st;
		deallocate prepare st;
	end if;
	SET @sqlstmt = CONCAT('CREATE ',indextype,' ',given_index,' ON ', given_table,' (',given_columns,')');
    set result = concat(result,'Create Index');
	PREPARE st FROM @sqlstmt;
	EXECUTE st;
	DEALLOCATE PREPARE st;
 

END */;;

/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;;
# Dump of PROCEDURE admin_getRelationshipCheckins
# ------------------------------------------------------------

/*!50003 DROP PROCEDURE IF EXISTS `admin_getRelationshipCheckins` */;;
/*!50003 SET SESSION SQL_MODE="NO_ENGINE_SUBSTITUTION"*/;;
/*!50003 CREATE*/ /*!50020 DEFINER=`DoubleAngel`@`%`*/ /*!50003 PROCEDURE `admin_getRelationshipCheckins`(rid int)
BEGIN
-- rid = relationship

select	"ToBeCreated" as checkinDate,
		wheel_setting,
		checkins.`comment`,
		checkins.deleted_checkin,
		needs.name,
		trinities.abbreviation
-- needs.name as need, 
		-- trinities.abbreviation as trinity, 
		-- un.no_longer_needed, 
		-- ns.slider_value, 
		-- ns.turned_stormy, 
		-- relationships.name,
		-- un.modified as UserNeedModifiedDate,
		-- un.created as UserNeedCreatedDate
  from q0_relationships relationships 
		join q0_users users on users.id = relationships.user_id
		join q0_checkins checkins on checkins.relationship_id = relationships.id
		left join q0_users_needs un on un.id = checkins.users_need_id
		left join q0_needs needs on needs.id = un.need_id
		left join q0_trinities trinities on trinities.id = needs.trinity_id
where relationships.id = rid
;

END */;;

/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;;
# Dump of PROCEDURE admin_getRelationshipNeeds
# ------------------------------------------------------------

/*!50003 DROP PROCEDURE IF EXISTS `admin_getRelationshipNeeds` */;;
/*!50003 SET SESSION SQL_MODE="NO_ENGINE_SUBSTITUTION"*/;;
/*!50003 CREATE*/ /*!50020 DEFINER=`DoubleAngel`@`%`*/ /*!50003 PROCEDURE `admin_getRelationshipNeeds`(rid int)
BEGIN
-- rid = relationships.id

select needs.name as need, 
		trinities.abbreviation as trinity, 
		un.no_longer_needed, 
		ns.slider_value, 
		ns.turned_stormy, 
		relationships.name,
		un.modified as UserNeedModifiedDate,
		un.created as UserNeedCreatedDate
  from q0_relationships relationships 
		join q0_users users on users.id = relationships.user_id
		join q0_users_needs un on un.user_id = users.id
		join q0_needs needs on needs.id = un.need_id
		join q0_needsliders ns on un.id = ns.users_need_id
		join q0_trinities trinities on trinities.id = needs.trinity_id
where relationships.id = rid
;
END */;;

/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;;
# Dump of PROCEDURE Checkin_SetDate_Range
# ------------------------------------------------------------

/*!50003 DROP PROCEDURE IF EXISTS `Checkin_SetDate_Range` */;;
/*!50003 SET SESSION SQL_MODE="NO_ENGINE_SUBSTITUTION"*/;;
/*!50003 CREATE*/ /*!50020 DEFINER=`DoubleAngel`@`%`*/ /*!50003 PROCEDURE `Checkin_SetDate_Range`(FromDate datetime, R_id int, r_Type char(7))
BEGIN
/* This checkin dates that are end or start of relatinoship must occur in sequence.  start then end then start then end
     Since users can move the date and also when starting or ending can set their own checkin date the system needs to 
     know the previous and next checkin of the opposite type. That sets the boundaries.  There is one exception
     that is when no start and end checkins have been done then the boundaries are today back to the start date of 
     the relationship.  It is expected that the user interface would not give them the option of "restart relationship" if 
     the relationship if the relation only had a start date.  So if end relationship is changing then the return values
     will be the next earliest restart/start relationship datetime and the next later restart relationship date time or null if there
     is no later restart relationship exists.  In that case the devices current datetime is the latest they can set.

	Parameters:
	FromDate: device datetime if this is a new record, checkin_date if this is a record being modified
	r_id: 	  relationship Id being addressed
	r_type:   int type of record being added or changed "1 = restart", "2 = end", "0 = normal"

    Return: 
		single record with two columns, Earliest, Latest.  these hold datetime values for the date range you can use. 
                 If Latest is null then use the current datetime on the device.
*/


set @r_StartDate = (select start_date from q0_relationships r where r.id = R_id);
-- set the type to look for based on the type this is.  If it's a restart we are looking for between two ends
--  if it's an end we are looking for between two restarts. leave it alone if it's normal
set @r_type = (select case when r_type = 1 then 2 when r_type = 2 then 1 end); 

if r_type = 0 then
	select @r_StartDate, null;
else
	Begin
		select 
				coalesce(EarliestDate,@r_StartDate) as EarliestDate, 
				LatestDate
		from (
		select
			(select max(checkin_date)
					from q0_checkins checkins
				   where checkins.relationship_id = r_id
					 and checkins.checkin_type = @r_Type
					 and checkins.checkin_date < FromDate
				group by relationship_id) as EarliestDate,
			   (select min(checkin_date)
					from q0_checkins checkins
				   where checkins.relationship_id = r_id
					 and checkins.checkin_type = @r_Type
					 and checkins.checkin_date > FromDate
				group by relationship_id) as LatestDate

		  from q0_checkins checkins
		where checkins.relationship_id = r_id
		group by 1,2) as CheckinDates
		;
	End;
end if;

END */;;

/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;;
# Dump of PROCEDURE getNeedsliderTrinityAbbrev
# ------------------------------------------------------------

/*!50003 DROP PROCEDURE IF EXISTS `getNeedsliderTrinityAbbrev` */;;
/*!50003 SET SESSION SQL_MODE="NO_ENGINE_SUBSTITUTION"*/;;
/*!50003 CREATE*/ /*!50020 DEFINER=`DoubleAngel`@`%`*/ /*!50003 PROCEDURE `getNeedsliderTrinityAbbrev`(in pId int, out pAbbrev char(3))
select trinities.abbreviation into pAbbrev
    	from q0_needsliders ns 
    		join q0_users_needs un on ns.users_need_id = un.id
    		join q0_needs needs on needs.id = un.need_id
    		join q0_trinities trinities on trinities.id = needs.trinity_id
       where ns.id = pId */;;

/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;;
# Dump of PROCEDURE InNeedOfCheckup
# ------------------------------------------------------------

/*!50003 DROP PROCEDURE IF EXISTS `InNeedOfCheckup` */;;
/*!50003 SET SESSION SQL_MODE="NO_ENGINE_SUBSTITUTION"*/;;
/*!50003 CREATE*/ /*!50020 DEFINER=`DoubleAngel`@`%`*/ /*!50003 PROCEDURE `InNeedOfCheckup`(u_id int, MonthToCheck date)
BEGIN
/*
	This routine is used in two places.  
	returns the relationship.id for all relationships that are active or that 
			ended in the month to check.  This result is done for every relationship
            or for the relationships for a particular user if the u_id is null.
            Further if the current date is before the @LastDay then relationships
            that ended in the month are not returned. 

Testing:  Not easy.  use InNeedOfCheckupTest stored procedure.  Copy the select statements from InNeedOfCheckup 
			into the select statements in the insert into in this procedure. 
			Then these two SQL statements in the IDE can
            be used to get the results and the raw data to check against.  The tests assumes that ForecastAndRelationshipDates
            returns the proper data so that has to be tested well first.
set @rid = 23;
set @MonthToCheck = "2015-04-02");
call InNeedOfCheckup2(@rid,@MonthToCheck);
select * from inneedof where id in (select id from q0_relationships where user_id = @rid) order by id;
select * from ForecastAndRelationshipDates frd where frd.relationship_id in (select r.id from q0_relationships r where r.user_id = @rid);		

NOTE:  It is possible that a person can change the ended relationship as follows and make it look like it should
       have a forecast when it would not:
			Ended Relationship March 15 th
			In may they change the date to April 10th
			the batch run has already run and would not have gotten a forecast for this relationship
            even thought the current date would indicate it should
       same could happen if they change the date backwards after a forecast is done.
*/

set @FirstDay = date(subdate(MonthToCheck, interval (day(MonthToCheck)-1) day ));
set @LastDay = adddate(last_day(MonthToCheck), interval 1 day);


if (u_id is null) then
   -- if running for end of month batch run return relationships ended in MonthToCheck
	select r.id
	  from q0_relationships r 
		left outer join ForecastAndRelationshipDates frd on frd.relationship_id = r.id
		where (LastForecastDate not between @FirstDay and @LastDay
				or LastForecastDate is null)
		and (EndedDate > @FirstDay 
				or EndedDate is null);
else 
   -- if running before end of month then don't return relationships ended in MonthToCheck
	Select r.id
	  from q0_relationships r
		left outer join ForecastAndRelationshipDates frd on frd.relationship_id = r.id
		where (LastForecastDate not between @FirstDay and @LastDay
				or LastForecastDate is null)
		and (EndedDate > @LastDay 
				or EndedDate is null)
	  and (u_id = frd.user_id);

end if;

END */;;

/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;;
# Dump of PROCEDURE InNeedOfCheckupTest
# ------------------------------------------------------------

/*!50003 DROP PROCEDURE IF EXISTS `InNeedOfCheckupTest` */;;
/*!50003 SET SESSION SQL_MODE="NO_ENGINE_SUBSTITUTION"*/;;
/*!50003 CREATE*/ /*!50020 DEFINER=`DoubleAngel`@`%`*/ /*!50003 PROCEDURE `InNeedOfCheckupTest`(u_id int, MonthToCheck date)
BEGIN

/*
	This routine is used in two places.  
	returns the relationship.id for all relationships that are active or that 
			ended in the month to check.  This result is done for every relationship
            or for the relationships for a particular user if the u_id is null.
            Further if the current date is before the @LastDay then relationships
            that ended in the month are not returned. 

Testing:  Not easy.  use InNeedOfCheckupTest stored procedure.  Copy the select statements from InNeedOfCheckup 
			into the select statements in the insert into in this procedure. 
			Then these two SQL statements in the IDE can
            be used to get the results and the raw data to check against.  The tests assumes that ForecastAndRelationshipDates
            returns the proper data so that has to be tested well first.
set @rid = 23;
set @MonthToCheck = "2015-04-02");
call InNeedOfCheckup2(@rid,@MonthToCheck);
select * from inneedof where id in (select id from q0_relationships where user_id = @rid) order by id;
select * from ForecastAndRelationshipDates frd where frd.relationship_id in (select r.id from q0_relationships r where r.user_id = @rid);		

*/

set @FirstDay = date(subdate(MonthToCheck, interval (day(MonthToCheck)-1) day ));
set @LastDay = adddate(last_day(MonthToCheck), interval 1 day);


if (u_id is null) then
   -- if running for end of month batch run return relationships ended in MonthToCheck
	create temporary table if not exists inneedof as (select r.id
	  from q0_relationships r 
		left outer join ForecastAndRelationshipDates frd on frd.relationship_id = r.id
		where (LastForecastDate not between @FirstDay and @LastDay
				or LastForecastDate is null)
		and (EndedDate > @FirstDay 
				or EndedDate is null));
else 
   -- if running before end of month then don't return relationships ended in MonthToCheck
	create temporary table if not exists inneedof as (Select r.id
	  from q0_relationships r
		left outer join ForecastAndRelationshipDates frd on frd.relationship_id = r.id
		where (LastForecastDate not between @FirstDay and @LastDay
				or LastForecastDate is null)
		and (EndedDate > @LastDay 
				or EndedDate is null)
	  and (u_id = frd.user_id));

end if;

END */;;

/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;;
# Dump of PROCEDURE MonitorGraph
# ------------------------------------------------------------

/*!50003 DROP PROCEDURE IF EXISTS `MonitorGraph` */;;
/*!50003 SET SESSION SQL_MODE="NO_ENGINE_SUBSTITUTION"*/;;
/*!50003 CREATE*/ /*!50020 DEFINER=`DoubleAngel`@`%`*/ /*!50003 PROCEDURE `MonitorGraph`(
	rid int,
	EndDate date,
	IntervalName char(5),
	NumOfIntervals int)
BEGIN
/* this procedure will return the average of all checkins for each interval from the date selected back for the 
    number of intervals requested.  
		Example:  call MonitorGraph(23,"2015-04-28","week",8)   
					returns 8 rows of three columns 
					IntervalName, Average  NextDate
					"2015-05-04"	6		"2015-03-08"
					"2015-04-27"	5       "2015-03-08" 
		The date sent is the last day of the interval.  Weeks end on sunday, start on monday
        the date labeling can be changed as needed.   The NextDate is the date to send the routine
		to get the next previous group of averages.  It will be the same in all rows.

NOTE!   THIS PROCEDURE DOES NOT FILL IN DATES OR INTERVALS WITH NO CHECKINS.  THOSE INTERVALS ARE SIMPLY NOT INCLUDED

tests:
	first test the date setting routines by commenting out the averages and sending this a bunch of dates
    with different IntervalNames and check the returns.  Especially the return of the dates that are first and last 
    of an interval.  Check Leap Year
    check averages by running against test data and manually comparing calculation to whats displayed.accessible
	check NextDate by running multiple times using nextdate as teh new date and make sure it get's the next set.

*/

if lower(IntervalName)= "day" then
	set @LastDate = adddate(EndDate,interval 1 day); -- must be one day after last date of data to be inclusive in query
	set @FirstDate = subdate(@LastDate,interval NumOfIntervals day);
elseif lower(IntervalName) = "week" then
	-- end of week that the date falls in minus 6 days to be the end of the last full week
	set @LastDate = date(EndDate+INTERVAL (7-WEEKDAY(EndDate)) DAY);
	set @FirstDate = subdate(@LastDate, interval (NumOfIntervals * 7) day); 
elseif lower(IntervalName) = "month" then
	set @LastDate = adddate(subdate(EndDate, interval (day(EndDate)-1) day), interval 1 month);
	set @FirstDate = subdate(@LastDate, interval NumOfIntervals month);
end if;


if lower(IntervalName) = "day" then
	select date(checkin_date) as EndOfInterval, 
			avg(wheel_setting) as Average,
			subdate(@FirstDate,interval 1 day) as NextDate
	 from q0_checkins 
	where checkin_date between @FirstDate and @LastDate
	and relationship_id = rid
	group by EndOfInterval;
elseif lower(IntervalName) = "week" then
	select DATE(checkin_date + INTERVAL (6 - WEEKDAY(checkin_date)) DAY) as EndOfInterval, 
			avg(wheel_setting) as Average,
			subdate(@FirstDate,interval 1 day) as NextDate
	 from q0_checkins 
	where checkin_date between @FirstDate and @LastDate
	and relationship_id = rid
	group by EndOfInterval;
elseif lower(IntervalName) = "month" then
	select subdate(adddate(subdate(date(checkin_date), interval (day(date(checkin_date))-1) day),interval 1 month), interval 1 day) as EndOfInterval,
			avg(wheel_setting) as Average,
			subdate(@FirstDate,interval 1 day) as NextDate
	 from q0_checkins 
	where checkin_date between @FirstDate and @LastDate
	and relationship_id = rid
	group by EndOfInterval;
end if;

END */;;

/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;;
# Dump of PROCEDURE MonitorGraphDetail
# ------------------------------------------------------------

/*!50003 DROP PROCEDURE IF EXISTS `MonitorGraphDetail` */;;
/*!50003 SET SESSION SQL_MODE="NO_ENGINE_SUBSTITUTION"*/;;
/*!50003 CREATE*/ /*!50020 DEFINER=`DoubleAngel`@`%`*/ /*!50003 PROCEDURE `MonitorGraphDetail`(
	rid int,
	EndDate date,
	IntervalName char(5),
	NumOfIntervals int)
BEGIN
/* this procedure will return the all checkins for each interval from the date selected back for the 
    number of intervals requested that were used in MonitorGraph to create the averages.
   Match the IntervalName from one result set with the interval name from the other.  
		Example:  call MonitorGraph(23,"2015-04-28","week",8)   
					returns 8 rows of three columns 
					IntervalName, Wheel_setting  NextDate
					"2015-05-03"	6			"2015-03-08"
					"2015-04-26"	5       	"2015-03-08" 
		The date sent is the last day of the interval.  Weeks end on sunday, start on monday
        the date labeling can be changed as needed.   The NextDate is the date to send the routine
		to get the next previous group of averages.  It will be the same in all rows.

NOTE!   THIS PROCEDURE DOES NOT FILL IN DATES OR INTERVALS WITH NO CHECKINS.  THOSE INTERVALS ARE SIMPLY NOT INCLUDED

tests:
   this is exactly the same code as MonitorGraph but without the group by or average

*/

if lower(IntervalName)= "day" then
	set @LastDate = adddate(EndDate,interval 1 day); -- must be one day after last date of data to be inclusive in query
	set @FirstDate = subdate(@LastDate,interval NumOfIntervals day);
elseif lower(IntervalName) = "week" then
	-- end of week that the date falls in minus 6 days to be the end of the last full week
	set @LastDate = date(EndDate+INTERVAL (7-WEEKDAY(EndDate)) DAY);
	set @FirstDate = subdate(@LastDate, interval (NumOfIntervals * 7) day); 
elseif lower(IntervalName) = "month" then
	set @LastDate = adddate(subdate(EndDate, interval (day(EndDate)-1) day), interval 1 month);
	set @FirstDate = subdate(@LastDate, interval NumOfIntervals month);
end if;


if lower(IntervalName) = "day" then
	select date(checkin_date) as EndOfInterval, 
			-- avg(wheel_setting) as Average,
			wheel_setting,
			subdate(@FirstDate,interval 1 day) as NextDate,
			date(checkin_date)
	 from q0_checkins 
	where checkin_date between @FirstDate and @LastDate
	and relationship_id = rid
	order by EndOfInterval
--	group by EndOfInterval
;
elseif lower(IntervalName) = "week" then
	select DATE(checkin_date + INTERVAL (6 - WEEKDAY(checkin_date)) DAY) as EndOfInterval, 
--			avg(wheel_setting) as Average,
			wheel_setting,
			subdate(@FirstDate,interval 1 day) as NextDate,
			date(checkin_date)
	 from q0_checkins 
	where checkin_date between @FirstDate and @LastDate
	and relationship_id = rid
	order by EndOfInterval
--	group by EndOfInterval
;
elseif lower(IntervalName) = "month" then
	select subdate(adddate(subdate(date(checkin_date), interval (day(date(checkin_date))-1) day),interval 1 month), interval 1 day) as EndOfInterval,
--			avg(wheel_setting) as Average,
			wheel_setting,
			subdate(@FirstDate,interval 1 day) as NextDate,
			date(checkin_date)
	 from q0_checkins 
	where checkin_date between @FirstDate and @LastDate
	and relationship_id = rid
	order by EndOfInterval
--	group by EndOfInterval
;
end if;

END */;;

/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;;
# Dump of PROCEDURE Relationship_EndDate
# ------------------------------------------------------------

/*!50003 DROP PROCEDURE IF EXISTS `Relationship_EndDate` */;;
/*!50003 SET SESSION SQL_MODE="NO_ENGINE_SUBSTITUTION"*/;;
/*!50003 CREATE*/ /*!50020 DEFINER=`DoubleAngel`@`%`*/ /*!50003 PROCEDURE `Relationship_EndDate`(
	r_id int -- relationship id
)
begin

select EndedDate 
  from Relationship_EndDates
 where Relationship_id = r_id;

/* 
users Relationship_EndDates View that uses Relationship_CheckinDates
*/
end */;;

/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;;
# Dump of PROCEDURE UserDefaultNeedsV2
# ------------------------------------------------------------

/*!50003 DROP PROCEDURE IF EXISTS `UserDefaultNeedsV2` */;;
/*!50003 SET SESSION SQL_MODE="NO_ENGINE_SUBSTITUTION"*/;;
/*!50003 CREATE*/ /*!50020 DEFINER=`DoubleAngel`@`%`*/ /*!50003 PROCEDURE `UserDefaultNeedsV2`(
in p_user_id int, 
in p_gender int,
in p_relationship int 
)
BEGIN
/* although gender and the initial relationship id could be looked up 
   in the interest of speed I've assumed the code already has those values
   and getting it as parameters eliminates two potentially expensive lookups
*/

insert into q0_users_needs
(need_id, picked_need_id, user_id, created, modified_by_user_id, modified)
select q0_users_needs.need_id, q0_users_needs.need_id, p_user_id, now(), p_user_id, now()
from q0_users_needs 
join q0_users on q0_users.id = q0_users_needs.user_id
left join q0_user_profiles on q0_user_profiles.user_id = q0_users.id 
where (username = 'male_default' or username = 'female_default')
and q0_user_profiles.gender = p_gender
and q0_users_needs.no_longer_needed = 0; 

insert into q0_needsliders 
(relationship_id, 
users_need_id, 
slider_value,
turned_stormy,
user_id, 
created, 
modified_by_user_id, 
modified)
select p_relationship, 
      un1.id,
      (select slider_value
			from q0_needsliders as ns
			join q0_users as users on users.id = ns.user_id
			join q0_user_profiles as up on users.id = up.user_id
			join q0_users_needs as un on ns.users_need_id = un.id
			where (users.username = 'male_default' or users.username = 'female_default')
			and up.gender = p_gender 
			and un1.need_id = un.need_id) as svalue,
      (select (case isnull(turned_stormy)
				when 1 then null
				when 0 then now()
				end) 
			from q0_needsliders as ns
			join q0_users as users on users.id = ns.user_id
			join q0_user_profiles as up on users.id = up.user_id
			join q0_users_needs as un on ns.users_need_id = un.id
			where (users.username = 'male_default' or users.username = 'female_default')
			and up.gender = p_gender 
			and un1.need_id = un.need_id) as tsvalue,
      p_user_id, 
	  now(), 
      p_user_id, 
      now()
from q0_users_needs as un1
where un1.user_id = p_user_id; 

/*
update q0_needsliders 
set turnedstormy = 
(select (case isnull(turned_stormy)
		when 1 then null
		when 0 then now()
end) 
from q0_needsliders where q0_needsliders.user_id = p_user_id) ;
*/

END */;;

/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;;
DELIMITER ;

/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
