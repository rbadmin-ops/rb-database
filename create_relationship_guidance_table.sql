CREATE TABLE `q0_relationship_guidance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(150) NOT NULL,
  `functional_area` varchar(45) DEFAULT NULL,
  `answer` text,
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified_by_user_id` int(11) DEFAULT NULL,
  `modified` datetime NOT NULL,
  `key` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `relationship_guidance_createdby` (`user_id`),
  KEY `relationship_guidance_modifiedby_idx` (`modified_by_user_id`),
  CONSTRAINT `relationship_guidance_createdby` FOREIGN KEY (`user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `relationship_guidance_modifiedby` FOREIGN KEY (`modified_by_user_id`) REFERENCES `q0_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COMMENT='I have not added search indexing pending knowing the tool we are going to use	';
