DROP PROCEDURE IF EXISTS `InNeedOfCheckup`;

DELIMITER ;;
CREATE DEFINER=`DoubleAngel`@`%` PROCEDURE `InNeedOfCheckup`(u_id int, MonthToCheck date)
BEGIN
/*
	This routine is used in two places.  
	returns the relationship.id for all relationships that are active or that 
			ended in the month to check.  This result is done for every relationship
            or for the relationships for a particular user if the u_id is null.
            Further if the current date is before the @LastDay then relationships
            that ended in the month are not returned. 

Testing:  Not easy.  use InNeedOfCheckupTest stored procedure.  Copy the select statements from InNeedOfCheckup 
			into the select statements in the insert into in this procedure. 
			Then these two SQL statements in the IDE can
            be used to get the results and the raw data to check against.  The tests assumes that ForecastAndRelationshipDates
            returns the proper data so that has to be tested well first.
set @rid = 23;
set @MonthToCheck = "2015-04-02");
call InNeedOfCheckup2(@rid,@MonthToCheck);
select * from inneedof where id in (select id from q0_relationships where user_id = @rid) order by id;
select * from ForecastAndRelationshipDates frd where frd.relationship_id in (select r.id from q0_relationships r where r.user_id = @rid);		

NOTE:  It is possible that a person can change the ended relationship as follows and make it look like it should
       have a forecast when it would not:
			Ended Relationship March 15 th
			In may they change the date to April 10th
			the batch run has already run and would not have gotten a forecast for this relationship
            even thought the current date would indicate it should
       same could happen if they change the date backwards after a forecast is done.
*/

set @FirstDay = date(subdate(MonthToCheck, interval (day(MonthToCheck)-1) day ));
set @LastDay = adddate(last_day(MonthToCheck), interval 1 day);


if (u_id is null) then
   -- if running for end of month batch run return relationships ended in MonthToCheck
	select r.id
	  from q0_relationships r 
		left outer join ForecastAndRelationshipDates frd on frd.relationship_id = r.id
		left outer join q0_users u on r.user_id = u.id
		where (LastForecastDate not between @FirstDay and @LastDay
				or LastForecastDate is null)
		and (EndedDate > @FirstDay 
				or EndedDate is null)
		and DATEDIFF(CURDATE(), u.`created`) > 15;
else 
   -- if running before end of month then don't return relationships ended in MonthToCheck
	Select r.id
	  from q0_relationships r
		left outer join ForecastAndRelationshipDates frd on frd.relationship_id = r.id
		left outer join q0_users u on r.user_id = u.id
		where (LastForecastDate not between @FirstDay and @LastDay
				or LastForecastDate is null)
		and (EndedDate > @LastDay 
				or EndedDate is null)
		and DATEDIFF(CURDATE(), u.`created`) > 15
	  and (u_id = frd.user_id);

end if;

END;;
DELIMITER ;